
require(['jquery'], function ($) {
    $(document).ready(function () {
        $('#dismiss, .menu-overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.menu-overlay').fadeOut();
        });
        $('.navbar-toggle').click(function(){
            $('#sidebar').addClass('active');
            $('.menu-overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        $(".home-faq-btn").click(function(){
            $(".home-slide-faq").toggle(1000);
            var $this = $(this);
            $this.toggleClass('home-faq-btn');
            if($this.hasClass('home-faq-btn')){
                $this.text('More');			
            } else {
                $this.text('Less');
            }
        });
        $('#heading2 .panel-title a, .form-navgen-data .panel .legend').click(function(){
            $('#heading2 .panel-title a, .form-navgen-data .panel .legend').not($(this)).find('.more-less').removeClass('fa-minus').addClass('fa-plus');
           // $(this).find('.more-less').toggleClass('fa-plus fa-minus');
            if($(this).find('.more-less').hasClass('fa-plus'))
            {
                $(this).find('.more-less').removeClass('fa-plus').addClass('fa-minus');
            }
            else{
                $(this).find('.more-less').removeClass('fa-minus').addClass('fa-plus');
            }
        });
        
    });
});