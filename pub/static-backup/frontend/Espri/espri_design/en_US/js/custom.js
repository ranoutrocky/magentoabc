
require(['jquery'], function ($) {

    $(document).ready(function () {
        $('#dismiss, .menu-overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.menu-overlay').fadeOut();
        });
        $('.navbar-toggle').click(function(){
            $('#sidebar').addClass('active');
            $('.menu-overlay').fadeIn();
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
        $(".home-faq-btn").click(function(){
            $(".home-slide-faq").toggle(1000);
        });
        
    });
});