define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data'
], function ($, authenticationPopup, customerData
) {
    'use strict';

    return function (Component) {
        return Component.extend({

            /**
             * @override
             */
            getCartParam: function (name) {
                var self = this;

                if(name === 'possible_onepage_checkout'){
                    $('#top-cart-btn-checkout').click(function (event) {
                        
                        var customer = customerData.get('customer');
                        if (!customer().firstname) {
                            event.preventDefault();
                            $(".customer-login-link").click();
                            return false;
                        }
                    });

                }
                return this._super(name);
            },
        });
    }
});