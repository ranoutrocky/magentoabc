<?php
//echo substr('1234567890',0,5);
//exit;
use Magento\Framework\App\Bootstrap;include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');
require_once('../../vendor/autoload.php');

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;
use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\Charge;
use Magento\Framework\App\Filesystem\DirectoryList;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');


 $state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('Psr\Log\LoggerInterface');
$storeManager->info('Magecomp Log');

$storeManager=$objectManager->get('Magento\Store\Model\StoreManagerInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$request = $objectManager->get(Magento\Framework\App\Request\Http::class);

$connection = $resource->getConnection();
$orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 

$quoteToOrder = $objectManager
            ->create(
                '\Magento\Quote\Model\Quote\Item\ToOrderItem'
            );
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$invoiceFileName = $helper->getInvoiceName();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();

$connection = $resource->getConnection();
$bucket = 'esprigas.beverage.portal';
$source_path=BP.'/var/NAV/Payments/';
$dest_path=BP.'/var/NAV/Payments/Archived/';


$yesterday =  date('Y-m-d',strtotime("-1 days"));
$today =  date('Y-m-d');
$payment_date=date('Y-m-d',strtotime("+2 days"));


$order_import = $resource->getTableName('esprimodule_orderimportdata_orderimportdata');


  $fileName = 'BP_to_NAV_Payment_Information_'.date('Ydm_His').'.csv';
  $s3_file_url = 's3://'.$bucket.'/Payments/BP/'.$fileName;

  
 $s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
        'key'    => $s3key,
        'secret' => $s3secretkey,
    ]
]);
$s3->registerStreamWrapper();
 
 $inputFileName = $source_path.$fileName ;
  $archiveFileName = $dest_path.$fileName ;

 
	
	 try {
	$objPHPExcel = new PHPExcel();
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(',')
                                             ->setEnclosure('')
                                             ->setLineEnding("\r\n");
											 
											 
    $paramVal=$request->getParam('date');
echo $paramVal;
	if(validateDate($paramVal))
	{
	  $yesterday =  $paramVal;
	  $today = date('Y-m-d', strtotime($yesterday . ' +1 day'));
	$payment_date=date('Y-m-d',strtotime($yesterday . "+2 days"));
	  $customdateparameter= " AND si.created_at >='$yesterday' AND si.created_at<='$today' ";
	}
	else
		$customdateparameter =' AND ei.export_status = 0 ';
	echo 'test'.'<br/>';

	/*
	echo $query = "SELECT ei.*,si.created_at,si.transaction_id FROM esprimodule_orderimportdata_orderimportdata AS ei
	INNER JOIN sales_order AS so ON ei.bp_order_number = so.increment_id
	INNER JOIN sales_invoice AS si ON so.entity_id = si.order_id
	WHERE ei.Status ='1' AND si.created_at >='$yesterday' AND si.created_at<='$today'
	AND si.transaction_id IS NOT NULL AND si.transaction_id!='' 
	$customdateparameter group by ei.bp_order_number ";
	*/
echo $query = "
	SELECT si.transaction_id,ei.nav_invoice_number,ei.nav_customer_number,ei.invoice_total,ei.bp_customer_id,si.created_at,ei.bp_order_number FROM esprimodule_orderimportdata_orderimportdata AS ei
	INNER JOIN sales_order AS so ON ei.bp_order_number = so.increment_id
	INNER JOIN sales_invoice AS si ON so.entity_id = si.order_id
	WHERE ei.Status ='1'
	AND si.transaction_id IS NOT NULL AND si.transaction_id!='' 
	$customdateparameter group by ei.bp_order_number ";

	
/*
echo	$query = "SELECT si.transaction_id,ei.nav_invoice_number,ei.nav_customer_number,ei.invoice_total,ei.bp_customer_id,si.created_at FROM esprimodule_orderimportdata_orderimportdata AS ei
	INNER JOIN sales_order AS so ON ei.bp_order_number = so.increment_id
	INNER JOIN sales_invoice AS si ON so.entity_id = si.order_id
	WHERE ei.Status ='1' AND si.created_at >='2021-04-09' AND si.created_at<='2021-04-10'
	AND si.transaction_id IS NOT NULL AND si.transaction_id!=''
$customdateparameter group by ei.bp_order_number ";
*/

$recordarray = $connection->fetchAll($query);
//print_r($recordarray);die;
$row = 0;
$TotalAmount = 0;
$tot_quantity=0;
$gl_line_num=1000;
 
    foreach ($recordarray as $record)
    {
		$row ++;
		$TotalAmount = $TotalAmount +  floatval($record['invoice_total']);
	 $magento_cus_id = $record['bp_customer_id'];
	     $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
 
$customerObj = $objectManager->create('Magento\Customer\Model\Customer')
            ->load($magento_cus_id);
 $customeName = $customerObj->getFirstname().' '.$customerObj->getLastname() ;
	
	 
	
	$cdate=date_create($record['created_at']);
    $createddate = date_format($cdate,"Y-m-d");

    
   
   $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $gl_line_num)
                              ->setCellValue('B'.$row,  'Customer')
                              ->setCellValue('C'.$row, $record['nav_customer_number'])
                              ->setCellValue('D'.$row, $customeName)
                              ->setCellValue('E'.$row, $createddate)
							   ->setCellValue('F'.$row, 'Payment')
                              ->setCellValue('G'.$row, $record['nav_invoice_number'])
                              ->setCellValue('H'.$row, $record['nav_invoice_number'])
                              ->setCellValue('I'.$row, $record['nav_invoice_number'])
							   ->setCellValue('J'.$row, '-'.$record['invoice_total'])
                              ->setCellValue('K'.$row, $record['nav_invoice_number'])
                              ->setCellValue('L'.$row, $record['invoice_total'])
                              ->setCellValue('M'.$row, 'Invoice')
							   ->setCellValue('N'.$row, $record['nav_invoice_number'])
							->setCellValue('O'.$row, $record['bp_order_number'])
                              ->setCellValue('P'.$row, $record['nav_invoice_number'])
                              ->setCellValue('Q'.$row, $record['nav_invoice_number'])
							   ->setCellValue('R'.$row, 'AC')
							   ->setCellValue('S'.$row, '1')
							   ->setCellValue('T'.$row, $record['invoice_total'])
                              ->setCellValue('U'.$row, $record['nav_invoice_number'])
                              ->setCellValue('V'.$row, $record['nav_invoice_number'])
							  ->setCellValue('W'.$row, $record['nav_invoice_number'])
                              ->setCellValue('X'.$row, $record['bp_order_number']);
		// $tot_quantity=$tot_quantity+$record['quantity'];					  
                            //  ->setCellValue('O'.$row, substr($record['transaction_id'],0,19))
							UpdateStatus($record['bp_order_number'],1);
	$gl_line_num=$gl_line_num+10;						  
	}		
$createddate = $today;
//$payment_date_new=date('Y-m-d',strtotime($today . "+2 days"));	
	$row ++;
	$objPHPExcel->getActiveSheet()
								->setCellValue('A'.$row, $gl_line_num)
								->setCellValue('B'.$row,  'G/L Account')
                              ->setCellValue('C'.$row, '10700')
                              ->setCellValue('D'.$row, 'Credit Card Clearing')
							  ->setCellValue('E'.$row, $createddate)
							  ->setCellValue('F'.$row, 'Payment')
							  ->setCellValue('G'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('H'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('I'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('J'.$row, '-'.$TotalAmount)
							  ->setCellValue('K'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('L'.$row, $TotalAmount)
							  ->setCellValue('M'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('N'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('O'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('P'.$row, 'Invoice-'.$createddate)
							  ->setCellValue('Q'.$row, 'Invoice-'.$createddate)
                               ->setCellValue('R'.$row, 'AC')
							   ->setCellValue('S'.$row, '1')
							   ->setCellValue('T'.$row, $TotalAmount)
							   ->setCellValue('U'.$row, 'Invoice-'.$createddate)
							   ->setCellValue('V'.$row, 'Invoice-'.$createddate)
							   ->setCellValue('W'.$row, 'Invoice-'.$createddate)
							   ->setCellValue('X'.$row, 'Stripe-'.$createddate)
							   ;

                             
							  
							  $objWriter->save($inputFileName);
							  
							 $dataS3=file_get_contents($inputFileName);
	                         file_put_contents($s3_file_url,$dataS3);
							 writeLog('done file'.$inputFileName);
							 rename($inputFileName,$archiveFileName);
   
	
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}
			
function UpdateStatus($orderimportdata_id, $status )
{
	echo $updatequery  = "UPDATE esprimodule_orderimportdata_orderimportdata SET export_status = '".$status."'    WHERE bp_order_number= '".$orderimportdata_id."'";
	writeLog($updatequery);
	
  $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
   $resourceConnection = $objectManager->get(
                     '\Magento\Framework\App\ResourceConnection');
$connection  = $resourceConnection->getConnection();	
                        $connection->query($updatequery);
}					


function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}
function writeLog($message, $logfile = 'bp_export_payment.log')
{
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);
    $logger->info($message);
}
