<?php
include('../../app/bootstrap.php');    
    include_once('../../Excel/Classes/PHPExcel.php');
    use Magento\Framework\App\Bootstrap;
    use Magento\Customer\Api\AccountManagementInterface;
    use Magento\Customer\Model\AccountManagement;
	//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
	use Zend\Log\Filter\Timestamp;


$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();

$var = 'nav_order_imported.xlsx';
$inputFileName='/var/www/html/coke-portal-dev/var/NAV/'.$var;
$outputFileName='/var/www/html/coke-portal-dev/var/NAV/done/'.$var.'-'.$date;
			//Read your Excel workbook
			try{
				$inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
				$objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel    =   $objReader->load($inputFileName);
			}catch(Exception $e){
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getActiveSheet(); 
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestColumn();

			$dataArray = array();
			$row=2;

			$startRow=2;
			//$highestRow=3;

			$rowA='A'.$startRow;
			  for ($row =$startRow; $row <= $highestRow; $row++){
				$invoice_no='A'.$row;
				$po_no='C'.$row;
				$nav_cus_id='F'.$row;
				  $magento_cus_id='G'.$row;
				  $magento_order_id='J'.$row;
				  $qty='O'.$row;
				  $unit_price='P'.$row;
				  $line_amt='Q'.$row;
				  $freight='S'.$row;
				  $tax='T'.$row;
				  $dday='D'.$row;
				  $dueday='E'.$row;
				  $sku='M'.$row;
				  $totel ='U'.$row;
				  $subtotel ='R'.$row;


				$invoice_no = $sheet->getCell($invoice_no)->getValue();
				$po_no = $sheet->getCell($po_no)->getValue();
				$nav_cus_id = $sheet->getCell($nav_cus_id)->getValue();
				  $magento_cus_id = $sheet->getCell($magento_cus_id)->getValue();
                  $magento_order_id = $sheet->getCell($magento_order_id)->getValue();
                $qty = $sheet->getCell($qty)->getValue();
				  $unit_price = $sheet->getCell($unit_price)->getValue();
				  $line_amt = $sheet->getCell($line_amt)->getValue();
				  $freight = $sheet->getCell($freight)->getValue();
				  $tax = $sheet->getCell($tax)->getValue();
				  $dday = $sheet->getCell($dday)->getValue();
				  $dueday = $sheet->getCell($dueday)->getValue();
				 $sku = $sheet->getCell($sku)->getValue();
                 $totel = $sheet->getCell($totel)->getValue();
                 $subtotel = $sheet->getCell($subtotel)->getValue();
				  


				$orderId = $magento_order_id;
					
					$orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 
$quoteToOrder = $objectManager
            ->create(
                '\Magento\Quote\Model\Quote\Item\ToOrderItem'
            );
$updateData = array("order_id"=>$orderId, "items"=>$sku, "qty"=>$qty,'shipping_charge' => $freight,'tax'=>$tax,'invoice_total'=>$totel,'subtotal'=>$subtotel);
$order = $orderObject->load($updateData['order_id']);
if ($order->getId() && $order) {
    echo '<pre>'; echo $order->getQuoteId(); echo '</pre>';
    $quote = $objectManager
        ->create(
            '\Magento\Quote\Model\Quote'
        )->load($order->getQuoteId());
    $csvSkus = explode(",", $updateData['items']);
    $qtys = explode(",", $updateData['qty']);
    $releventqty = [];
    if (!empty($csvSkus) && !empty($qtys)) {
       foreach ($csvSkus as $key => $value) {
           if (isset($qtys[$key])) {
                $releventqty[$value] = $qtys[$key];
           }
       }
    }
    echo '<pre>'; print_r($csvSkus); echo '</pre>';
    $updateItems = [];
    foreach ($quote->getAllItems() as $quoteItem) {
        if (in_array($quoteItem->getSku(), $csvSkus)) {
            $updateItems[$quoteItem->getId().'_'.$quoteItem->getSku()] = $quoteItem->getId();
        }
    }
   // echo '<pre>'; print_r($updateItems); echo '</pre>';
    if (!empty($updateItems) && !empty($releventqty)) {
        foreach ($quote->getAllItems() as $quoteItem) {
            if (in_array($quoteItem->getId(), $updateItems)) {
                if (isset($releventqty[$quoteItem->getSku()])) {
                    if ($releventqty[$quoteItem->getSku()] > 0) {
                        $quoteItem->setQty($releventqty[$quoteItem->getSku()]);
                    }
                    else {
                        unset($updateItems[$quoteItem->getId().'_'.$quoteItem->getSku()]);
                    }
                }
            }
        }
        //echo '<pre>'; print_r($updateItems); echo '</pre>';exit;
        //invoice total likely be subtotal + tax + shipping
    if (!$quote->isVirtual()) {
        $total = $quote->getShippingAddress();
        if (isset($updateData['shipping_charge'])) {
            $total->setShippingAmount($updateData['shipping_charge']);
            $total->setBaseShippingAmount($updateData['shipping_charge']);
        }
        if (isset($updateData['tax'])) {
            $total->setTaxAmount($updateData['tax']);
            $total->setBaseTaxAmount($updateData['tax']);
        }
        $total->setSubtotal($updateData['subtotal']);
        $total->setBaseSubtotal($updateData['subtotal']);
        $total->setGrandTotal($updateData['invoice_total']);
        $total->setBaseGrandTotal($updateData['invoice_total']);
        $quote->collectTotals();
        $quote->save();
    }
    $quote->setSubtotal($updateData['subtotal']);
    $quote->setBaseSubtotal($updateData['subtotal']);
    $quote->setGrandTotal($updateData['invoice_total']);
    $quote->setBaseGrandTotal($updateData['invoice_total']);
    $quote->save();
    foreach ($quote->getAllItems() as $quoteItem) {
        if (in_array($quoteItem->getId(), $updateItems)) {
            $orderItem = $quoteToOrder->convert($quoteItem);
            $origOrderItemNew = $order->getItemByQuoteItemId($quoteItem->getId());
            if ($origOrderItemNew) {
                $origOrderItemNew->addData($orderItem->getData());
            } else {
                if ($quoteItem->getParentItem()) {
                    $orderItem->setParentItem(
                        $order->getItemByQuoteItemId($orderItem->getParentItem()->getId())
                    );
                }
                $order->addItem($orderItem);
            }
        }
    }
    foreach ($order->getAllItems() as $orderItem) {
        $getQuoteItemId = $orderItem->getQuoteItemId();
        if (in_array($getQuoteItemId, $updateItems)) {
        }
        else {
            $orderItem->isDeleted(true);
        }
    }
    if (isset($updateData['shipping_charge'])) {
        $order->setShippingAmount($updateData['shipping_charge']);
        $order->setBaseShippingAmount($updateData['shipping_charge']);
    }
    if (isset($updateData['tax'])) {
        $order->setTaxAmount($updateData['tax']);
        $order->setBaseTaxAmount($updateData['tax']);
    }
    $order->setSubtotal($quote->getSubtotal())
        ->setBaseSubtotal($quote->getBaseSubtotal())
        ->setGrandTotal($quote->getGrandTotal())
        ->setBaseGrandTotal($quote->getBaseGrandTotal());
    $quote->save();
    $order->save();
    //create invoice part
    $orderRepo = $objectManager->get('\Magento\Sales\Api\OrderRepositoryInterface');
    $invoiceService = $objectManager->get('\Magento\Sales\Model\Service\InvoiceService');
    $transaction = $objectManager->get('\Magento\Framework\DB\Transaction'); 
    $invoiceSender = $objectManager->get('\Magento\Sales\Model\Order\Email\Sender\InvoiceSender'); 
    $updatedOrder = $orderRepo->get($order->getId());
    if ($updatedOrder->canInvoice()) {
        $invoice = $invoiceService->prepareInvoice($updatedOrder);
        $invoice->setShippingAmount($updateData['shipping_charge']);
        $invoice->setSubtotal($updateData['subtotal']);
        $invoice->setBaseSubtotal($updateData['subtotal']);
        $invoice->setGrandTotal($updateData['invoice_total']);
        $invoice->setBaseGrandTotal($updateData['invoice_total']);
        $invoice->register();
        $invoice->save();
        $transactionSave = $transaction->addObject(
            $invoice
        )->addObject(
            $invoice->getOrder()
        );
        $transactionSave->save();
         echo '<pre>'; echo 'invoice done invoice id-->'. $invoice->getId(); echo '</pre>';
       // $this->invoiceSender->send($invoice);
       /* //Send Invoice mail to customer
        $updatedOrder->addStatusHistoryComment(
            __('Notified customer about invoice creation #%1.', $invoice->getId())
        )
            ->setIsCustomerNotified(true)
            ->save();*/
    }
    else {
        echo '<pre>'; echo 'invoice done already order id-->'. $order->getId(); echo '</pre>';
    }
    }
    
}
else {
  //  echo '<pre>'; echo 'debugging'; echo '</pre>';exit;
    //nothing to do
}
					
		
			}

			
			rename($inputFileName, $outputFileName);
			writeLog('Record inserted Successfully');


	function writeLog($message,$logfile='nav_order.log')
	{
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/'.$logfile);
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($message);
	}