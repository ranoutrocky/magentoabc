<?php

include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');

use Magento\Framework\App\Bootstrap;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;


$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
  
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$store = $storeManager->getStore();
$websiteId = $storeManager->getStore()->getWebsiteId();

$customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
$customerid = "50";
$order_item['item']= array("id"=>'20', "qty"=>'1');


// $orderIncrementId='BP012000016';
// $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
// $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($orderIncrementId);
// $method=$order->getPayment()->getMethod(); 
// $expmonth=$order->getPayment();
// $expyear=$order->getPayment()->getCcExpYear();
// $cclast4=$order->getPayment()->getCcLast4();
// $methodTitle = $method->getTitle();

// echo "<pre>";
// print_r($expmonth->getData());
// exit;
// echo "<pre>+++++";
// print_r($order_item);
// exit;
/**
 * check whether the email address is already registered or not
 */
// $customer = $customerFactory->setWebsiteId($websiteId)->loadById($orderData['email']);
// if (!$customer->getId()) {
//     try {
//         $customer = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
//         $customer->setWebsiteId($websiteId);
//         $customer->setEmail($orderData['email']);
//         $customer->setFirstname($orderData['shipping_address']['firstname']);
//         $customer->setLastname($orderData['shipping_address']['lastname']);
//         $customer->setPassword($orderData['email']);
//         $customer->save();
  
//         $customer->setConfirmation(null);
//         $customer->save();

//         $customAddress = $objectManager->get('\Magento\Customer\Model\AddressFactory')->create();
//         $customAddress->setData($orderData['shipping_address'])
//                       ->setCustomerId($customer->getId())
//                       ->setIsDefaultBilling('1')
//                       ->setIsDefaultShipping('1')
//                       ->setSaveInAddressBook('1');
//         $customAddress->save();  
//     } catch (Exception $e) {
//         echo $e->getMessage();
//     }
// }

$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customerid);
try {
    $billingID =  $customer->getDefaultBilling();
$address = $objectManager->create('Magento\Customer\Model\Address')->load($billingID);
echo "<pre>";print_r($address->getData());
    $quoteFactory = $objectManager->get('\Magento\Quote\Model\QuoteFactory')->create();
    $shippingQuoteRate = $objectManager->get('\Magento\Quote\Model\Quote\Address\Rate');
    $quoteFactory->setStore($store);
    $quoteFactory->setCurrency();
    $quoteFactory->assignCustomer($customer);
    foreach ($order_item as $item) {
        
        $product = $objectManager->get('\Magento\Catalog\Model\ProductRepository')->getById($item['id']);// get product by product id 
        $quoteFactory->addProduct($product, intval($item['qty']));  // add products to quote
    }
  
    /*
    * Set Address to quote
    */
    $quoteFactory->getBillingAddress()->addData($address->getData());
    $quoteFactory->getShippingAddress()->addData($address->getData());

    /*
    * Collect Rates and Set Shipping & Payment Method
    * You can get this information from the database table `quote_shipping_rate`
    */
    $shippingRateCarrier = 'freeshipping';
    $shippingRateCarrierTitle = 'Preferred Delivery';
    $shippingRateCode = 'freeshipping';
    $shippingRateMethod = 'freeshipping';
    $shippingRatePrice = '0';
    $shippingRateMethodTitle = 'Preferred Delivery';

    $shippingAddress = $quoteFactory->getShippingAddress();

    $shippingQuoteRate->setCarrier($shippingRateCarrier);
    $shippingQuoteRate->setCarrierTitle($shippingRateCarrierTitle);
    $shippingQuoteRate->setCode($shippingRateCode);
    $shippingQuoteRate->setMethod($shippingRateMethod);
    $shippingQuoteRate->setPrice($shippingRatePrice);
    $shippingQuoteRate->setMethodTitle($shippingRateMethodTitle);

    $shippingAddress->setCollectShippingRates(true)
                    ->collectShippingRates()
                    ->setShippingMethod($shippingRateCode); //shipping method
    $quoteFactory->getShippingAddress()->addShippingRate($shippingQuoteRate);

    $quoteFactory->setPaymentMethod('purchaseorder'); //payment method
    $quoteFactory->setInventoryProcessed(false);
    $quoteFactory->save();

    /*
    * Set Sales Order Payment
    */
    $orderNo = "123456";
    $quoteFactory->getPayment()->importData(['method' => 'purchaseorder','po_number' => $orderNo]);

    /*
    * Collect Totals & Save Quote
    */
    $quoteFactory->collectTotals()->save();

    /*
    * Create Order From Quote
    */
    
    $order = $objectManager->get('\Magento\Quote\Model\QuoteManagement')->submit($quoteFactory);
    $order->setEmailSent(0);
    $payment = $order->getPayment();
    $payment->setMethod('purchaseorder'); // Assuming 'purchaseorder' is updated payment method
    $payment->setPoNumber($orderNo);
    $payment->save();
    $order->setState('new');
    $order->setStatus('pending');
    
    $order->setEmailSent(0);
    
    $order->save();

    echo "<br/>Order Created<br/>";
} catch (Exception $e) {
    echo $e->getMessage();
}
?>