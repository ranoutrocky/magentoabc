<?php
include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');

use Magento\Framework\App\Bootstrap;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;


$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
 $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();

$customerId = 142;
$customer = $customerFactory->load($customerId);
/* echo "<pre>";
print_r($customer->getNavAch());die; */
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

$connection = $resource->getConnection();


$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();
$regionCollection = $objectManager->create('\Magento\Directory\Model\Country')->loadByCode('US')->getRegions();
$regions = $regionCollection->loadData()->toOptionArray(false);
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$fileName = $helper->getCustomerName();

$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();

require '../../vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
$fileName='NAV_to_BP_Customers';
$keyname = $fileName.'.csv';

$bucket = 'esprigas.beverage.portal';


 $s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
        'key'    => $s3key,
        'secret' => $s3secretkey,
    ]
]);
$s3->registerStreamWrapper();

$inputFileName = BP.'/var/NAV/Customers/' . $keyname; //$var;

$fileArr=[];
$dir='s3://'.$bucket.'/Customers/NAV/';
if (is_dir($dir) && ($dh = opendir($dir))) {
    while (($file = readdir($dh)) !== false) {
	$path=pathinfo($file);
	if (!in_array('Archived',$path))
	{
	 if((stripos($file,'.csv')>0) )
	 {
		array_push($fileArr,$file);	 
	 }
    }
	}
    closedir($dh);

}

 foreach ($fileArr as &$keyname) {
 

$inputFileName =BP. '/var/NAV/Customers/' . $keyname; //$var;
echo $inputFileName;
$s3->registerStreamWrapper();
$url = 's3://'.$bucket.'/Customers/NAV/'.$keyname;
$done_url='s3://'.$bucket.'/Customers/NAV/Archived/'.$keyname.'-'. $date;
try {
	$dataS3=file_get_contents($url);
	file_put_contents($inputFileName,$dataS3);
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
$outputFileName =BP. '/var/NAV/Customers/Done/' . $keyname . '-'. $date;
//echo $outputFileName;
$customer_import = $resource->getTableName('esprimodule_customerimportdata_customerimportdata'); //gives table name with 

$query = "EXISTS(SELECT * from ".$customer_import." WHERE Filename='".$keyname."')";
$sql2 = "SELECT ".$query."";
//echo $query;
$isfile = $connection->fetchAll($sql2);
//print_r($isfile);die;
//echo $isfile[0][$query];
if ($isfile[0][$query]!=0) {
 // continue;
  echo $keyname."  Filename allready updated";
  exit;
}

$logstatus = "Failed";
//echo $logstatus;
try {
	$inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
	$objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel    =   $objReader->load($inputFileName);
} catch (Exception $e) {
	die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getActiveSheet();
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$dataArray = array();
$row = 2;

$startRow = 2;
$highestRow=3;

echo $rowA='A'.$startRow;
for ($row = $startRow; $row <= $highestRow; $row++) {
	$cust_id = 'A' . $row;
	$nav_id = 'B' . $row;
	$business_name = 'C' . $row;
	$customer_name = 'D' . $row;
	$address1 = 'E' . $row;
	$address2 = 'F' . $row;
	$city = 'G' . $row;
	$state = 'H' . $row;
	$pincode = 'I' . $row;
	$email = 'J' . $row;
	$group = 'L' . $row;
	$phone = 'M' . $row;
	$program = 'N' . $row;
	$delivery_days = 'O' . $row;
	$delivery_freq = 'P' . $row;
	$customer_status = 'Q' . $row;
	$customer_time_zone = 'R' . $row;

	$cokeId = 'S' . $row;
	$caseSFID = 'T' . $row;
	$mobile_number = 'U' . $row;
	$nav_ach = 'V' . $row;
	$customerId = $sheet->getCell($cust_id)->getValue();
	echo $nav_cust_id = $sheet->getCell($nav_id)->getValue();
	echo $business_name = $sheet->getCell($business_name)->getValue();
	echo $customer_name = $sheet->getCell($customer_name)->getValue();
	$address1 = $sheet->getCell($address1)->getValue();
	$address2 = $sheet->getCell($address2)->getValue();
	$city = $sheet->getCell($city)->getValue();
	$state = $sheet->getCell($state)->getValue();
	$pincode = $sheet->getCell($pincode)->getValue();
	$email = $sheet->getCell($email)->getValue();
	$phone = $sheet->getCell($phone)->getValue();
	$buy_grp = $sheet->getCell($group)->getValue();
	$program = $sheet->getCell($program)->getValue();
	$delivery_days = $sheet->getCell($delivery_days)->getValue();
	$delivery_freq = $sheet->getCell($delivery_freq)->getValue();
	$customer_status = $sheet->getCell($customer_status)->getValue();
	$pc_status="23";
	$customer_time_zone = $sheet->getCell($customer_time_zone)->getValue();
	
	$cokeId = $sheet->getCell($cokeId)->getValue();
	$caseSFID = $sheet->getCell($caseSFID)->getValue();
	$mobile_number = $sheet->getCell($mobile_number)->getValue();
	$nav_ach = $sheet->getCell($nav_ach)->getValue();
$grupid ='';
	foreach ($groupOptions as $key => $value) {
		if ($value['label'] == $buy_grp) {
			$grupid = $value['value'];
		}
	}
	$resignId='';
	foreach ($regions as $key1 => $value1) {
		if ($value1['label'] == $state) {
			$resignId = $value1['value'];
		}
	}
	if (isset($customer_status)) {
		if ($customer_status == "Business_info_approved") {
			$fc_status = "8";
		} elseif ($customer_status == "Payment_info_submitted") {
			$fc_status = "9";
		} elseif ($customer_status == "Approved") {
			$fc_status = "11";
			$pc_status = "23";
		} elseif ($customer_status == "Blocked_for_invoice") {
			$fc_status = "12";
		} elseif ($customer_status == "Blocked_for_purchase") {
			$fc_status = "13";
		}elseif ($customer_status == "Business_info_submitted") {
			$fc_status = "20";
		}
	} else {
		$fc_status = "8";
	}
	//$buy_grp = $sheet->getCell( 'C'.$startRow )->getValue();
	$updatedfields='';
	try {
	$customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
	//echo'<pre>';
	//print_r($customer->getProgramId());
	if ($nav_cust_id!=$customer->getMsId()) {
		$customer->setMsId($nav_cust_id);
		$updatedfields.='NavID ,';
	}
	if ($grupid!=$customer->getGroupId()) {
		$customer->setGroupId($grupid);
		$updatedfields.='Grup ID ,';
	}
	if ($business_name!=$customer->getBusinessName()) {
		$customer->setBusinessName($business_name);
		$updatedfields .='business_name ,';
	}
	if ($fc_status!=$customer->getCustomerStatus()) {
		$customer->setCustomerStatus($fc_status);
		$customer->setPaymentStatus($pc_status);
		$updatedfields .='Customer Status ,';
	}
	if ($program!=$customer->getProgramId()) {
		$customer->setProgramId($program);
		$updatedfields .='program id ,';
	}
	if ($delivery_days!=$customer->getDeliveryDaysId()) {
		$customer->setDeliveryDaysId($delivery_days);
		$updatedfields .='delivery_days  ,';
	} 
	if ($delivery_freq!=$customer->getDeliveryFreqencyId()) {
		$customer->setDeliveryFreqencyId($delivery_freq);
		$updatedfields .='delivery_freq  ,';
	} 
	if ($customer_time_zone!=$customer->getTimeZone()) {
		$customer->setTimeZone($customer_time_zone);
		$updatedfields .='customer_time_zone  ,';
	}  
	
	if ($cokeId!=$customer->getCokeId()) {
		$customer->setCokeId($cokeId);
		$updatedfields .='coke_id ,';
	}
	if ($caseSFID!=$customer->getCaseSfid()) {
		$customer->setCaseSfid($caseSFID);
		$updatedfields .='case_sfid ,';
	}
	if ($mobile_number!=$customer->getMobileNumber()) {
		$customer->setMobileNumber($mobile_number);
		$updatedfields .='mobile_number ,';
	}
	if ($nav_ach!=$customer->getNavAch()) {
		$customer->setNavAch($nav_ach);
		$updatedfields .='nav_ach ,';
	}
	$customer->save();
	writeLog('customer Id = ' . $customerId .'For this Id Updated address Field -> '.$updatedfields);
	
} catch (Exception $exception) {
	writeLog('customer Id = ' . $customerId .'verify Data ');
	
}		
$updatedaddfields='';
	if ($customer->getAddresses()) {
		try {
		foreach ($customer->getAddresses() as $address) {
			$customerAddress[] = $address->toArray();
		}
	
		foreach ($customerAddress as $customerAddres) {
	
	
			$obj = \Magento\Framework\App\ObjectManager::getInstance();
			$address = $obj->create('\Magento\Customer\Model\Address')->load($customerAddres['entity_id']);
			if ($address->getTelephone()!= $phone) {
				$address->setTelephone($phone);
				$updatedaddfields .="phone";
			}

			if ($address->getCountryId()!= 'US') {
				$address->setCountryId('US');
				$updatedaddfields .="Country";
			}
			if ($address->getPostcode()!= $pincode) {
				$address->setPostcode($pincode);
				$updatedaddfields .="pincode";
			}
			if ($address->getCity()!= $city) {
				$address->setCity($city);
				$updatedaddfields .="city";
			}
			if ($address->getRegionId()!= $resignId) {
				$address->setRegionId($resignId);
				$updatedaddfields .="state";
			}
			if ($address->getRegion()!= $state) {
				$address->setRegion($state);
				$updatedaddfields .="state";
			}
			$adata =array(
				'0' => $address1, 
				'1' => $address2  
			);
			$diffstreet=array_diff($address->getStreet(),$adata);
			if ($diffstreet) {
				$address->setStreet(array(
					'0' => $address1, 
					'1' => $address2  
				));
				$updatedaddfields .="street";
			}
			
			$address->setSaveInAddressBook('1')
				->setIsDefaultShipping('1')
				->setIsDefaultBilling('1')
				->save();

		writeLog('customer Id = ' . $customerId .'For this Id Updated Field -> '.$updatedaddfields);
			
		}
	} catch (Exception $exception) {
		writeLog('customer Id = ' . $customerId .print_r($exception->getMessage(), true));
	}
	}else{
		try {

			echo 'Succesfully Saved' . $customer->getId();
			// Add Address For created customer
			$object_addres = $objectManager->get('\Magento\Customer\Model\AddressFactory');
			$set_address = $object_addres->create();
	
			$set_address->setCustomerId($customer->getId())
			->setFirstname($customer->getFirstname())
			->setLastname($customer->getLastname())
			->setCountryId('US')
			// if Customer country is USA then need add state / province
			->setRegionId($resignId)
			->setPostcode($pincode)
			->setCity($city)
			->setTelephone($phone)
			->setStreet(array(
							'0' => $address1, // Required
							'1' => $address2  // optional
						))
			->setIsDefaultBilling('1')
			->setIsDefaultShipping('1')
			->setSaveInAddressBook('1');
			
				$set_address->save();

				writeLog('customer Id = ' . $customerId .'Customer all addrss field Updated ');
				// save Customer address
			$logstatus = "Success";
		} catch (Exception $exception) {
		
			// error message
			writeLog('customer Id = ' . $customerId .print_r($exception->getMessage(), true));
		}
	}
  $sql1 = "INSERT INTO `".$customer_import."` (`Customerid`, `Filename`, `Status`, `Navcustomerid`) VALUES ('".$customerId."', '".$keyname."', '".$logstatus."', '".$nav_cust_id."');";
       

$connection->query($sql1);

}
//$done = 'hii.xlsx';
rename($inputFileName, $outputFileName);
rename($url,$done_url);
writeLog('Record inserted Successfully');
}

function writeLog($message, $logfile = 'nav_customer.log')
{
	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
	$logger = new \Zend\Log\Logger();
	$logger->addWriter($writer);
	$logger->info($message);
}