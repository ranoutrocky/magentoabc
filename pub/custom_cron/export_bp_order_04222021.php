<?php

use Magento\Framework\App\Bootstrap;
include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');
require_once('../../vendor/autoload.php');

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;
use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\Charge;
use Magento\Framework\App\Filesystem\DirectoryList;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');


 $state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('Psr\Log\LoggerInterface');
$storeManager->info('Magecomp Log');

$storeManager=$objectManager->get('Magento\Store\Model\StoreManagerInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$request = $objectManager->get(Magento\Framework\App\Request\Http::class);

$connection = $resource->getConnection();
$orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 

$quoteToOrder = $objectManager
            ->create(
                '\Magento\Quote\Model\Quote\Item\ToOrderItem'
            );
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$invoiceFileName = $helper->getInvoiceName();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();

$connection = $resource->getConnection();
$bucket = 'esprigas.beverage.portal';
$source_path=BP.'/var/NAV/Orders/';
$dest_path=BP.'/var/NAV/Orders/Archived/';


$yesterday =  date('Y-m-d',strtotime("-1 days"));
$today =  date('Y-m-d');
$payment_date=date('Y-m-d',strtotime("+1 days"));

$order_import = $resource->getTableName('esprimodule_customimportexport_order');

// $fileName = 'BP_to_NAV_Content_Orders_'.date('mdYHis').'.csv';

// $s3_file_url = 's3://' . $bucket . '/Orders/BP/' . $fileName;

  
$s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
       'key'    => $s3key,
       'secret' => $s3secretkey,
    ]
]);
$s3->registerStreamWrapper();
 
 $order_import = $resource->getTableName('esprimodule_customimportexport_order');

 $fileName = 'BP_to_NAV_Content_Orders_'.date('mdYHis').'.csv';

 $s3_file_url = 's3://' . $bucket . '/Orders/BP/' . $fileName;

// $inputFileName = $source_path.$fileName ;
//  $archiveFileName = $dest_path.$fileName ;

 
	
	 try {
	$objPHPExcel = new PHPExcel();
   $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV')->setDelimiter(',')
                                            ->setEnclosure('')
                                            ->setLineEnding("\r\n");
											 
 $max_order_id = 0;										 
 $query = "Select max(to_order_id) FROM esprimodule_customimportexport_order";
 $records= $connection->fetchAll($query);
 
 foreach ($records as $record1)
    {
		$max_order_id =  $record1['max(to_order_id)'];
	}

		echo	$query_order = "SELECT so.entity_id,DATE_FORMAT(so.created_at,'%Y-%m-%d') AS order_date,'' AS po_number, cev.value AS 'Customer No', so.customer_email As 'Customer_Email',
				   so.customer_id AS 'BP Customer ID',so.increment_id AS 'BP Order Number',soi.sku AS 'BP Order Item Number',
				   0 AS 'LineNumber',soi.qty_ordered AS 'Quantity',
				   so.shipping_method AS 'shipping_method'
				   FROM
				   sales_order so INNER JOIN sales_order_item soi ON so.entity_id=soi.order_id
				   INNER JOIN customer_entity_varchar cev ON cev.entity_id=so.customer_id AND cev.attribute_id=143
				   WHERE so.entity_id>$max_order_id";

		$recordarray = $connection->fetchAll($query_order);
		$firstid='';
		$lastorderid='';
		$row = 1;
		$prev_order='0';
		$i=1;


		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,'CustomerNo')
                              ->setCellValue('B'.$row,  'Customer_Email')
                              ->setCellValue('C'.$row, 'PO_Number')
                              ->setCellValue('D'.$row, 'Order_Date')
                              ->setCellValue('E'.$row, 'BP_Customer_ID')
							  ->setCellValue('F'.$row, 'BP_Order_Number')
                              ->setCellValue('G'.$row, 'Quantity')
                              ->setCellValue('H'.$row, 'LineNumber')
                              ->setCellValue('I'.$row, 'BP_Order_Item_Number')
							  ->setCellValue('J'.$row, 'Description')
                              ->setCellValue('K'.$row, 'Rush_Delivery');
    foreach ($recordarray as $record)
    {
		$row ++;
		if($row == 2){
			 echo $firstid = $record['entity_id'];
		}
		
		if($record['shipping_method'] =="flatrate_flatrate"){
           $rush = "Yes";
        }else{
           $rush = "No";
        }
		
	if($prev_order!=$record['BP Order Number'])
	{
		$i=1;
		$prev_order = $record['BP Order Number'];
		$fileName = 'BP_to_NAV_Content_Orders_'.date('mdYHis').'-'.$record['BP Order Number'].'.csv';
		$s3_file_url = 's3://' . $bucket . '/Orders/BP/' . $fileName;
		$inputFileName = $source_path.$fileName ;
		$archiveFileName = $dest_path.$fileName ;

	}
	else {
		 $i++;
	}	
	
	$date = date_create($record['order_date']);
	$date1=date_format($date,"Y-m-d");	
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$row,$record['Customer No'])
                              ->setCellValue('B'.$row,  $record['Customer_Email'])
                              ->setCellValue('C'.$row, '')
                              ->setCellValue('D'.$row,$date1 )
                              ->setCellValue('E'.$row, $record['BP Customer ID'])
							   ->setCellValue('F'.$row, $record['BP Order Number'])
                              ->setCellValue('G'.$row, round($record['Quantity']))
                              ->setCellValue('H'.$row, $i)
                              ->setCellValue('I'.$row,$record['BP Order Item Number'])
							   ->setCellValue('J'.$row, $record['BP Order Item Number'])
                              ->setCellValue('K'.$row,$rush);
							  
							  
		$lastorderid = $record['entity_id'];
		
		
		echo '<br/>'.'BP Order Date: '.$date1. ' BP Customer ID: '.$record['BP Customer ID'].' NAV Customer No: '. $record['Customer No'].' BP Order Number: '.$record['BP Order Number'].' Line Number: '.$i.' BP Order Item Number: '.$record['BP Order Item Number'].' BP Quantity: '.round($record['Quantity']).' Rush Delivery: '.$rush.'<br/>';
		
									  
	}	

						   if($firstid!=""){
        $sql1 = "Insert Into esprimodule_customimportexport_order  (from_order_id, to_order_id) Values ($firstid,$lastorderid)";
      $connection->query($sql1);
	  
	   $objWriter->save($inputFileName);
							  
							 $dataS3=file_get_contents($inputFileName);
	                         file_put_contents($s3_file_url,$dataS3);
							 writeLog('done file'.$inputFileName);
							 rename($inputFileName,$archiveFileName);
	  
      } 
	  
							 
   
	
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}
			

function writeLog($message, $logfile = 'bp_export_ORDER.log')
{
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);
    $logger->info($message);
}
