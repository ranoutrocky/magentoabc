<?php

use Magento\Framework\App\Bootstrap;include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');
require_once('../../vendor/autoload.php');

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;
use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\Charge;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');


 $state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('Psr\Log\LoggerInterface');
$storeManager->info('Magecomp Log');

$storeManager=$objectManager->get('Magento\Store\Model\StoreManagerInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$request = $objectManager->get(Magento\Framework\App\Request\Http::class);

$connection = $resource->getConnection();
$orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 

$quoteToOrder = $objectManager
            ->create(
                '\Magento\Quote\Model\Quote\Item\ToOrderItem'
            );
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$invoiceFileName = $helper->getInvoiceName();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();

$connection = $resource->getConnection();

$source_path='/var/www/html/coke-portal-dev/var/NAV/Invoices/';
$dest_path='/var/www/html/coke-portal-dev/var/NAV/Invoices/Archived/';
$paramVal=$request->getParam('process');
$fileName='NAV_to_BP_Content_Invoices';
$processMsgVal='<br/>'.'Processing Content Invoices:'.'<br/>';
if($paramVal=='rental')
{
	$fileName='NAV_to_BP_Rental_Invoices';
	$processMsgVal='<br/>'.'Processing Rental Invoices:'.'<br/>';
}
else if ($paramVal=='content' || $paramVal=='')
{
	$fileName='NAV_to_BP_Rental_Invoices';
	$processMsgVal='<br/>'.'Processing Content Invoices:'.'<br/>';
}
else
{
	echo 'Invalid Parameter passed. Please provide either no parameter or content or rental';
	exit;
}


$keyname = $fileName.'.xlsx';
$bucket = 'esprigas.beverage.portal';



 $s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
        'key'    => $s3key,
        'secret' => $s3secretkey,
    ]
]);
$s3->registerStreamWrapper();

$fileArr=[];
$dir='s3://'.$bucket.'/Invoices/NAV/';
echo $dir;
if (is_dir($dir) && ($dh = opendir($dir))) {
    while (($file = readdir($dh)) !== false) {
	$path=pathinfo($file);
	print_r($path);
	if (!in_array('Archived',$path))
	{
	 if((strpos($file,'.xlsx')>0) )
	 {
		array_push($fileArr,$file);	 
	 }
    }
	}
    closedir($dh);

}

 foreach ($fileArr as &$keyname) {
$var = $keyname;
$inputFileName=$source_path.$var;
$outputFileName=$dest_path.$var.'-'.$date;

$url = 's3://'.$bucket.'/Invoices/NAV/'.$keyname;
$done_url='s3://'.$bucket.'/Invoices/NAV/Archived/'.$keyname.'-'. $date;
echo $url;
try {
	$dataS3=file_get_contents($url);
	file_put_contents($inputFileName,$dataS3);
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
$outputFileName = '/var/www/html/coke-portal-dev/var/NAV/Invoices/Done/' . $keyname . '-' . $date;


$order_import = $resource->getTableName('esprimodule_orderimportdata_orderimportdata'); //gives table name with 

$query = "EXISTS(SELECT * from ".$order_import." WHERE Filename='".$var."')";
$sql2 = "SELECT ".$query."";

$isfile = $connection->fetchAll($sql2);
/*
if ($isfile[0][$query]!=0) {
  //continue;
  echo $var."  Filename allready updated";
  exit;
}
*/
// echo 'test';
$filenamexls = $var;


try {
    $inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
    $objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel    =   $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getActiveSheet();
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
$dataArray = array();
$row = 2;

$startRow = 2;
//$highestRow=3;
// $prev_magento_order_id='';
$newrow_numbers = 2;
for ($row = $startRow; $row <= $highestRow; $row++) {
	$invoice_no = 'A' . $row;
	$invoice_no = trim($sheet->getCell($invoice_no)->getValue());
	if ($invoice_no!="") {
	//	$newrow_numbers++;
	$newrow_numbers=$row;
	}else{
		break;
	}

 }

echo $highestRow = $newrow_numbers;



$ordersarray = [];
$rowA = 'A' . $startRow;
for ($row = $startRow; $row <= $highestRow; $row++) {
	//echo  $row;
    $invoice_no = 'A' . $row;
    $po_no = 'C' . $row;
    $dday = 'D' . $row;
    $dueday = 'E' . $row;
    $nav_cus_id = 'F' . $row;
    $magento_cus_id = 'G' . $row;
    $magento_order_id = 'I' . $row;
    $magento_order_id = trim($sheet->getCell($magento_order_id)->getValue());
	$magento_order_id = '';
    $line_number = 'K' . $row;
    $line_number = trim($sheet->getCell($line_number)->getValue());
    $sku = 'L' . $row;
    $qty = 'N' . $row;
    $sku = trim($sheet->getCell($sku)->getValue());
    $qty = trim($sheet->getCell($qty)->getValue());
    $unit_price = 'O' . $row;
    $line_amt = 'P' . $row;
    $subtotel = 'Q' . $row;
    $freight = 'R' . $row;
    $tax = 'S' . $row;
    $totel = 'T' . $row;

    $invoice_no = trim($sheet->getCell($invoice_no)->getValue());
    $po_no = trim($sheet->getCell($po_no)->getValue());
    $nav_cus_id = trim($sheet->getCell($nav_cus_id)->getValue());
    $magento_cus_id = trim($sheet->getCell($magento_cus_id)->getValue());
    $unit_price = trim($sheet->getCell($unit_price)->getValue());
    $line_amt = trim($sheet->getCell($line_amt)->getValue());
    $freight = trim($sheet->getCell($freight)->getValue());
    $tax = trim($sheet->getCell($tax)->getValue());
    $dday = trim($sheet->getCell($dday)->getValue());
    $dueday = trim($sheet->getCell($dueday)->getValue());
    $totel = trim($sheet->getCell($totel)->getValue());
    $subtotel = trim($sheet->getCell($subtotel)->getValue());

    $olditme = '';
    $oldqty = '';
    $newitem = '';
    $newitemqty = '';


    if ($magento_order_id != "") {

        if (!empty($ordersarray)) {

            if (array_key_exists($magento_order_id, $ordersarray)) {
                $olditme = $ordersarray[$magento_order_id]['items'];
                $oldqty = $ordersarray[$magento_order_id]['qty'];
                $updateditem = $olditme . "," . $sku;
                $updatedsku = $oldqty . "," . $qty;
                $ordersarray[$magento_order_id]['items'] = $updateditem;
                $ordersarray[$magento_order_id]['qty'] = $updatedsku;
            } else {
                $ordersarray[$magento_order_id] = array("order_id" => $magento_order_id, "items" => $sku, "qty" => $qty, 'shipping_charge' => $freight, 'tax' => $tax, 'invoice_total' => $totel, 'subtotal' => $subtotel);
            }
        } else {
            $ordersarray[$magento_order_id] = array("order_id" => $magento_order_id, "items" => $sku, "qty" => $qty, 'shipping_charge' => $freight, 'tax' => $tax, 'invoice_total' => $totel, 'subtotal' => $subtotel);
        }
        if ($row == $highestRow) {

            createinvoice($ordersarray,$objectManager,$invoice_no,$magento_cus_id,$filenamexls);
		//	exit;
       //     rename($inputFileName, $outputFileName);

            writeLog('Record inserted Successfully');
            echo "success";
        }
    } else {

    		if (!empty($newordersarray)) {

            if (array_key_exists($invoice_no, $newordersarray)) {
                $olditme = $newordersarray[$invoice_no]['items'];
                $oldqty = $newordersarray[$invoice_no]['qty'];
                $updateditem = $olditme . "," . $sku;
                $updatedsku = $oldqty . "," . $qty;
                $newordersarray[$invoice_no]['items'] = $updateditem;
                $newordersarray[$invoice_no]['qty'] = $updatedsku;
            } else {
                $newordersarray[$invoice_no] = array("customer_id" => $magento_cus_id, 'items' => $sku, 'qty' => $qty, 'shipping_charge' => $freight, 'tax' => $tax, 'invoice_total' => $totel, 'subtotal' => $subtotel);
            }
        } else {
            $newordersarray[$invoice_no] = array("customer_id" => $magento_cus_id, 'items' => $sku, 'qty' => $qty, 'shipping_charge' => $freight, 'tax' => $tax, 'invoice_total' => $totel, 'subtotal' => $subtotel);
        }


    	// $ordersarray_for_new[] = array("customer_id" => $magento_cus_id, 'items' => $sku, 'qty' => $qty, 'shipping_charge' => $freight, 'tax' => $tax, 'invoice_total' => $totel, 'subtotal' => $subtotel ,'invoiceid' => $invoice_no);
        

        if ($row == $highestRow) {
        	ordercreatefornew($newordersarray,$filenamexls);
            createinvoice($ordersarray,$objectManager,$invoice_no,$magento_cus_id,$filenamexls);
          //   rename($inputFileName, $outputFileName);
             writeLog('Record inserted Successfully');
             echo "success";
        }
       
            }
				//	rename($url,$done_url);
}

}


function ordercreatefornew($ordersarray_for_new,$filenamexls)
{

	foreach ($ordersarray_for_new as $key => $updateData) {
		if ($key=="") {
			continue;
		}
		echo "<pre>";
	print_r($updateData);
	$invoice_no = $key;
	$bootstrap = Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
    $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
    $date = $objDate->gmtDate();
    $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();
    $regionCollection = $objectManager->create('\Magento\Directory\Model\Country')->loadByCode('US')->getRegions();
    $regions = $regionCollection->loadData()->toOptionArray(false);
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $store = $storeManager->getStore();
    $websiteId = $storeManager->getStore()->getWebsiteId();
    $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');


   // $productObj = $productRepository->get($updateData['items']);

// echo '<pre>'; echo 'create a new order here'; echo '</pre>';
    $magento_cus_id = $updateData['customer_id'];
$stripeCustomerId = "";
if (isset($updateData['customer_id'])) {
 $customerId = $updateData['customer_id'];
 $customerData = $objectManager->create('\Magento\Customer\Model\Customer')->load($customerId);
 $resourceConnection = $objectManager
                 ->get(
                     '\Magento\Framework\App\ResourceConnection'
                 );
 $connection  = $resourceConnection->getConnection();
       $tableName = $connection->getTableName('stripe_customers');
 //check customer stripe data then only create order
/* $query = $connection->select()
         ->from($tableName,['customer_id','stripe_id'])
         ->where('customer_id = ?', $updateData['customer_id']);
		 */
		 
		 $query='select customer_id,stripe_id from stripe_customers where customer_id ='.$updateData['customer_id'].' order by id desc limit 1';

 $fetchData = $connection->fetchAll($query);
 if (!empty($fetchData) && isset($fetchData[0]['stripe_id'])) {
  $stripeCustomerId = $fetchData[0]['stripe_id'];
 }
 echo '$stripeCustomerId'.$stripeCustomerId;
 if ($customerData && $customerData->getId() && $stripeCustomerId != "") {
	
    $callback = createMageOrder($updateData,$stripeCustomerId,$objectManager);
    if (isset($callback['order_id'])) {
     $updateData['order_id'] = $callback['order_id'];
     $order = $objectManager->get('\Magento\Sales\Model\Order')->load($callback['order_id']);
     
     if (isset($updateData['shipping_charge'])) {
         $order->setShippingAmount($updateData['shipping_charge']);
         $order->setBaseShippingAmount($updateData['shipping_charge']);
     }
     if (isset($updateData['tax'])) {
         $order->setTaxAmount($updateData['tax']);
         $order->setBaseTaxAmount($updateData['tax']);
     }
     $quote = $objectManager
         ->create(
             '\Magento\Quote\Model\Quote'
         )->load($order->getQuoteId());
     $order->setSubtotal($quote->getSubtotal())
         ->setBaseSubtotal($quote->getBaseSubtotal())
         ->setGrandTotal($quote->getGrandTotal())
         ->setBaseGrandTotal($quote->getBaseGrandTotal());

     $payment = $order->getPayment();
     $payment->setMethod('stripe_payments');
     $payment->save();
     $order->save();
     $order = $objectManager->get('\Magento\Sales\Model\Order')->load($order->getId());
     createshipment($order);
     echo "===create invoice part";
     $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$lstorder = $objectManager->get('Magento\Sales\Model\Order')->getCollection()->addFieldToFilter('customer_id',$updateData['customer_id'])->setOrder('created_at','DESC')->getFirstItem();
$isNewOrder=true;
     $flag = prepareInvoice($order,$updateData,$stripeCustomerId,$objectManager,$lstorder,$invoice_no,$magento_cus_id,$order->getId(),$filenamexls,$isNewOrder);
    }
    else {
        $result['msg'] = "please check data";
     echo '<pre>'; print_r($result['msg']); echo '</pre>';exit;
    }
 }
 else {
     if ($stripeCustomerId == "") {
         echo '<pre>'; echo 'stripeCustomerId is not avaliable pls check'; echo '</pre>';exit;
     }
     else {
         echo '<pre>'; echo 'customer is not avaliable pls check'; echo '</pre>';exit;
     }
 }  
}
	}
	

}
function createinvoice($ordersarray,$objectManager,$invoice_no,$magento_cus_id,$filenamexls){
    echo "<pre>";
   // print_r($ordersarray);
    $order = null;
    $StripeCustomermodel = $objectManager->get('StripeIntegration\Payments\Model\StripeCustomer');
    $helper = $objectManager->get('StripeIntegration\Payments\Helper\Generic');
    $loadproduct = $objectManager->create('\Magento\Catalog\Model\Product');
    $productFactory = $objectManager
            ->create(
                '\Magento\Catalog\Model\ProductFactory'
            );
    $quoteToOrder = $objectManager
            ->create(
                '\Magento\Quote\Model\Quote\Item\ToOrderItem'
            );
$orderObject = $objectManager->get('\Magento\Sales\Model\Order');
foreach ($ordersarray as $key => $updateData) {
    if (isset($updateData['order_id'])) {
        echo "=".$updateData['order_id'];
        $stringsku = str_replace(' ', '', $updateData['order_id']);
        echo '$stringsku'.$stringsku;
        $order = $orderObject->loadByIncrementId($stringsku);
        //$order = $orderObject->load($updateData['order_id']);
    //   echo $order->getId();
        
        if ($order->getId() && $order) {

            $additionalInformation = $order->getPayment()->getAdditionalInformation();
            $paycode = $order->getPayment()->getMethod();
            	$repeat=0;
            	firstUpdate($order,$objectManager,$updateData,$productFactory,$quoteToOrder,$repeat);
            //	exit;
          
            $customerId = $order->getCustomerId();
			// echo $customerId;
            $customerData = $objectManager->create('\Magento\Customer\Model\Customer')->load($customerId);
            $resourceConnection = $objectManager
                            ->get(
                                '\Magento\Framework\App\ResourceConnection'
                            );
            $connection  = $resourceConnection->getConnection();
                  $tableName = $connection->getTableName('stripe_customers');
            //check customer stripe data then only create order
            $query = $connection->select()
                    ->from($tableName,['customer_id','stripe_id'])
                    ->where('customer_id = ?', $customerId);
    
            $fetchData = $connection->fetchAll($query);
            $stripeCustomerId = "";
        if (!empty($fetchData) && isset($fetchData[0]['stripe_id'])) {
         $stripeCustomerId = $fetchData[0]['stripe_id'];
        }
		// echo '$paycode'.$paycode;
		// echo '$order->canInvoice()'.$order->canInvoice();
     //  echo '$stripeCustomerId'.$stripeCustomerId;
            if ($order->canInvoice() && $paycode == "stripe_payments" /*&& !empty($additionalInformation) && isset($additionalInformation['customer_stripe_id']*/ && $stripeCustomerId != "") {
               // $stripeCustomerId = $additionalInformation['customer_stripe_id'];
			//   echo 'inv';
                $lstorder="no";
                $flag = prepareInvoice($order,$updateData,$stripeCustomerId,$objectManager,$lstorder,$invoice_no,$magento_cus_id,$stringsku,$filenamexls);
            }
        }else{
            echo "Order Id not valid .. !";
            
        }
    }
    
    }
}
function firstUpdate($order,$objectManager,$updateData,$productFactory,$quoteToOrder,$repeat)
{
	$quote = $objectManager
                ->create(
                    '\Magento\Quote\Model\Quote'
                )->load($order->getQuoteId());
            $csvSkus = explode(",", $updateData['items']);
            $qtys = explode(",", $updateData['qty']);
            $releventqty = [];
            if (!empty($csvSkus) && !empty($qtys)) {
               foreach ($csvSkus as $key => $value) {
                   if (isset($qtys[$key])) {
                        $releventqty[$value] = $qtys[$key];
                   }
               }
            }
            $updateItems = [];
        $newAddItems = $oldSku = [];
        foreach ($quote->getAllItems() as $quoteItem) {
            if (in_array($quoteItem->getSku(), $csvSkus)) {
                $updateItems[$quoteItem->getId().'_'.$quoteItem->getSku()] = $quoteItem->getId();
                $oldSku[$quoteItem->getSku()] = $quoteItem->getSku();
            }
        }
        foreach ($csvSkus as $key => $value) {
           if (!in_array($value, $oldSku) && isset($releventqty[$value])) {
               if ($releventqty[$value] > 0) {
                   $newAddItems[$value] = $releventqty[$value];
               }
           }
        }
        if (!empty($updateItems) && !empty($releventqty)) {
            foreach ($quote->getAllItems() as $quoteItem) {
                if (in_array($quoteItem->getId(), $updateItems)) {
                    if (isset($releventqty[$quoteItem->getSku()])) {
                        if ($releventqty[$quoteItem->getSku()] > 0) {
                            $quoteItem->setQty($releventqty[$quoteItem->getSku()]);
                        }
                        else {
                            unset($updateItems[$quoteItem->getId().'_'.$quoteItem->getSku()]);
                             $item->delete();
                        }
                    }
                }
            }
        }
		/*
		else{
        	foreach ($quote->getAllItems() as $quoteItem) {
        		
        		$quoteItem->delete();
        	}
		
        }
		*/
        $productModel = $productFactory->create();
        if (!empty($newAddItems)) {
            foreach ($newAddItems as $itemsku => $itemqty) {
                $existingProductID = $productModel->getIdBySku($itemsku);
                if ($existingProductID) {
                	$loadproduct = $objectManager->create('\Magento\Catalog\Model\Product');
                    $product = $loadproduct->load($existingProductID);
                    if ($product && $product->getId()) {
                       $quote->addProduct($product, intval($itemqty));
                    }
                }
            }
        }
        if (!$quote->isVirtual()) {
            $total = $quote->getShippingAddress();
            if (isset($updateData['shipping_charge'])) {
                $total->setShippingAmount($updateData['shipping_charge']);
                $total->setBaseShippingAmount($updateData['shipping_charge']);
            }
            if (isset($updateData['tax'])) {
                $total->setTaxAmount($updateData['tax']);
                $total->setBaseTaxAmount($updateData['tax']);
            }
            $total->setSubtotal($updateData['subtotal']);
            $total->setBaseSubtotal($updateData['subtotal']);
            $total->setGrandTotal($updateData['invoice_total']);
            $total->setBaseGrandTotal($updateData['invoice_total']);
            $quote->collectTotals();
            $quote->save();
        }
        $quote->setSubtotal($updateData['subtotal']);
        $quote->setBaseSubtotal($updateData['subtotal']);
        $quote->setGrandTotal($updateData['invoice_total']);
        $quote->setBaseGrandTotal($updateData['invoice_total']);
        $quote->save();
        foreach ($quote->getAllItems() as $quoteItem) {
            if (in_array($quoteItem->getId(), $updateItems)) {
                $orderItem = $quoteToOrder->convert($quoteItem);
                $origOrderItemNew = $order->getItemByQuoteItemId($quoteItem->getId());
                if ($origOrderItemNew) {
                    $origOrderItemNew->addData($orderItem->getData());
                } else {
                    if ($quoteItem->getParentItem()) {
                        $orderItem->setParentItem(
                            $order->getItemByQuoteItemId($orderItem->getParentItem()->getId())
                        );
                    }
                    $order->addItem($orderItem);
                }
            }
        }
        foreach ($order->getAllItems() as $orderItem) {
            $getQuoteItemId = $orderItem->getQuoteItemId();
            if (in_array($getQuoteItemId, $updateItems) || in_array($orderItem->getProduct()->getSku(), $newAddItems)) {
            }
            else {
                $orderItem->isDeleted(true);
            }
        }
        if (isset($updateData['shipping_charge'])) {
            $order->setShippingAmount($updateData['shipping_charge']);
            $order->setBaseShippingAmount($updateData['shipping_charge']);
        }
        if (isset($updateData['tax'])) {
            $order->setTaxAmount($updateData['tax']);
            $order->setBaseTaxAmount($updateData['tax']);
        }
        $order->setSubtotal($quote->getSubtotal())
            ->setBaseSubtotal($quote->getBaseSubtotal())
            ->setGrandTotal($quote->getGrandTotal())
            ->setBaseGrandTotal($quote->getBaseGrandTotal());
        $quote->save();
        $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true)->save();
        $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING, true)->save();
        $order->save();
            if ($repeat==1 || $repeat==0) {
            	$repeat =$repeat+1;
            	firstUpdate($order,$objectManager,$updateData,$productFactory,$quoteToOrder,$repeat);
            }
}

function createshipment($order){
    $bootstrap = Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    // $order = $objectManager->create('Magento\Sales\Model\Order')
    // ->loadByAttribute('increment_id', $incrementId);

$incrementId =$order->getIncrementId();
if ($order->canShip()) {
    
// Initialize the order shipment object
$convertOrder = $objectManager->create('Magento\Sales\Model\Convert\Order');
$shipment = $convertOrder->toShipment($order);
// Loop through order items
foreach ($order->getAllItems() AS $orderItem) {
    // Check if order item has qty to ship or is virtual
    if (! $orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
        continue;
    }
    $qtyShipped = $orderItem->getQtyToShip();
    // Create shipment item with qty
    $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
    // Add shipment item to shipment
    $shipment->addItem($shipmentItem);
}

// Register shipment
$shipment->register();
$shipment->getOrder()->setIsInProcess(true);

try {
    // Save created shipment and order
    $shipment->save();
    $shipment->getOrder()->save();

    // Send email
    //$objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
    //    ->notify($shipment);
    //$shipment->save();
} catch (\Exception $e) {
   echo "Shipment Not Created". $e->getMessage(); exit;
}

echo "Shipment Succesfully Generated for order: #".$incrementId;
} else {
echo "Shipment Not Created Becuase It's already created or something went wrong";
}
}
function prepareInvoice($order,$updateData,$stripeCustomerId,$objectManager,$lstorder,$invoice_no,$magento_cus_id,$stringsku,$filenamexls,$isNewOrder=false) {
    //create invoice part
	echo 'prepare invoice';
	$helper=null;
    $orderRepo = $objectManager->get('\Magento\Sales\Api\OrderRepositoryInterface');
    $invoiceService = $objectManager->get('\Magento\Sales\Model\Service\InvoiceService');
    $transaction = $objectManager->get('\Magento\Framework\DB\Transaction'); 
    $invoiceSender = $objectManager->get('\Magento\Sales\Model\Order\Email\Sender\InvoiceSender'); 
    //$updatedOrder = $orderRepo->get($order->getId());
    $updatedOrder = $objectManager->get('\Magento\Sales\Model\Order')->load($order->getId());
    $paycode = $updatedOrder->getPayment()->getMethod();
    $StripeCustomermodel = $objectManager->get('StripeIntegration\Payments\Model\StripeCustomer');
    $helper = $objectManager->get('StripeIntegration\Payments\Helper\Generic');


    $logstatus = "Failed";
//	$stripeCustomerId ='';
/*
    if ($lstorder!="no") {
        $additionalInformation = $lstorder->getPayment()->getAdditionalInformation();
        $paycode = $lstorder->getPayment()->getMethod();
    }else{
        $additionalInformation = $order->getPayment()->getAdditionalInformation();
        $paycode = $order->getPayment()->getMethod();
    }
	*/
       $additionalInformation = $order->getPayment()->getAdditionalInformation();
        $paycode = $order->getPayment()->getMethod();
$cardFlag='';

// echo $updatedOrder->canInvoice().'test';
// exit;
    //echo '<pre>'; print_r($paycode); echo '</pre>';exit;
    if (($isNewOrder)|| ($updatedOrder->canInvoice() && $paycode == "stripe_payments" && !empty($additionalInformation) && isset($additionalInformation['customer_stripe_id']))) {
		if(!$isNewOrder)
        $stripeCustomerId = $additionalInformation['customer_stripe_id'];
		echo '<br/> Stripe Cust ID:'. $stripeCustomerId.'<br/>' ;
      //  $_stripeCustomer = $StripeCustomermodel->retrieveByStripeID($stripeCustomerId);
        $cardFlag = "";
		$cardFlag=getCustomerPaymentMethod($stripeCustomerId);
       /* if ($_stripeCustomer) {
            $cards =  $helper->listCards($_stripeCustomer, []);
            if (!empty($cards)) {
                foreach ($cards as $key => $value) {
                    $cardFlag = $value->id;
                    break;
                }
            }
        }
		*/
		 echo '<br/>'. $cardFlag ;
        if ($cardFlag != "") {
			$secKey=  'sk_test_51IDw2JKzsQLdHgSO5cTcWkyAnAm9yDy3i6zqgDrCu1LdlFo1UY91lflrbF60n6sssjgoY3w30Mmaa6wG21UMitJh004ZJNY14B';

            \Stripe\Stripe::setApiKey($secKey);
            if ($updatedOrder->canInvoice()) {
                $request = [
                    'amount'   => $updateData['invoice_total'] * 100, 
                    'currency' => $order->getOrderCurrencyCode(), 
                    'customer' => $stripeCustomerId,
                    'payment_method' => $cardFlag,
                    'confirm' => true,
                    'description' => $order->getIncrementId(), 
                  ];
                          $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/requestrequest.log');
                          $logger = new \Zend\Log\Logger();
                          $logger->addWriter($writer);
                          $logger->info('request');
                          $logger->log(5,print_r($request,true));
              //    echo '<pre>'; print_r($request); echo '</pre>';
                $charge =  \Stripe\PaymentIntent::create([$request]);
			//	echo '<pre>'; print_r($charge); echo '</pre>';exit;
                // $stripe->paymentIntents->confirm(
                //     'pi_1IT9HfKzsQLdHgSOL8cUW48j',
                //     ['payment_method' => 'pm_card_visa']
                //   );
                $transactionData = array();
                $flag = isset($charge['charges']['data'][0]);
                
				if ($flag) {
                    $chargeJson = $charge['charges']['data'][0]->jsonSerialize();
                    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
                    {
					//	echo '<pre>'; print_r($charge['charges']['data'][0]->jsonSerialize()); echo '</pre>';exit;
						     $payment = $order->getPayment();
  $payment->setMethod('stripe_payments');
  $payment->save();
     $order->save();
	 $invoice = $invoiceService->prepareInvoice($updatedOrder);
                        $invoice->setShippingAmount($updateData['shipping_charge']);
                        $invoice->setSubtotal($updateData['subtotal']);
						$invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
                        $invoice->setBaseSubtotal($updateData['subtotal']);
                        $invoice->setGrandTotal($updateData['invoice_total']);
                        $invoice->setBaseGrandTotal($updateData['invoice_total']);
                        $order->setTotalPaid($updateData['invoice_total']); 
                        $invoice->register();
                        $invoice->save();
                        $transactionSave = $transaction->addObject(
                            $invoice
                        )->addObject(
                            $invoice->getOrder()
                        );
						//04.01.2021 - Update payment info -start
 $resourceConnection = $objectManager
                 ->get(
                     '\Magento\Framework\App\ResourceConnection'
                 );
 $connection  = $resourceConnection->getConnection();					
					$pymenttable = $connection->getTableName('esprimodule_orderimportdata_orderimportdata');
                        $savetrns_id = "UPDATE sales_order_payment SET last_trans_id = '".$chargeJson['payment_method']."' WHERE sales_order_payment.entity_id = ".$order->getId();
                        $connection->query($savetrns_id);
						
						//04.01.2021 - Update payment info -end
                        $transactionSave->save();
                     echo '<pre>'; echo 'invoice done invoice id-->'. $invoice->getId(); echo '</pre>';
                     if ($invoice->getId()) {
                       $logstatus = "Success";
                     }
                     $order->setState(\Magento\Sales\Model\Order::STATE_COMPLETE, true)->save();
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE, true)->save();
                     
                    }
                    else {
                        echo '<pre>'; echo 'error in gateway or customer card does not have sufficent balance or customer do not have valid cards'; echo '</pre>';
                     //   echo '<pre>'; print_r($chargeJson); echo '</pre>';exit;
                    }
                }
            }
        }
    }
    
//     $order_import = $resource->getTableName('esprimodule_orderimportdata_orderimportdata'); //gives table name with 

//     $sql2 = "INSERT INTO `".$order_import."` (`Orderid`, `Invoiceid`, `Customerid`, `Filename`, `Status`) VALUES ('".$stringsku."', '".$invoice_no."', '".$magento_cus_id."', '".$filenamexls."', '".$logstatus."');";

// $connection->query($sql2);
}

function createMageOrder($updateData,$stripeCustomerId,$objectManager) {
    $_storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $customerRepository = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface');
    $customerId = $updateData['customer_id'];
    $customer = $objectManager->create('\Magento\Customer\Model\Customer')->load($customerId);
	 $groupid = $customer->getGroupId();
    if ($groupid == '6') {
        $shipping_method = 'freeshiping_freeshiping';
    }else{
        $shipping_method = 'flatrate_flatrate';
    }

    $addressFactory = $objectManager->get('\Magento\Customer\Api\AddressRepositoryInterface');
    $shippingAddressId = $customer->getDefaultShipping();
    $shippingAddress = [];
    $result['msg'] = "please check data";
    if ($shippingAddressId) {
        
        try {
            $shippingAddress = $addressFactory->getById($shippingAddressId)->__toArray();
            if (isset($shippingAddress['street'][0])) {
                $shippingAddress['street'] = $shippingAddress['street'][0];
            }
        } catch (\Exception $e) {
            //
        }
    }
        if (empty($shippingAddress)) {
            $orderColl = $objectManager->get('\Magento\Sales\Model\Order')->getCollection()->addFieldToFilter('customer_id',$customerId)->setOrder('entity_id','DESC');
               foreach ($orderColl as $key => $value) {
                    $childOrder = $objectManager->get('\Magento\Sales\Model\Order')->load($value['entity_id']);
                    if ($childOrder->getIsNotVirtual()) {
                        $shippingAddress = $childOrder->getShippingAddress()->getData();
                        break;
                    }
               }
        }
        if (empty($shippingAddress)) {
             $result=['error'=>1,'msg'=>'no shippingAddress avaliable to add in order'];
            return $result;
            //echo '<pre>'; echo 'no shippingAddress avaliable to add in order'; echo '</pre>';exit;
        }
        if (isset($shippingAddress['region']['region'])) {
            $shippingAddress['region'] = $shippingAddress['region']['region'];
        }
        $shippingAddress['save_in_address_book'] = 0;
       // echo '<pre>'; print_r($shippingAddress); echo '</pre>';exit;
        $StripeCustomermodel = $objectManager->get('StripeIntegration\Payments\Model\StripeCustomer');
        $helper = $objectManager->get('StripeIntegration\Payments\Helper\Generic');
        $loadproduct = $objectManager->create('\Magento\Catalog\Model\Product');
        $productFactory = $objectManager
                ->create(
                    '\Magento\Catalog\Model\ProductFactory'
                );
        $store = $_storeManager->getStore();
        $cartManagementInterface = $objectManager
        ->get(
            '\Magento\Quote\Api\CartManagementInterface'
        );
        $cart_id = $cartManagementInterface->createEmptyCart();
         $cartRepositoryInterface = $objectManager
        ->get(
            '\Magento\Quote\Api\CartRepositoryInterface'
        );
        $currencyFactory = $objectManager
        ->create(
            '\Magento\Directory\Model\CurrencyFactory'
        );
        $quote = $cartRepositoryInterface->get($cart_id);
        $quote->setStore($store); //set store for which you create quote

        //for prevent issue Uncaught (Status 400) Non-INR transactions in India should have shipping/billing address outside India. More info here: https://stripe.com/docs/india-exports thrown
        // $currencyCode = 'USD';
        // // make change here.
        // $currency =  $currencyFactory->create()->load($currencyCode);
 
        // $_storeManager->getStore($store->getId())->setCurrentCurrency($currency);
        // if you have allready buyer id then you can load customer directly 
        $customer= $customerRepository->getById($customer->getEntityId());
        $quote->setCurrency();
        $quote->assignCustomer($customer); //Assign quote to customer
        $csvSkus = explode(",", $updateData['items']);
        $qtys = explode(",", $updateData['qty']);
        $releventqty = [];
        if (!empty($csvSkus) && !empty($qtys)) {
           foreach ($csvSkus as $key => $value) {
               if (isset($qtys[$key])) {
                 if ($qtys[$key] > 0) {
                      $releventqty[$value] = $qtys[$key];
                   }
                    
               }
           }
        }
        if (!empty($releventqty)) {
           
        }
        else {
            $result=['error'=>1,'msg'=>'no items to add in order'];
            return $result;
        }
       
        if (!empty($releventqty)) {
            foreach($releventqty as $item => $qty){
                $productModel = $productFactory->create();
                $existingProductID = $productModel->getIdBySku($item);
                if ($existingProductID) {
                    //echo '<pre>'; print_r($existingProductID); echo '</pre>';
                    $loadproduct = $objectManager->create('\Magento\Catalog\Model\Product');
                    $product = $loadproduct->load($existingProductID);
                    $quote->addProduct(
                        $product,
                        intval($qty)
                    );
                }
                
            }
        }
        // 
        //add items in quote
        

        //Set Address to quote
        $quote->getBillingAddress()->addData($shippingAddress);
        $quote->getShippingAddress()->addData($shippingAddress);

        // Collect Rates and Set Shipping & Payment Method

        $shippingAddress = $quote->getShippingAddress();
        
        $shippingAddress->setCollectShippingRates(true)
                        ->collectShippingRates()
                        ->setShippingMethod($shipping_method); //shipping method
        $quote->setPaymentMethod('purchaseorder'); //payment method
        $quote->setInventoryProcessed(false); //not effetc inventory
        $quote->save();
       
        $quote->collectTotals();
        if (!$quote->isVirtual()) {
            $total = $quote->getShippingAddress();
            if (isset($updateData['shipping_charge'])) {
                $total->setShippingAmount($updateData['shipping_charge']);
                $total->setBaseShippingAmount($updateData['shipping_charge']);
            }
            if (isset($updateData['tax'])) {
                $total->setTaxAmount($updateData['tax']);
                $total->setBaseTaxAmount($updateData['tax']);
            }
            $total->setSubtotal($updateData['subtotal']);
            $total->setBaseSubtotal($updateData['subtotal']);
            $total->setGrandTotal($updateData['invoice_total']);
            $total->setBaseGrandTotal($updateData['invoice_total']);
           // $quote->collectTotals();
            $quote->save();
        }
        $quote->setSubtotal($updateData['subtotal']);
        $quote->setBaseSubtotal($updateData['subtotal']);
        $quote->setGrandTotal($updateData['invoice_total']);
        $quote->setBaseGrandTotal($updateData['invoice_total']);
        $quote->save();
        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => 'purchaseorder','number'=>'PO1234567']);

        // Collect Totals & Save Quote
       // $quote->collectTotals()->save();
        
        
        // Create Order From Quote
        $quote = $cartRepositoryInterface->get($quote->getId());
		  if ($groupid == '6') {
        $shipping_method = 'freeshiping_freeshiping';
    }else{
        $shipping_method = 'flatrate_flatrate';
    }
        $shippingAddress->setCollectShippingRates(true)->collectShippingRates()->setShippingMethod($shipping_method);
        //echo '<pre>'; print_r($quote->getItemsCount()); echo '</pre>';exit;
        $orderId = $cartManagementInterface->placeOrder($quote->getId());

        $order = $objectManager->get('\Magento\Sales\Model\Order')->load($orderId);
        if (isset($updateData['shipping_charge'])) {
            $order->setShippingAmount($updateData['shipping_charge']);
            $order->setBaseShippingAmount($updateData['shipping_charge']);
        }
        if (isset($updateData['tax'])) {
            $order->setTaxAmount($updateData['tax']);
            $order->setBaseTaxAmount($updateData['tax']);
        }
        $order->setSubtotal($quote->getSubtotal())
            ->setBaseSubtotal($quote->getBaseSubtotal())
            ->setGrandTotal($quote->getGrandTotal())
            ->setBaseGrandTotal($quote->getBaseGrandTotal());
        $order->save();
        $order->setEmailSent(0);
        $increment_id = $order->getRealOrderId();
        if($order->getEntityId()){
            echo '<pre>'; echo 'new ORder done order id-->'. $order->getRealOrderId(); echo '</pre>';
            $result['order_id']= $order->getRealOrderId();
        }else{
            $result=['error'=>1,'msg'=>'error in order place'];
        }
        return $result;
}
function writeLog($message, $logfile = 'nav_order.log')
{
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);
    $logger->info($message);
}
function getCustomerPaymentMethod($cus)
{
$result='';
// echo $cus;
/*
$paymentMethod_bank =  \Stripe\Customer::allSources(
    $cus,
    ['object' => 'bank_account',  'limit' => 3]
  );
  
  if($paymentMethod_bank)
  {
	  $ba=$paymentMethod_bank['data'][0]->id;
	  $isVerified=$paymentMethod_bank['data'][0]->status=='verified'?1:0;
	if($isVerified==0)
	{
		  $bank_account = \Stripe\Customer::retrieveSource(
    $cus,
    $ba
  );

		$isVerified=$bank_account->verify(['amounts' => [32, 45]]);
	}
	
   if($isVerified)
   {
	return $ba;
  }
  }
  else
	*/  
  {
	  $secKey=  'sk_test_51IDw2JKzsQLdHgSO5cTcWkyAnAm9yDy3i6zqgDrCu1LdlFo1UY91lflrbF60n6sssjgoY3w30Mmaa6wG21UMitJh004ZJNY14B';

	$stripe = new \Stripe\StripeClient($secKey);
	$s_customer = \Stripe\Customer::retrieve(
    $cus
  );
   // echo '<pre>';
   // print_r($s_customer);
  
  if($s_customer)
  {
	  echo $pm=$s_customer['invoice_settings']->default_payment_method;
  }
  else
	  return $result;
  
  if($pm=='')
{
	
	$res=$stripe->paymentMethods->all([
  'customer' => $cus,
  'type' => 'card',
	]);
//		echo '<pre>';
//	 print_r($res);
	if($res)
	{
		$pm=$res['data'][0]->id;  
		$result=$pm;
		return $result;
	}

}
else
	return $pm;
	
if($pm=='') 
	return $result;
	  
  }
}
  
  
