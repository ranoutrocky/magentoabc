<?php

use Magento\Framework\App\Bootstrap;include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');
require_once('../../vendor/autoload.php');

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;
use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\Charge;
use Magento\Framework\App\Filesystem\DirectoryList;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');


 $state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('Psr\Log\LoggerInterface');
$storeManager->info('Magecomp Log');

$storeManager=$objectManager->get('Magento\Store\Model\StoreManagerInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$request = $objectManager->get(Magento\Framework\App\Request\Http::class);

$connection = $resource->getConnection();
$orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 

$quoteToOrder = $objectManager
            ->create(
                '\Magento\Quote\Model\Quote\Item\ToOrderItem'
            );
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$invoiceFileName = $helper->getInvoiceName();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();

$connection = $resource->getConnection();
$bucket = 'esprigas.beverage.portal';
$source_path='/var/www/html/coke-portal-dev/var/NAV/Payments/';
$dest_path='/var/www/html/coke-portal-dev/var/NAV/Payments/Archived/';


$yesterday =  date('Y-m-d',strtotime("-1 days"));
$today =  date('Y-m-d');
$payment_date=date('Y-m-d',strtotime("+1 days"));

$order_import = $resource->getTableName('esprimodule_orderimportdata_orderimportdata');


  $fileName = 'BP_to_NAV_Payment_Information_'.date('Ydm_His').'.csv';
  $s3_file_url = 's3://'.$bucket.'/Payments/BP/'.$fileName;

  
 $s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
        'key'    => $s3key,
        'secret' => $s3secretkey,
    ]
]);
$s3->registerStreamWrapper();
 
 $inputFileName = $source_path.$fileName ;
  $archiveFileName = $dest_path.$fileName ;

 
	
	 try {
	$objPHPExcel = new PHPExcel();
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');

	$query = "SELECT ei.*,si.created_at FROM esprimodule_orderimportdata_orderimportdata AS ei
	INNER JOIN sales_order AS so ON ei.bp_order_number = so.increment_id
	INNER JOIN sales_invoice AS si ON so.entity_id = si.order_id
	WHERE ei.Status ='1' AND si.created_at >'$yesterday' AND si.created_at<'$today'
	AND si.transaction_id IS NOT NULL AND si.transaction_id!='' AND si.state=2";

$recordarray = $connection->fetchAll($query);

$row = 0;
$TotalAmount = 0;
    foreach ($recordarray as $record)
    {
		$row ++;
		$TotalAmount = $TotalAmount +  floatval($record['line_amount']);
	 $magento_cus_id = $record['bp_customer_id'];
	     $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
 
$customerObj = $objectManager->create('Magento\Customer\Model\Customer')
            ->load($magento_cus_id);
 $customeName = $customerObj->getFirstname().' '.$customerObj->getLastname() ;
	
	 
	

    
   
   $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $record['line_number'])
                              ->setCellValue('B'.$row,  'Customer')
                              ->setCellValue('C'.$row, $record['nav_customer_number'])
                              ->setCellValue('D'.$row, $customeName)
                              ->setCellValue('E'.$row, $record['created_at'])
							   ->setCellValue('F'.$row, 'Payment')
                              ->setCellValue('G'.$row, $record['nav_invoice_number'])
                              ->setCellValue('H'.$row, '')
                              ->setCellValue('I'.$row, '')
							   ->setCellValue('J'.$row, '-'.$record['line_amount'])
                              ->setCellValue('K'.$row, '')
                              ->setCellValue('L'.$row, $record['line_amount'])
                              ->setCellValue('M'.$row, 'Invoice')
							   ->setCellValue('N'.$row, '')
                              ->setCellValue('O'.$row, '')
                              ->setCellValue('P'.$row, '')
                              ->setCellValue('Q'.$row, $record['nav_invoice_number'])
							   ->setCellValue('R'.$row, 'AC')
							   ->setCellValue('S'.$row, $record['quantity'])
							   ->setCellValue('T'.$row, $unit_price = $record['unit_price'])
                              ->setCellValue('U'.$row, '')
                              ->setCellValue('V'.$row, '')
                              ->setCellValue('W'.$row, $record['nav_invoice_number']);
							  
							  
	}		
	$row ++;
	$objPHPExcel->getActiveSheet()
								->setCellValue('A'.$row, '')
								->setCellValue('B'.$row,  'G/L Account')
                              ->setCellValue('C'.$row, ''.'10700')
                              ->setCellValue('D'.$row, 'Credit Card Clearing')
							  ->setCellValue('E'.$row, $payment_date)
							  ->setCellValue('F'.$row, 'Payment')
							  ->setCellValue('G'.$row, '')
							  ->setCellValue('H'.$row, '')
							  ->setCellValue('I'.$row, '')
							  ->setCellValue('J'.$row, '')
							  ->setCellValue('K'.$row, '')
							  ->setCellValue('L'.$row, ''.$TotalAmount)
							  ->setCellValue('M'.$row, '')
							  ->setCellValue('N'.$row, '')
							  ->setCellValue('O'.$row, '')
							  ->setCellValue('P'.$row, '')
							  ->setCellValue('Q'.$row, '')
                               ->setCellValue('R'.$row, 'AC')
							   ->setCellValue('S'.$row, '')
							   ->setCellValue('T'.$row, '')
							   ->setCellValue('U'.$row, '')
							   ->setCellValue('V'.$row, '')
							   ->setCellValue('W'.$row, '')
							   ;

                             
							  
							  $objWriter->save($inputFileName);
							  
							 $dataS3=file_get_contents($inputFileName);
	                         file_put_contents($s3_file_url,$dataS3);
							 writeLog('done file'.$inputFileName);
							 rename($inputFileName,$archiveFileName);
   
	
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}
			
	
	
	
					






function writeLog($message, $logfile = 'bp_export_payment.log')
{
    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
    $logger = new \Zend\Log\Logger();
    $logger->addWriter($writer);
    $logger->info($message);
}
