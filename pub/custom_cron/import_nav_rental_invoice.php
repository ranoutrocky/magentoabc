<?php
include('../../app/bootstrap.php');    
    include_once('../../Excel/Classes/PHPExcel.php');
    use Magento\Framework\App\Bootstrap;
    use Magento\Customer\Api\AccountManagementInterface;
    use Magento\Customer\Model\AccountManagement;
	//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
	use Zend\Log\Filter\Timestamp;


$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$fileName = $helper->getRenatalinvoiceName();
$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();		
require '../../vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

$keyname = $fileName;

$bucket = 'esprigas.beverage.portal';
/*
$s3 = new S3Client([
    'version' => 'latest',
    'region'  => 'us-east-1'
]);
*/

$s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
        'key'    => $s3key,
        'secret' => $s3secretkey,
    ]
]);
// $var =  'nav_customer_imported.xlsx';
$inputFileName = '/var/www/html/coke-portal-dev/var/NAV/' . $keyname; //$var;
$s3->registerStreamWrapper();
$url = 's3://'.$bucket.'/'.$keyname;
try {
    // Get the object.
    $result = $s3->getObject([
        'Bucket' => $bucket,
        'Key'    => $keyname,
    ]);
	$dataS3=file_get_contents($url);
	file_put_contents($inputFileName,$dataS3);

    // Display the object in the browser.
  // header("Content-Type: {$result['ContentType']}");
 //   echo  $result['Body'];
 // var_dump($result);
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
 


//$inputFileName = '/var/www/html/coke-portal-dev/var/NAV/' . $var;
// $inputFileName = $result['Body'];
$outputFileName = '/var/www/html/coke-portal-dev/var/NAV/done/' . $keyname . '-' . $date;
// $var = 'nav_to_magento_renatl_order_imported.xlsx';
// $inputFileName='/var/www/html/coke-portal-dev/var/NAV/'.$var;
// $outputFileName='/var/www/html/coke-portal-dev/var/NAV/done/'.$var.'-'.$date;
			//Read your Excel workbook
			try{
				$inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
				$objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel    =   $objReader->load($inputFileName);
			}catch(Exception $e){
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getActiveSheet(); 
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestColumn();

			$dataArray = array();
			$row=2;

			$startRow=2;
			//$highestRow=3;
            // $prev_magento_order_id='';
            $ordersarray=[];
			$rowA='A'.$startRow;
			  for ($row =$startRow; $row <= $highestRow; $row++){
                $line_number='L'.$row;
                $line_number = $sheet->getCell($line_number)->getValue();
                $magento_order_id='J'.$row;
               $magento_order_id = $sheet->getCell($magento_order_id)->getValue();
                $sku='M'.$row;
                $qty='O'.$row;
              $sku = $sheet->getCell($sku)->getValue();
              $qty = $sheet->getCell($qty)->getValue();
               $invoice_no='A'.$row;
               $po_no='C'.$row;
               $nav_cus_id='F'.$row;
                 $magento_cus_id='G'.$row;
                 $unit_price='P'.$row;
                 $line_amt='Q'.$row;
                 $freight='S'.$row;
                 $tax='T'.$row;
                 $dday='D'.$row;
                 $dueday='E'.$row;
                 
                 $totel ='U'.$row;
                 $subtotel ='R'.$row;


               $invoice_no = $sheet->getCell($invoice_no)->getValue();
               $po_no = $sheet->getCell($po_no)->getValue();
               $nav_cus_id = $sheet->getCell($nav_cus_id)->getValue();
                 $magento_cus_id = $sheet->getCell($magento_cus_id)->getValue();
                 $unit_price = $sheet->getCell($unit_price)->getValue();
                 $line_amt = $sheet->getCell($line_amt)->getValue();
                 $freight = $sheet->getCell($freight)->getValue();
                 $tax = $sheet->getCell($tax)->getValue();
                 $dday = $sheet->getCell($dday)->getValue();
                 $dueday = $sheet->getCell($dueday)->getValue();
                 $totel = $sheet->getCell($totel)->getValue();
                $subtotel = $sheet->getCell($subtotel)->getValue();
                
                $olditme = '';
                $oldqty = '';
                if ($magento_order_id =="") {
                    $ordersarray_for_new = array("customerid"=>$magento_cus_id,'items'=>$sku, 'qty'=>$qty,'shipping_charge' => $freight,'tax'=>$tax,'invoice_total'=>$totel,'subtotal'=>$subtotel);
                    ordercreatefornew($ordersarray_for_new);
                //     if(!empty($ordersarray)){

                //         if (array_key_exists(trim($magento_order_id), $ordersarray)) {
                //            $olditme = $ordersarray[$magento_order_id]['items'];
                //             $oldqty = $ordersarray[$magento_order_id]['qty'];
                //             $updateditem = $olditme.",".$sku;
                //             $updatedsku = $oldqty.",".$qty;
                //             $ordersarray[$magento_order_id]['items']=$updateditem;
                //             $ordersarray[$magento_order_id]['qty']=$updatedsku;
                //         }else{
                //             $ordersarray[$magento_order_id] = array("order_id"=>$magento_order_id, "items"=>$sku, "qty"=>$qty,'shipping_charge' => $freight,'tax'=>$tax,'invoice_total'=>$totel,'subtotal'=>$subtotel);
                //         }
    
                //     }else{
                //         $ordersarray[$magento_order_id] = array("order_id"=>$magento_order_id, "items"=>$sku, "qty"=>$qty,'shipping_charge' => $freight,'tax'=>$tax,'invoice_total'=>$totel,'subtotal'=>$subtotel);
                //     }
                //     if($row == $highestRow){
                   
                //         invoicreate($ordersarray);
                //          rename($inputFileName, $outputFileName);
                //  writeLog('Record inserted Successfully');
                // echo "success";
                //     }
                    
                }
                // else{
                //     $ordersarray_for_new = array("customerid"=>$magento_cus_id,'items'=>$sku, 'qty'=>$qty,'shipping_charge' => $freight,'tax'=>$tax,'invoice_total'=>$totel,'subtotal'=>$subtotel);
                //     ordercreatefornew($ordersarray_for_new);
                // }
                
				
				
				
                




					
		
			}

			
			function ordercreatefornew($ordersarray_for_new)
            {
                $bootstrap = Bootstrap::create(BP, $_SERVER);
            $objectManager = $bootstrap->getObjectManager();
            $objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
            $objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
            $date = $objDate->gmtDate();
            $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();
            $regionCollection = $objectManager->create('\Magento\Directory\Model\Country')->loadByCode('US')->getRegions();
            $regions = $regionCollection->loadData()->toOptionArray(false);
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $store = $storeManager->getStore();
            $websiteId = $storeManager->getStore()->getWebsiteId();
            $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');


                $productObj = $productRepository->get($ordersarray_for_new['items']);

	
                $customerid = $ordersarray_for_new['customerid'];
                echo "==".$productObj->getId();
                $order_item['item']= array("id"=>$productObj->getId(), "qty"=>$ordersarray_for_new['qty']);
                // exit;
                $customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customerid);
            try {
                $billingID =  $customer->getDefaultBilling();
            $address = $objectManager->create('Magento\Customer\Model\Address')->load($billingID);
            echo "<pre>";print_r($address->getData());
                $quoteFactory = $objectManager->get('\Magento\Quote\Model\QuoteFactory')->create();
                $shippingQuoteRate = $objectManager->get('\Magento\Quote\Model\Quote\Address\Rate');
                $quoteFactory->setStore($store);
                $quoteFactory->setCurrency();
                $quoteFactory->assignCustomer($customer);
                foreach ($order_item as $item) {
                    
                    $product = $objectManager->get('\Magento\Catalog\Model\ProductRepository')->getById($item['id']);// get product by product id 
                    $quoteFactory->addProduct($product, intval($item['qty']));  // add products to quote
                }
              
                /*
                * Set Address to quote
                */
                $quoteFactory->getBillingAddress()->addData($address->getData());
                $quoteFactory->getShippingAddress()->addData($address->getData());
            
                /*
                * Collect Rates and Set Shipping & Payment Method
                * You can get this information from the database table `quote_shipping_rate`
                */
                $shippingRateCarrier = 'freeshipping';
                $shippingRateCarrierTitle = 'Preferred Delivery';
                $shippingRateCode = 'freeshipping';
                $shippingRateMethod = 'freeshipping';
                $shippingRatePrice = '0';
                $shippingRateMethodTitle = 'Preferred Delivery';
            
                $shippingAddress = $quoteFactory->getShippingAddress();
            
                $shippingQuoteRate->setCarrier($shippingRateCarrier);
                $shippingQuoteRate->setCarrierTitle($shippingRateCarrierTitle);
                $shippingQuoteRate->setCode($shippingRateCode);
                $shippingQuoteRate->setMethod($shippingRateMethod);
                $shippingQuoteRate->setPrice($shippingRatePrice);
                $shippingQuoteRate->setMethodTitle($shippingRateMethodTitle);
            
                $shippingAddress->setCollectShippingRates(true)
                                ->collectShippingRates()
                                ->setShippingMethod($shippingRateCode); //shipping method
                $quoteFactory->getShippingAddress()->addShippingRate($shippingQuoteRate);
            
                
                $quoteFactory->setPaymentMethod('checkmo'); //payment method
                $quoteFactory->setInventoryProcessed(false); //not effetc inventory
                $quoteFactory->save(); //Now Save quote and your quote is ready
            
                // Set Sales Order Payment
                $quoteFactory->getPayment()->importData(['method' => 'checkmo']);
            
                // Collect Totals & Save Quote
                $quoteFactory->collectTotals()->save();
            
                // Create Order From Quote
                $order = $objectManager->get('\Magento\Quote\Model\QuoteManagement')->submit($quoteFactory);
                
                $order->setEmailSent(0);
                $increment_id = $order->getRealOrderId();
                $ordersarray[$increment_id] = array("order_id"=>$increment_id, "items"=>$ordersarray_for_new['items'], "qty"=>$ordersarray_for_new['qty'],'shipping_charge' => $ordersarray_for_new['shipping_charge'],'tax'=>$ordersarray_for_new['tax'],'invoice_total'=>$ordersarray_for_new['invoice_total'],'subtotal'=>$ordersarray_for_new['subtotal']);
                invoicreate($ordersarray);
            
                echo "<br/>Order Created<br/>";
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            }

    function invoicreate($ordersarray){
        $bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
        $orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 
        $quoteToOrder = $objectManager->create('\Magento\Quote\Model\Quote\Item\ToOrderItem');
        foreach ($ordersarray as $key => $updateData) {
            $order = $orderObject->loadByIncrementId($updateData['order_id']);
            echo $order->getId();
            // continue;
if ($order->getId() && $order) {
    echo '<pre>'; echo $order->getQuoteId(); echo '</pre>';
    $quote = $objectManager->create('\Magento\Quote\Model\Quote')->load($order->getQuoteId());
    $csvSkus = explode(",", $updateData['items']);
    $qtys = explode(",", $updateData['qty']);
    $releventqty = [];
    if (!empty($csvSkus) && !empty($qtys)) {
       foreach ($csvSkus as $key => $value) {
           if (isset($qtys[$key])) {
                $releventqty[$value] = $qtys[$key];
           }
       }
    }
    echo '<pre>'; print_r($csvSkus); echo '</pre>';
    $updateItems = [];
    foreach ($quote->getAllItems() as $quoteItem) {
        echo "qute item ".$quoteItem->getSku();
        if (in_array($quoteItem->getSku(), $csvSkus)) {
            echo "update".$updateItems[$quoteItem->getId().'_'.$quoteItem->getSku()] = $quoteItem->getId();
        }
    }
    echo '<pre>++'; print_r($updateItems); echo '</pre>';
    if (!empty($updateItems) && !empty($releventqty)) {
        foreach ($quote->getAllItems() as $quoteItem) {
            if (in_array($quoteItem->getId(), $updateItems)) {
                if (isset($releventqty[$quoteItem->getSku()])) {
                    if ($releventqty[$quoteItem->getSku()] > 0) {
                        $quoteItem->setQty($releventqty[$quoteItem->getSku()]);
                    }
                    else {
                        unset($updateItems[$quoteItem->getId().'_'.$quoteItem->getSku()]);
                    }
                }
            }
        }
        //echo '<pre>'; print_r($updateItems); echo '</pre>';exit;
        //invoice total likely be subtotal + tax + shipping
    if (!$quote->isVirtual()) {
        $total = $quote->getShippingAddress();
        if (isset($updateData['shipping_charge'])) {
            $total->setShippingAmount($updateData['shipping_charge']);
            $total->setBaseShippingAmount($updateData['shipping_charge']);
        }
        if (isset($updateData['tax'])) {
            $total->setTaxAmount($updateData['tax']);
            $total->setBaseTaxAmount($updateData['tax']);
        }
        $total->setSubtotal($updateData['subtotal']);
        $total->setBaseSubtotal($updateData['subtotal']);
        $total->setGrandTotal($updateData['invoice_total']);
        $total->setBaseGrandTotal($updateData['invoice_total']);
        $quote->collectTotals();
        $quote->save();
        echo "qute save";
    }
    $quote->setSubtotal($updateData['subtotal']);
    $quote->setBaseSubtotal($updateData['subtotal']);
    $quote->setGrandTotal($updateData['invoice_total']);
    $quote->setBaseGrandTotal($updateData['invoice_total']);
    $quote->save();
    foreach ($quote->getAllItems() as $quoteItem) {
        if (in_array($quoteItem->getId(), $updateItems)) {
            $orderItem = $quoteToOrder->convert($quoteItem);
            $origOrderItemNew = $order->getItemByQuoteItemId($quoteItem->getId());
            if ($origOrderItemNew) {
                $origOrderItemNew->addData($orderItem->getData());
            } else {
                if ($quoteItem->getParentItem()) {
                    $orderItem->setParentItem(
                        $order->getItemByQuoteItemId($orderItem->getParentItem()->getId())
                    );
                }
                $order->addItem($orderItem);
            }
        }
    }
    foreach ($order->getAllItems() as $orderItem) {
        $getQuoteItemId = $orderItem->getQuoteItemId();
        if (in_array($getQuoteItemId, $updateItems)) {
        }
        else {
            $orderItem->isDeleted(true);
        }
    }
    if (isset($updateData['shipping_charge'])) {
        $order->setShippingAmount($updateData['shipping_charge']);
        $order->setBaseShippingAmount($updateData['shipping_charge']);
    }
    if (isset($updateData['tax'])) {
        $order->setTaxAmount($updateData['tax']);
        $order->setBaseTaxAmount($updateData['tax']);
    }
    $order->setSubtotal($quote->getSubtotal())
        ->setBaseSubtotal($quote->getBaseSubtotal())
        ->setGrandTotal($quote->getGrandTotal())
        ->setBaseGrandTotal($quote->getBaseGrandTotal());
    $quote->save();
    $orderState = Order::STATE_PROCESSING;
$order->setState($orderState)->setStatus(Order::STATE_PROCESSING);
$order->save();
    $order->save();
    

    //create invoice part
    $orderRepo = $objectManager->get('\Magento\Sales\Api\OrderRepositoryInterface');
    $invoiceService = $objectManager->get('\Magento\Sales\Model\Service\InvoiceService');
    $transaction = $objectManager->get('\Magento\Framework\DB\Transaction'); 
    $invoiceSender = $objectManager->get('\Magento\Sales\Model\Order\Email\Sender\InvoiceSender'); 
    $updatedOrder = $orderRepo->get($order->getId());
    if ($updatedOrder->canInvoice()) {
        $invoice = $invoiceService->prepareInvoice($updatedOrder);
        $invoice->setShippingAmount($updateData['shipping_charge']);
        $invoice->setSubtotal($updateData['subtotal']);
        $invoice->setBaseSubtotal($updateData['subtotal']);
        $invoice->setGrandTotal($updateData['invoice_total']);
        $invoice->setBaseGrandTotal($updateData['invoice_total']);
        $invoice->register();
        $invoice->save();
        $transactionSave = $transaction->addObject(
            $invoice
        )->addObject(
            $invoice->getOrder()
        );
        $transactionSave->save();
         echo '<pre>'; echo 'invoice done invoice id-->'. $invoice->getId(); echo '</pre>';
       // $this->invoiceSender->send($invoice);
       /* //Send Invoice mail to customer
        $updatedOrder->addStatusHistoryComment(
            __('Notified customer about invoice creation #%1.', $invoice->getId())
        )
            ->setIsCustomerNotified(true)
            ->save();*/
    }
    else {
        echo '<pre>'; echo 'invoice done already order id-->'. $order->getId(); echo '</pre>';
    }
    }
    
}
else {
  //  echo '<pre>'; echo 'debugging'; echo '</pre>';exit;
    //nothing to do
}
        }

    }

	function writeLog($message,$logfile='nav_order.log')
	{
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/'.$logfile);
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($message);
	}