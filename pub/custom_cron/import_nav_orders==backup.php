<?php
include('../../app/bootstrap.php');    
    include_once('../../Excel/Classes/PHPExcel.php');
    use Magento\Framework\App\Bootstrap;
    use Magento\Customer\Api\AccountManagementInterface;
    use Magento\Customer\Model\AccountManagement;
	//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
	use Zend\Log\Filter\Timestamp;


$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();

$var = 'nav_order_imported.xlsx';
$inputFileName='/var/www/html/coke-portal-dev/var/NAV/'.$var;
$outputFileName='/var/www/html/coke-portal-dev/var/NAV/done/'.$var.'-'.$date;
			//Read your Excel workbook
			try{
				$inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
				$objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel    =   $objReader->load($inputFileName);
			}catch(Exception $e){
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getActiveSheet(); 
			$highestRow = $sheet->getHighestRow(); 
			$highestColumn = $sheet->getHighestColumn();

			$dataArray = array();
			$row=2;

			$startRow=2;
			//$highestRow=3;

			$rowA='A'.$startRow;
			  for ($row =$startRow; $row <= $highestRow; $row++){
				$invoice_no='A'.$row;
				$po_no='C'.$row;
				$nav_cus_id='F'.$row;
				  $magento_cus_id='G'.$row;
				  $magento_order_id='J'.$row;
				  $qty='O'.$row;
				  $unit_price='P'.$row;
				  $line_amt='Q'.$row;
				  $freight='S'.$row;
				  $tax='T'.$row;
				  $dday='D'.$row;
				  $dueday='E'.$row;


				 echo "--". $invoice_no = $sheet->getCell($invoice_no)->getValue();
				 echo "--".  $po_no = $sheet->getCell($po_no)->getValue();
				 echo "--".  $nav_cus_id = $sheet->getCell($nav_cus_id)->getValue();
				 echo "--".$magento_cus_id = $sheet->getCell($magento_cus_id)->getValue();
				 echo "--". $magento_order_id = $sheet->getCell($magento_order_id)->getValue();
				 echo "--". $qty = $sheet->getCell($qty)->getValue();
				 echo "--".$unit_price = $sheet->getCell($unit_price)->getValue();
				 echo "--". $line_amt = $sheet->getCell($line_amt)->getValue();
				 echo "--". $freight = $sheet->getCell($freight)->getValue();
				 echo "--". $tax = $sheet->getCell($tax)->getValue();
				 echo "--".$dday = $sheet->getCell($dday)->getValue();
				 echo "--". $dueday = $sheet->getCell($dueday)->getValue();
				  


					$orderId = $magento_order_id;
					//$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
					$quoteToOrder = $objectManager->create('Magento\Quote\Model\Quote\Item\ToOrderItem');
					$order = $objectManager->create('Magento\Sales\Model\Order')->load($orderId);
					$quote = $objectManager->create('\Magento\Quote\Model\Quote')->load($order->getQuoteId());

					$item = $quote->getItemById($order->getQuoteId());
					if(!$item) return;

					$taxAmount = $item->getTaxPercent() * $qty * ($item->getProduct()->getPrice()/100);
					$rowTotalInclTax = $item->getPrice() * $qty + $taxAmount;
					$item
						->setQty($qty)
						->setTaxAmount($taxAmount)
						->setRowTotalInclTax($rowTotalInclTax);
					$existData = $order->getItemByQuoteItemId($item->getId());
					$orderItem = $this->toOrderItem->convert($item);
					$existData->addData($orderItem->getData());
					//$item = $order->getData();
					//  print_r($order->getShippingAddress()->getData());
					//  exit;
					//  if ($quote->getItemsCount() > 0) {
					// 	// Updating Quote Items
					// 	foreach ($quote->getItemsCollection() as $quoteItem) {
					// 		$origOrderItem = $order->getItemByQuoteItemId($quoteItem->getId());
					// 		echo $orderItemId = $origOrderItem->getItemId();
					// 		$quoteItem->setQty($qty);
					// 		$quoteItem->setDeliveryDate($dday);
					// 		$quoteItem->setDueDate($dueday);
					// 	}
					// 	// Updating Quote Payment PO number
					// 	$payment = $quote->getPayment();
					// 	$payment->setPoNumber($po_no);
					// 	$quote->setPayment($payment);
					// 	// Updating Quote Shipment rate
					// 	$address = $quote->getShippingAddress();
					// 	$rates = $address->removeAllShippingRates();
					// 	$rate = $this->shippingAddressRate->create();
					// 	$rate->setCode(self::SHIPPING_METHOD_CODE)
					// 		->setPrice($freight);
					// 	$address->addShippingRate($rate);
					// 	$address->setShippingAmount(floatval($freight))
					// 		->setBaseShippingAmount(floatval($freight));
					// 	$quote->addAddress($address);
	
					// 	$quote->collectTotals();
					// 	$quote->save($quote);
					// }
exit;
					foreach ($quote->getItemsCollection() as $quoteItem) {
						$orderItem = $this->toOrderItem->convert($quoteItem);
						$origOrderItemNew = $order->getItemByQuoteItemId($quoteItem->getId());
	
						if ($origOrderItemNew) {
							$customFields = [
								'delivery_date' => $dday,
								'due_date' => $dueday,
								'customer_number' => $nav_cus_id
							];
							$origOrderItemNew->addData(
								array_merge($orderItem->getData(), $customFields)
							);
						} else {
							if ($quoteItem->getParentItem()) {
								$orderItem->setParentItem(
									$order->getItemByQuoteItemId($orderItem->getParentItem()->getId())
								);
							}
							$order->addItem($orderItem);
						}
						$payment = $order->getPayment();
						$payment->setPoNumber($po_no);
						// Updating Quote Shipment rate
						$payment->setBaseShippingAmount(floatval($freight));
						$payment->setBaseShippingCaptured(floatval($freight));
						$payment->setShippingAmount(floatval($freight));
						$payment->setShippingCaptured(floatval($freight));
						$order->setPayment($payment);
						$order->setShippingAmount(floatval($freight))
							->setBaseShippingAmount(floatval($freight))
							->setSubtotal($quote->getSubtotal())
							->setBaseSubtotal($quote->getBaseSubtotal())
							->setGrandTotal($quote->getGrandTotal())
							->setBaseGrandTotal($quote->getBaseGrandTotal());
						$quote->save($quote);
						$order->save($order);
					}
					
		
			}

			//exit;
			rename($inputFileName, $outputFileName);
			writeLog('Record inserted Successfully');


	function writeLog($message,$logfile='nav_order.log')
	{
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/'.$logfile);
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info($message);
	}