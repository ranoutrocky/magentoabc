<?php
 ini_set('display_errors', '1');
use Magento\Framework\App\Bootstrap;
include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');
require_once('../../vendor/autoload.php');

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;
use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\Charge;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;


$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$obj = $bootstrap->getObjectManager();
$state = $obj->get('Magento\Framework\App\State');


 $state->setAreaCode('frontend');

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->get('Psr\Log\LoggerInterface');
$storeManager->info('Magecomp Log');

$storeManager=$objectManager->get('Magento\Store\Model\StoreManagerInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$request = $objectManager->get(Magento\Framework\App\Request\Http::class);

$connection = $resource->getConnection();
$orderObject = $objectManager->get('\Magento\Sales\Model\Order'); 
$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');

$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$helper = $objectManager->create(EspriModule\InvoiceIntegration\Helper\Data::class);
$invoiceFileName = $helper->getInvoiceName();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');

$s3helper = $objectManager->create(Flagbit\FlysystemS3\Helper\Config::class);
$s3key = $s3helper->getS3Key();
$s3secretkey = $s3helper->getS3Secret();
$s3region = $s3helper->getS3Region();
$s3bucket = $s3helper->getS3Bucket();



$connection = $resource->getConnection();

$source_path=BP.'/var/NAV/Rental/';
$dest_path=BP.'/var/NAV/Rental/Archived/';
$filenamexls ='';

/*
$paramVal=$request->getParam('process');

$fileName='NAV_to_BP_Content_Invoices';
$processMsgVal='<br/>'.'Processing Content Invoices:'.'<br/>';
if($paramVal=='rental')
{
	$fileName='NAV_to_BP_Rental_Invoices';
	$processMsgVal='<br/>'.'Processing Rental Invoices:'.'<br/>';
}
else if ($paramVal=='content' || $paramVal=='')
{
	$fileName='NAV_to_BP_Rental_Invoices';
	$processMsgVal='<br/>'.'Processing Content Invoices:'.'<br/>';
}
else
{
	echo 'Invalid Parameter passed. Please provide either no parameter or content or rental';
	exit;
}
*/

$fileName='NAV_to_BP_Rental';
$order_import ='esprimodule_orderimportdata_orderimportdata'; // $resource->getTableName('esprimodule_order_import');

$keyname = $fileName.'.csv';
$bucket = 'esprigas.beverage.portal';




 $s3 = new Aws\S3\S3Client([
    'region'  => $s3region,
    'version' => 'latest',
    'credentials' => [
        'key'    => $s3key,
        'secret' => $s3secretkey,
    ]
]);
$s3->registerStreamWrapper();

$fileArr=[];
$dir='s3://'.$bucket.'/Rental/NAV/';
//echo $dir;
if (is_dir($dir) && ($dh = opendir($dir))) {
    while (($file = readdir($dh)) !== false) {
	$path=pathinfo($file);

	if (!in_array('Archived',$path))
	{
		//print_r($path);	
	 if((stripos($file,'.csv')>0) )
	 {
		// print_r($path);	
		array_push($fileArr,$file);	
		echo $file;
		
	 }
    }
	}
    closedir($dh);

}


 foreach ($fileArr as &$keyname) {
$var = $keyname;
$invoice_file_name = $keyname;
$inputFileName=$source_path.$var;
$outputFileName=$dest_path.$var.'-'.date('YmdHis');
$url = 's3://'.$bucket.'/Rental/NAV/'.$keyname;
$done_url='s3://'.$bucket.'/Rental/NAV/Archived/'.$keyname.'-'. $date;
$log_url='s3://'.$bucket.'/Rental/NAV/Archived/'.str_replace(".csv", ".log",$keyname).'-'. $date;

try {
	$dataS3=file_get_contents($url);
	file_put_contents($inputFileName,$dataS3);
   $LogfileName = $dest_path.$var;
  $LogfileName = str_replace(".csv", ".log",$LogfileName);
  
} catch (S3Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
//$outputFileName = BP.'/var/NAV/Invoices/Done/' . $keyname . '-' . $date;


 //gives table name with 



// echo 'test';
$filenamexls = $var;


try {
    $inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
    $objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel    =   $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
 $sheet = $objPHPExcel->getActiveSheet();
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();
$dataArray = array();
$row = 2;

$startRow = 2;
//$highestRow=3;
// $prev_magento_order_id='';
$newrow_numbers = 2;
for ($row = $startRow; $row <= $highestRow; $row++) {
	$cus_id = 'A' . $row;
	$cus_id = trim($sheet->getCell($cus_id)->getValue());
	if ($cus_id!="") {
	//	$newrow_numbers++;
	$newrow_numbers=$row;
	}else{
		break;
	}

 }

echo $highestRow = $newrow_numbers;

//echo $rowA='A'.$startRow;
 for ($row = $startRow; $row <= $highestRow; $row++) {
	$cust_id = 'A' . $row;
	$nav_id = 'B' . $row;
	$productid = 'C' . $row;
	$qty = 'D' . $row;
	$date = 'E' . $row;
	

	
	$customerId = $sheet->getCell($cust_id)->getValue();
	$nav_cust_id = $sheet->getCell($nav_id)->getValue();
	$productid = $sheet->getCell($productid)->getValue();
	 $qty = $sheet->getCell($qty)->getValue();
	$date = $sheet->getCell($date)->getValue();
	//echo $productid;
	echo $customerId;
	$productObj = $productRepository->get($productid);

	
	
	echo "==".$productObj->getId();
	$order_item['item']= array("id"=>$productObj->getId(), "qty"=>$qty);
	// exit;
	$customer = $objectManager->get('\Magento\Customer\Api\CustomerRepositoryInterface')->getById($customerId);

	//$buy_grp = $sheet->getCell( 'C'.$startRow )->getValue();

	$rentalmodel = $objectManager->create('EspriModule\Rentalbalace\Model\Rentalbalace');
	
	$rentalmodel->setCustomerid($customerId);
	$rentalmodel->setNavcustomerid($nav_cust_id);
	$rentalmodel->setItemnumber($productid);
	$rentalmodel->setQty($qty);
	$rentalmodel->setAsofdate($date);
	
	
    $rentalmodel->save();
    
  //  writeLog('customer Id = ' . $rentalmodel->getRentalbalaceId() . ': data saved');
echo 'done=='.$rentalmodel->getRentalbalaceId();
}
//$done = 'hii.xlsx';
rename($inputFileName, $outputFileName);
rename($url,$done_url);
//writeLog('Record inserted Successfully');


function writeLog($message, $logfile = 'nav_rental.log')
{
	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
	$logger = new \Zend\Log\Logger();
	$logger->addWriter($writer);
	$logger->info($message);
}		 
}
?>