<?php
echo "==";
exit;
include('../../app/bootstrap.php');
include_once('../../Excel/Classes/PHPExcel.php');

use Magento\Framework\App\Bootstrap;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
//use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Zend\Log\Filter\Timestamp;


$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$objectManager->get('Magento\Framework\App\State')->setAreaCode('frontend');
$objDate = $objectManager->create('Magento\Framework\Stdlib\DateTime\DateTime');
$date = $objDate->gmtDate();
$groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')->toOptionArray();
$regionCollection = $objectManager->create('\Magento\Directory\Model\Country')->loadByCode('US')->getRegions();
$regions = $regionCollection->loadData()->toOptionArray(false);
		


$var = 'nav_rental_imported.xlsx';
$inputFileName = '/var/www/html/coke-portal-dev/var/NAV/' . $var;
$outputFileName = '/var/www/html/coke-portal-dev/var/NAV/done/' . $var . '-' . $date;
//Read your Excel workbook
try {
	$inputFileType  =   PHPExcel_IOFactory::identify($inputFileName);
	$objReader      =   PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel    =   $objReader->load($inputFileName);
} catch (Exception $e) {
	die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getActiveSheet();
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

$dataArray = array();
$row = 2;

$startRow = 2;
//$highestRow=3;

//echo $rowA='A'.$startRow;
for ($row = $startRow; $row <= $highestRow; $row++) {
	$cust_id = 'A' . $row;
	$nav_id = 'B' . $row;
	$productid = 'C' . $row;
	$qty = 'D' . $row;
	$date = 'E' . $row;
	


	echo $customerId = $sheet->getCell($cust_id)->getValue();
	$nav_cust_id = $sheet->getCell($nav_id)->getValue();
	$productid = $sheet->getCell($productid)->getValue();
	$qty = $sheet->getCell($qty)->getValue();
	$date = $sheet->getCell($date)->getValue();
	
exit;
	

	//$buy_grp = $sheet->getCell( 'C'.$startRow )->getValue();

	$rentalmodel = $objectManager->create('EspriModule\Rentalbalace\Model\Rentalbalace');
	//echo'<pre>';
	//print_r($customer->getProgramId());
	$rentalmodel->setCustomerid($customerId);
	$rentalmodel->setNavcustomerid($nav_cust_id);
	$rentalmodel->setItemnumber($productid);
	$rentalmodel->setQty($qty);
	$rentalmodel->setAsofdate($date);
	
	
    $rentalmodel->save();
    
	writeLog('customer Id = ' . $rentalmodel->getId() . ': data saved');

	
		


}
//$done = 'hii.xlsx';
rename($inputFileName, $outputFileName);
writeLog('Record inserted Successfully');


function writeLog($message, $logfile = 'nav_rental.log')
{
	$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/' . $logfile);
	$logger = new \Zend\Log\Logger();
	$logger->addWriter($writer);
	$logger->info($message);
}
