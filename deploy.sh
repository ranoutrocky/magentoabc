echo "permissions";
sudo chmod -R 777 var/ pub/ generated/ ;

echo "Upgrade";
sudo php bin/magento setup:upgrade;

echo "Deploy";
sudo php bin/magento setup:static-content:deploy -f;

sudo chmod -R 777 var/ generated/ pub/;

echo "Indexer";
sudo php bin/magento indexer:reindex;

echo "Cache clean";
sudo php bin/magento c:c;

echo "Cache flush";
sudo php bin/magento c:f;

echo "permissions";
sudo chmod -R 777 var/ generated/ pub/;

