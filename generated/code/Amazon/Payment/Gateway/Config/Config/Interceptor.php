<?php
namespace Amazon\Payment\Gateway\Config\Config;

/**
 * Interceptor class for @see \Amazon\Payment\Gateway\Config\Config
 */
class Interceptor extends \Amazon\Payment\Gateway\Config\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Amazon\Core\Model\AmazonConfig $amazonConfig, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\App\RequestInterface $request, \Magento\Sales\Api\OrderRepositoryInterface $orderRepository)
    {
        $this->___init();
        parent::__construct($amazonConfig, $scopeConfig, $request, $orderRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function isActive($storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isActive');
        return $pluginInfo ? $this->___callPlugins('isActive', func_get_args(), $pluginInfo) : parent::isActive($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getValue($field, $storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getValue');
        return $pluginInfo ? $this->___callPlugins('getValue', func_get_args(), $pluginInfo) : parent::getValue($field, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function setMethodCode($methodCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setMethodCode');
        return $pluginInfo ? $this->___callPlugins('setMethodCode', func_get_args(), $pluginInfo) : parent::setMethodCode($methodCode);
    }

    /**
     * {@inheritdoc}
     */
    public function setPathPattern($pathPattern)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPathPattern');
        return $pluginInfo ? $this->___callPlugins('setPathPattern', func_get_args(), $pluginInfo) : parent::setPathPattern($pathPattern);
    }
}
