<?php
namespace Amazon\Core\Helper\Data;

/**
 * Interceptor class for @see \Amazon\Core\Helper\Data
 */
class Interceptor extends \Amazon\Core\Helper\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Module\ModuleListInterface $moduleList, \Magento\Framework\App\Helper\Context $context, \Magento\Framework\Encryption\EncryptorInterface $encryptor, \Magento\Store\Model\StoreManagerInterface $storeManager, \Amazon\Core\Helper\ClientIp $clientIpHelper, \Magento\Framework\Module\StatusFactory $moduleStatusFactory, \Amazon\Core\Model\AmazonConfig $config)
    {
        $this->___init();
        parent::__construct($moduleList, $context, $encryptor, $storeManager, $clientIpHelper, $moduleStatusFactory, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantId($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMerchantId');
        return $pluginInfo ? $this->___callPlugins('getMerchantId', func_get_args(), $pluginInfo) : parent::getMerchantId($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getAccessKey($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAccessKey');
        return $pluginInfo ? $this->___callPlugins('getAccessKey', func_get_args(), $pluginInfo) : parent::getAccessKey($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getSecretKey($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSecretKey');
        return $pluginInfo ? $this->___callPlugins('getSecretKey', func_get_args(), $pluginInfo) : parent::getSecretKey($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getClientId($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getClientId');
        return $pluginInfo ? $this->___callPlugins('getClientId', func_get_args(), $pluginInfo) : parent::getClientId($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getClientSecret($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getClientSecret');
        return $pluginInfo ? $this->___callPlugins('getClientSecret', func_get_args(), $pluginInfo) : parent::getClientSecret($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentRegion($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentRegion');
        return $pluginInfo ? $this->___callPlugins('getPaymentRegion', func_get_args(), $pluginInfo) : parent::getPaymentRegion($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getRegion($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRegion');
        return $pluginInfo ? $this->___callPlugins('getRegion', func_get_args(), $pluginInfo) : parent::getRegion($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrencyCode($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrencyCode');
        return $pluginInfo ? $this->___callPlugins('getCurrencyCode', func_get_args(), $pluginInfo) : parent::getCurrencyCode($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getScaRegions($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getScaRegions');
        return $pluginInfo ? $this->___callPlugins('getScaRegions', func_get_args(), $pluginInfo) : parent::getScaRegions($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetUrl($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getWidgetUrl');
        return $pluginInfo ? $this->___callPlugins('getWidgetUrl', func_get_args(), $pluginInfo) : parent::getWidgetUrl($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetPath($key, $store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getWidgetPath');
        return $pluginInfo ? $this->___callPlugins('getWidgetPath', func_get_args(), $pluginInfo) : parent::getWidgetPath($key, $store);
    }

    /**
     * {@inheritdoc}
     */
    public function getLoginScope($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getLoginScope');
        return $pluginInfo ? $this->___callPlugins('getLoginScope', func_get_args(), $pluginInfo) : parent::getLoginScope($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isEuPaymentRegion($scope = 'store')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEuPaymentRegion');
        return $pluginInfo ? $this->___callPlugins('isEuPaymentRegion', func_get_args(), $pluginInfo) : parent::isEuPaymentRegion($scope);
    }

    /**
     * {@inheritdoc}
     */
    public function isSandboxEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isSandboxEnabled');
        return $pluginInfo ? $this->___callPlugins('isSandboxEnabled', func_get_args(), $pluginInfo) : parent::isSandboxEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isPwaEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isPwaEnabled');
        return $pluginInfo ? $this->___callPlugins('isPwaEnabled', func_get_args(), $pluginInfo) : parent::isPwaEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isLwaEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isLwaEnabled');
        return $pluginInfo ? $this->___callPlugins('isLwaEnabled', func_get_args(), $pluginInfo) : parent::isLwaEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isEnabled');
        return $pluginInfo ? $this->___callPlugins('isEnabled', func_get_args(), $pluginInfo) : parent::isEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentAction($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentAction');
        return $pluginInfo ? $this->___callPlugins('getPaymentAction', func_get_args(), $pluginInfo) : parent::getPaymentAction($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorizationMode($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAuthorizationMode');
        return $pluginInfo ? $this->___callPlugins('getAuthorizationMode', func_get_args(), $pluginInfo) : parent::getAuthorizationMode($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdateMechanism($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getUpdateMechanism');
        return $pluginInfo ? $this->___callPlugins('getUpdateMechanism', func_get_args(), $pluginInfo) : parent::getUpdateMechanism($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonDisplayLanguage($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonDisplayLanguage');
        return $pluginInfo ? $this->___callPlugins('getButtonDisplayLanguage', func_get_args(), $pluginInfo) : parent::getButtonDisplayLanguage($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonType($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonType');
        return $pluginInfo ? $this->___callPlugins('getButtonType', func_get_args(), $pluginInfo) : parent::getButtonType($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonTypePwa($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonTypePwa');
        return $pluginInfo ? $this->___callPlugins('getButtonTypePwa', func_get_args(), $pluginInfo) : parent::getButtonTypePwa($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonTypeLwa($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonTypeLwa');
        return $pluginInfo ? $this->___callPlugins('getButtonTypeLwa', func_get_args(), $pluginInfo) : parent::getButtonTypeLwa($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonColor($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonColor');
        return $pluginInfo ? $this->___callPlugins('getButtonColor', func_get_args(), $pluginInfo) : parent::getButtonColor($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonSize($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonSize');
        return $pluginInfo ? $this->___callPlugins('getButtonSize', func_get_args(), $pluginInfo) : parent::getButtonSize($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getEmailStoreName($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getEmailStoreName');
        return $pluginInfo ? $this->___callPlugins('getEmailStoreName', func_get_args(), $pluginInfo) : parent::getEmailStoreName($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getAdditionalAccessScope($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAdditionalAccessScope');
        return $pluginInfo ? $this->___callPlugins('getAdditionalAccessScope', func_get_args(), $pluginInfo) : parent::getAdditionalAccessScope($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isLoggingEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isLoggingEnabled');
        return $pluginInfo ? $this->___callPlugins('isLoggingEnabled', func_get_args(), $pluginInfo) : parent::isLoggingEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreName($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStoreName');
        return $pluginInfo ? $this->___callPlugins('getStoreName', func_get_args(), $pluginInfo) : parent::getStoreName($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreFrontName($storeId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStoreFrontName');
        return $pluginInfo ? $this->___callPlugins('getStoreFrontName', func_get_args(), $pluginInfo) : parent::getStoreFrontName($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getRedirectUrl()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRedirectUrl');
        return $pluginInfo ? $this->___callPlugins('getRedirectUrl', func_get_args(), $pluginInfo) : parent::getRedirectUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function getSandboxSimulationStrings($context = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSandboxSimulationStrings');
        return $pluginInfo ? $this->___callPlugins('getSandboxSimulationStrings', func_get_args(), $pluginInfo) : parent::getSandboxSimulationStrings($context);
    }

    /**
     * {@inheritdoc}
     */
    public function getSandboxSimulationOptions()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSandboxSimulationOptions');
        return $pluginInfo ? $this->___callPlugins('getSandboxSimulationOptions', func_get_args(), $pluginInfo) : parent::getSandboxSimulationOptions();
    }

    /**
     * {@inheritdoc}
     */
    public function isPaymentButtonEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isPaymentButtonEnabled');
        return $pluginInfo ? $this->___callPlugins('isPaymentButtonEnabled', func_get_args(), $pluginInfo) : parent::isPaymentButtonEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isLoginButtonEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isLoginButtonEnabled');
        return $pluginInfo ? $this->___callPlugins('isLoginButtonEnabled', func_get_args(), $pluginInfo) : parent::isLoginButtonEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function isCurrentCurrencySupportedByAmazon()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isCurrentCurrencySupportedByAmazon');
        return $pluginInfo ? $this->___callPlugins('isCurrentCurrencySupportedByAmazon', func_get_args(), $pluginInfo) : parent::isCurrentCurrencySupportedByAmazon();
    }

    /**
     * {@inheritdoc}
     */
    public function getAmazonAccountUrlByPaymentRegion($paymentRegion)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAmazonAccountUrlByPaymentRegion');
        return $pluginInfo ? $this->___callPlugins('getAmazonAccountUrlByPaymentRegion', func_get_args(), $pluginInfo) : parent::getAmazonAccountUrlByPaymentRegion($paymentRegion);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentRegionUrl($key, $store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentRegionUrl');
        return $pluginInfo ? $this->___callPlugins('getPaymentRegionUrl', func_get_args(), $pluginInfo) : parent::getPaymentRegionUrl($key, $store);
    }

    /**
     * {@inheritdoc}
     */
    public function getClientPath($key, $store = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getClientPath');
        return $pluginInfo ? $this->___callPlugins('getClientPath', func_get_args(), $pluginInfo) : parent::getClientPath($key, $store);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlackListedTerms($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBlackListedTerms');
        return $pluginInfo ? $this->___callPlugins('getBlackListedTerms', func_get_args(), $pluginInfo) : parent::getBlackListedTerms($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isBlacklistedTermValidationEnabled($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isBlacklistedTermValidationEnabled');
        return $pluginInfo ? $this->___callPlugins('isBlacklistedTermValidationEnabled', func_get_args(), $pluginInfo) : parent::isBlacklistedTermValidationEnabled($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getOAuthRedirectUrl()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getOAuthRedirectUrl');
        return $pluginInfo ? $this->___callPlugins('getOAuthRedirectUrl', func_get_args(), $pluginInfo) : parent::getOAuthRedirectUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function isPwaButtonVisibleOnProductPage($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isPwaButtonVisibleOnProductPage');
        return $pluginInfo ? $this->___callPlugins('isPwaButtonVisibleOnProductPage', func_get_args(), $pluginInfo) : parent::isPwaButtonVisibleOnProductPage($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function isPayButtonAvailableInMinicart($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isPayButtonAvailableInMinicart');
        return $pluginInfo ? $this->___callPlugins('isPayButtonAvailableInMinicart', func_get_args(), $pluginInfo) : parent::isPayButtonAvailableInMinicart($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function allowAmLoginLoading($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'allowAmLoginLoading');
        return $pluginInfo ? $this->___callPlugins('allowAmLoginLoading', func_get_args(), $pluginInfo) : parent::allowAmLoginLoading($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentialsJson($scope = 'store', $scopeCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCredentialsJson');
        return $pluginInfo ? $this->___callPlugins('getCredentialsJson', func_get_args(), $pluginInfo) : parent::getCredentialsJson($scope, $scopeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getAmazonCredentialsFields()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAmazonCredentialsFields');
        return $pluginInfo ? $this->___callPlugins('getAmazonCredentialsFields', func_get_args(), $pluginInfo) : parent::getAmazonCredentialsFields();
    }

    /**
     * {@inheritdoc}
     */
    public function getAmazonCredentialsEncryptedFields()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAmazonCredentialsEncryptedFields');
        return $pluginInfo ? $this->___callPlugins('getAmazonCredentialsEncryptedFields', func_get_args(), $pluginInfo) : parent::getAmazonCredentialsEncryptedFields();
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getVersion');
        return $pluginInfo ? $this->___callPlugins('getVersion', func_get_args(), $pluginInfo) : parent::getVersion();
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        return $pluginInfo ? $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo) : parent::isModuleOutputEnabled($moduleName);
    }
}
