<?php
namespace StripeIntegration\Payments\Controller\Customer\Cards;

/**
 * Interceptor class for @see \StripeIntegration\Payments\Controller\Customer\Cards
 */
class Interceptor extends \StripeIntegration\Payments\Controller\Customer\Cards implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Customer\Model\Session $session, \StripeIntegration\Payments\Model\Config $config, \StripeIntegration\Payments\Helper\Generic $helper, \Magento\Customer\Model\Customer $customer, \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $session, $config, $helper, $customer, $customerFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function saveCard($params)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'saveCard');
        return $pluginInfo ? $this->___callPlugins('saveCard', func_get_args(), $pluginInfo) : parent::saveCard($params);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteCard($token)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'deleteCard');
        return $pluginInfo ? $this->___callPlugins('deleteCard', func_get_args(), $pluginInfo) : parent::deleteCard($token);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        return $pluginInfo ? $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo) : parent::getActionFlag();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        return $pluginInfo ? $this->___callPlugins('getRequest', func_get_args(), $pluginInfo) : parent::getRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        return $pluginInfo ? $this->___callPlugins('getResponse', func_get_args(), $pluginInfo) : parent::getResponse();
    }
}
