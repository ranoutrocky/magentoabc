<?php
namespace Amasty\CompanyAccount\Api\Data;

/**
 * ExtensionInterface class for @see \Amasty\CompanyAccount\Api\Data\CompanyInterface
 */
interface CompanyExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Amasty\CompanyAccount\Api\Data\CreditInterface|null
     */
    public function getCredit();

    /**
     * @param \Amasty\CompanyAccount\Api\Data\CreditInterface $credit
     * @return $this
     */
    public function setCredit(\Amasty\CompanyAccount\Api\Data\CreditInterface $credit);
}
