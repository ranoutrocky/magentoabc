<?php
namespace Amasty\CompanyAccount\Api\Data;

/**
 * Extension class for @see \Amasty\CompanyAccount\Api\Data\CompanyInterface
 */
class CompanyExtension extends \Magento\Framework\Api\AbstractSimpleObject implements CompanyExtensionInterface
{
    /**
     * @return \Amasty\CompanyAccount\Api\Data\CreditInterface|null
     */
    public function getCredit()
    {
        return $this->_get('credit');
    }

    /**
     * @param \Amasty\CompanyAccount\Api\Data\CreditInterface $credit
     * @return $this
     */
    public function setCredit(\Amasty\CompanyAccount\Api\Data\CreditInterface $credit)
    {
        $this->setData('credit', $credit);
        return $this;
    }
}
