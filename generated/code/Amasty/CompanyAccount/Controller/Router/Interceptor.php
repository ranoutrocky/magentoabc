<?php
namespace Amasty\CompanyAccount\Controller\Router;

/**
 * Interceptor class for @see \Amasty\CompanyAccount\Controller\Router
 */
class Interceptor extends \Amasty\CompanyAccount\Controller\Router implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Amasty\CompanyAccount\Model\ConfigProvider $configProvider, \Magento\Framework\App\ActionFactory $actionFactory)
    {
        $this->___init();
        parent::__construct($configProvider, $actionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'match');
        return $pluginInfo ? $this->___callPlugins('match', func_get_args(), $pluginInfo) : parent::match($request);
    }
}
