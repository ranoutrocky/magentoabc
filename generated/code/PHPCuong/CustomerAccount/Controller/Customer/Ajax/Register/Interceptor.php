<?php
namespace PHPCuong\CustomerAccount\Controller\Customer\Ajax\Register;

/**
 * Interceptor class for @see \PHPCuong\CustomerAccount\Controller\Customer\Ajax\Register
 */
class Interceptor extends \PHPCuong\CustomerAccount\Controller\Customer\Ajax\Register implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory, \Magento\Framework\Controller\Result\RawFactory $resultRawFactory, \Magento\Customer\Model\Session $customerSession, \Magento\Customer\Model\Registration $customerRegistration, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Customer\Api\AccountManagementInterface $accountManagement, \Magento\Customer\Model\CustomerExtractor $customerExtractor, \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory, \Magento\Customer\Model\Url $customerUrl, \Magento\Framework\UrlInterface $urlModel, \Magento\Customer\Helper\Address $addressHelper, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Customer\Model\Account\Redirect $accountRedirect, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Framework\Escaper $escaper, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, \Magento\Framework\Stdlib\Cookie\PhpCookieManager $cookieMetadataManager)
    {
        $this->___init();
        parent::__construct($context, $resultJsonFactory, $resultRawFactory, $customerSession, $customerRegistration, $formKeyValidator, $accountManagement, $customerExtractor, $subscriberFactory, $customerUrl, $urlModel, $addressHelper, $storeManager, $accountRedirect, $scopeConfig, $escaper, $cookieMetadataFactory, $cookieMetadataManager);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        return $pluginInfo ? $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo) : parent::getActionFlag();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        return $pluginInfo ? $this->___callPlugins('getRequest', func_get_args(), $pluginInfo) : parent::getRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        return $pluginInfo ? $this->___callPlugins('getResponse', func_get_args(), $pluginInfo) : parent::getResponse();
    }
}
