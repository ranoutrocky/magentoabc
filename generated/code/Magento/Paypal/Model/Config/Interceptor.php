<?php
namespace Magento\Paypal\Model\Config;

/**
 * Interceptor class for @see \Magento\Paypal\Model\Config
 */
class Interceptor extends \Magento\Paypal\Model\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Directory\Helper\Data $directoryHelper, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Payment\Model\Source\CctypeFactory $cctypeFactory, \Magento\Paypal\Model\CertFactory $certFactory, $params = [])
    {
        $this->___init();
        parent::__construct($scopeConfig, $directoryHelper, $storeManager, $cctypeFactory, $certFactory, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function isMethodAvailable($methodCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMethodAvailable');
        return $pluginInfo ? $this->___callPlugins('isMethodAvailable', func_get_args(), $pluginInfo) : parent::isMethodAvailable($methodCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedMerchantCountryCodes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSupportedMerchantCountryCodes');
        return $pluginInfo ? $this->___callPlugins('getSupportedMerchantCountryCodes', func_get_args(), $pluginInfo) : parent::getSupportedMerchantCountryCodes();
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedBuyerCountryCodes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSupportedBuyerCountryCodes');
        return $pluginInfo ? $this->___callPlugins('getSupportedBuyerCountryCodes', func_get_args(), $pluginInfo) : parent::getSupportedBuyerCountryCodes();
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantCountry()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMerchantCountry');
        return $pluginInfo ? $this->___callPlugins('getMerchantCountry', func_get_args(), $pluginInfo) : parent::getMerchantCountry();
    }

    /**
     * {@inheritdoc}
     */
    public function isMethodSupportedForCountry($method = null, $countryCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMethodSupportedForCountry');
        return $pluginInfo ? $this->___callPlugins('isMethodSupportedForCountry', func_get_args(), $pluginInfo) : parent::isMethodSupportedForCountry($method, $countryCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryMethods($countryCode = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCountryMethods');
        return $pluginInfo ? $this->___callPlugins('getCountryMethods', func_get_args(), $pluginInfo) : parent::getCountryMethods($countryCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getPayPalBasicStartUrl($token)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPayPalBasicStartUrl');
        return $pluginInfo ? $this->___callPlugins('getPayPalBasicStartUrl', func_get_args(), $pluginInfo) : parent::getPayPalBasicStartUrl($token);
    }

    /**
     * {@inheritdoc}
     */
    public function isOrderReviewStepDisabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isOrderReviewStepDisabled');
        return $pluginInfo ? $this->___callPlugins('isOrderReviewStepDisabled', func_get_args(), $pluginInfo) : parent::isOrderReviewStepDisabled();
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutStartUrl($token)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutStartUrl');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutStartUrl', func_get_args(), $pluginInfo) : parent::getExpressCheckoutStartUrl($token);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutOrderUrl($orderId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutOrderUrl');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutOrderUrl', func_get_args(), $pluginInfo) : parent::getExpressCheckoutOrderUrl($orderId);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutEditUrl($token)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutEditUrl');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutEditUrl', func_get_args(), $pluginInfo) : parent::getExpressCheckoutEditUrl($token);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutCompleteUrl($token)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutCompleteUrl');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutCompleteUrl', func_get_args(), $pluginInfo) : parent::getExpressCheckoutCompleteUrl($token);
    }

    /**
     * {@inheritdoc}
     */
    public function getStartBillingAgreementUrl($token)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStartBillingAgreementUrl');
        return $pluginInfo ? $this->___callPlugins('getStartBillingAgreementUrl', func_get_args(), $pluginInfo) : parent::getStartBillingAgreementUrl($token);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaypalUrl(array $params = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaypalUrl');
        return $pluginInfo ? $this->___callPlugins('getPaypalUrl', func_get_args(), $pluginInfo) : parent::getPaypalUrl($params);
    }

    /**
     * {@inheritdoc}
     */
    public function getPayPalIpnUrl()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPayPalIpnUrl');
        return $pluginInfo ? $this->___callPlugins('getPayPalIpnUrl', func_get_args(), $pluginInfo) : parent::getPayPalIpnUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function areButtonsDynamic()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'areButtonsDynamic');
        return $pluginInfo ? $this->___callPlugins('areButtonsDynamic', func_get_args(), $pluginInfo) : parent::areButtonsDynamic();
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutShortcutImageUrl($localeCode, $orderTotal = null, $pal = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutShortcutImageUrl');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutShortcutImageUrl', func_get_args(), $pluginInfo) : parent::getExpressCheckoutShortcutImageUrl($localeCode, $orderTotal, $pal);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutInContextImageUrl($localeCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutInContextImageUrl');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutInContextImageUrl', func_get_args(), $pluginInfo) : parent::getExpressCheckoutInContextImageUrl($localeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentMarkImageUrl($localeCode, $orderTotal = null, $pal = null, $staticSize = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentMarkImageUrl');
        return $pluginInfo ? $this->___callPlugins('getPaymentMarkImageUrl', func_get_args(), $pluginInfo) : parent::getPaymentMarkImageUrl($localeCode, $orderTotal, $pal, $staticSize);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentMarkWhatIsPaypalUrl(?\Magento\Framework\Locale\ResolverInterface $localeResolver = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentMarkWhatIsPaypalUrl');
        return $pluginInfo ? $this->___callPlugins('getPaymentMarkWhatIsPaypalUrl', func_get_args(), $pluginInfo) : parent::getPaymentMarkWhatIsPaypalUrl($localeResolver);
    }

    /**
     * {@inheritdoc}
     */
    public function getSolutionImageUrl($localeCode, $isVertical = false, $isEcheck = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getSolutionImageUrl');
        return $pluginInfo ? $this->___callPlugins('getSolutionImageUrl', func_get_args(), $pluginInfo) : parent::getSolutionImageUrl($localeCode, $isVertical, $isEcheck);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentFormLogoUrl($localeCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentFormLogoUrl');
        return $pluginInfo ? $this->___callPlugins('getPaymentFormLogoUrl', func_get_args(), $pluginInfo) : parent::getPaymentFormLogoUrl($localeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getAdditionalOptionsLogoTypes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAdditionalOptionsLogoTypes');
        return $pluginInfo ? $this->___callPlugins('getAdditionalOptionsLogoTypes', func_get_args(), $pluginInfo) : parent::getAdditionalOptionsLogoTypes();
    }

    /**
     * {@inheritdoc}
     */
    public function getAdditionalOptionsLogoUrl($localeCode, $type = false)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAdditionalOptionsLogoUrl');
        return $pluginInfo ? $this->___callPlugins('getAdditionalOptionsLogoUrl', func_get_args(), $pluginInfo) : parent::getAdditionalOptionsLogoUrl($localeCode, $type);
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutButtonFlavors()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutButtonFlavors');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutButtonFlavors', func_get_args(), $pluginInfo) : parent::getExpressCheckoutButtonFlavors();
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutButtonTypes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutButtonTypes');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutButtonTypes', func_get_args(), $pluginInfo) : parent::getExpressCheckoutButtonTypes();
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentActions()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentActions');
        return $pluginInfo ? $this->___callPlugins('getPaymentActions', func_get_args(), $pluginInfo) : parent::getPaymentActions();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequireBillingAddressOptions()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequireBillingAddressOptions');
        return $pluginInfo ? $this->___callPlugins('getRequireBillingAddressOptions', func_get_args(), $pluginInfo) : parent::getRequireBillingAddressOptions();
    }

    /**
     * {@inheritdoc}
     */
    public function getPaymentAction()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPaymentAction');
        return $pluginInfo ? $this->___callPlugins('getPaymentAction', func_get_args(), $pluginInfo) : parent::getPaymentAction();
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutSolutionTypes()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutSolutionTypes');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutSolutionTypes', func_get_args(), $pluginInfo) : parent::getExpressCheckoutSolutionTypes();
    }

    /**
     * {@inheritdoc}
     */
    public function getExpressCheckoutBASignupOptions()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getExpressCheckoutBASignupOptions');
        return $pluginInfo ? $this->___callPlugins('getExpressCheckoutBASignupOptions', func_get_args(), $pluginInfo) : parent::getExpressCheckoutBASignupOptions();
    }

    /**
     * {@inheritdoc}
     */
    public function shouldAskToCreateBillingAgreement()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'shouldAskToCreateBillingAgreement');
        return $pluginInfo ? $this->___callPlugins('shouldAskToCreateBillingAgreement', func_get_args(), $pluginInfo) : parent::shouldAskToCreateBillingAgreement();
    }

    /**
     * {@inheritdoc}
     */
    public function getWpsPaymentDeliveryMethods()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getWpsPaymentDeliveryMethods');
        return $pluginInfo ? $this->___callPlugins('getWpsPaymentDeliveryMethods', func_get_args(), $pluginInfo) : parent::getWpsPaymentDeliveryMethods();
    }

    /**
     * {@inheritdoc}
     */
    public function getWppCcTypesAsOptionArray()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getWppCcTypesAsOptionArray');
        return $pluginInfo ? $this->___callPlugins('getWppCcTypesAsOptionArray', func_get_args(), $pluginInfo) : parent::getWppCcTypesAsOptionArray();
    }

    /**
     * {@inheritdoc}
     */
    public function getWppPeCcTypesAsOptionArray()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getWppPeCcTypesAsOptionArray');
        return $pluginInfo ? $this->___callPlugins('getWppPeCcTypesAsOptionArray', func_get_args(), $pluginInfo) : parent::getWppPeCcTypesAsOptionArray();
    }

    /**
     * {@inheritdoc}
     */
    public function getPayflowproCcTypesAsOptionArray()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPayflowproCcTypesAsOptionArray');
        return $pluginInfo ? $this->___callPlugins('getPayflowproCcTypesAsOptionArray', func_get_args(), $pluginInfo) : parent::getPayflowproCcTypesAsOptionArray();
    }

    /**
     * {@inheritdoc}
     */
    public function isCurrencyCodeSupported($code)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isCurrencyCodeSupported');
        return $pluginInfo ? $this->___callPlugins('isCurrencyCodeSupported', func_get_args(), $pluginInfo) : parent::isCurrencyCodeSupported($code);
    }

    /**
     * {@inheritdoc}
     */
    public function exportExpressCheckoutStyleSettings(\Magento\Framework\DataObject $to)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'exportExpressCheckoutStyleSettings');
        return $pluginInfo ? $this->___callPlugins('exportExpressCheckoutStyleSettings', func_get_args(), $pluginInfo) : parent::exportExpressCheckoutStyleSettings($to);
    }

    /**
     * {@inheritdoc}
     */
    public function getApiAuthenticationMethods()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiAuthenticationMethods');
        return $pluginInfo ? $this->___callPlugins('getApiAuthenticationMethods', func_get_args(), $pluginInfo) : parent::getApiAuthenticationMethods();
    }

    /**
     * {@inheritdoc}
     */
    public function getApiCertificate()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getApiCertificate');
        return $pluginInfo ? $this->___callPlugins('getApiCertificate', func_get_args(), $pluginInfo) : parent::getApiCertificate();
    }

    /**
     * {@inheritdoc}
     */
    public function getBmlPublisherId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBmlPublisherId');
        return $pluginInfo ? $this->___callPlugins('getBmlPublisherId', func_get_args(), $pluginInfo) : parent::getBmlPublisherId();
    }

    /**
     * {@inheritdoc}
     */
    public function getBmlDisplay($section)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBmlDisplay');
        return $pluginInfo ? $this->___callPlugins('getBmlDisplay', func_get_args(), $pluginInfo) : parent::getBmlDisplay($section);
    }

    /**
     * {@inheritdoc}
     */
    public function getBmlPosition($section)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBmlPosition');
        return $pluginInfo ? $this->___callPlugins('getBmlPosition', func_get_args(), $pluginInfo) : parent::getBmlPosition($section);
    }

    /**
     * {@inheritdoc}
     */
    public function getBmlSize($section)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBmlSize');
        return $pluginInfo ? $this->___callPlugins('getBmlSize', func_get_args(), $pluginInfo) : parent::getBmlSize($section);
    }

    /**
     * {@inheritdoc}
     */
    public function setMethodInstance($method)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setMethodInstance');
        return $pluginInfo ? $this->___callPlugins('setMethodInstance', func_get_args(), $pluginInfo) : parent::setMethodInstance($method);
    }

    /**
     * {@inheritdoc}
     */
    public function setMethod($method)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setMethod');
        return $pluginInfo ? $this->___callPlugins('setMethod', func_get_args(), $pluginInfo) : parent::setMethod($method);
    }

    /**
     * {@inheritdoc}
     */
    public function getMethodCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMethodCode');
        return $pluginInfo ? $this->___callPlugins('getMethodCode', func_get_args(), $pluginInfo) : parent::getMethodCode();
    }

    /**
     * {@inheritdoc}
     */
    public function setStoreId($storeId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setStoreId');
        return $pluginInfo ? $this->___callPlugins('setStoreId', func_get_args(), $pluginInfo) : parent::setStoreId($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getValue($key, $storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getValue');
        return $pluginInfo ? $this->___callPlugins('getValue', func_get_args(), $pluginInfo) : parent::getValue($key, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function setMethodCode($methodCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setMethodCode');
        return $pluginInfo ? $this->___callPlugins('setMethodCode', func_get_args(), $pluginInfo) : parent::setMethodCode($methodCode);
    }

    /**
     * {@inheritdoc}
     */
    public function setPathPattern($pathPattern)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPathPattern');
        return $pluginInfo ? $this->___callPlugins('setPathPattern', func_get_args(), $pluginInfo) : parent::setPathPattern($pathPattern);
    }

    /**
     * {@inheritdoc}
     */
    public function shouldUseUnilateralPayments()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'shouldUseUnilateralPayments');
        return $pluginInfo ? $this->___callPlugins('shouldUseUnilateralPayments', func_get_args(), $pluginInfo) : parent::shouldUseUnilateralPayments();
    }

    /**
     * {@inheritdoc}
     */
    public function isWppApiAvailabe()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isWppApiAvailabe');
        return $pluginInfo ? $this->___callPlugins('isWppApiAvailabe', func_get_args(), $pluginInfo) : parent::isWppApiAvailabe();
    }

    /**
     * {@inheritdoc}
     */
    public function isWppApiAvailable()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isWppApiAvailable');
        return $pluginInfo ? $this->___callPlugins('isWppApiAvailable', func_get_args(), $pluginInfo) : parent::isWppApiAvailable();
    }

    /**
     * {@inheritdoc}
     */
    public function isMethodActive($method)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMethodActive');
        return $pluginInfo ? $this->___callPlugins('isMethodActive', func_get_args(), $pluginInfo) : parent::isMethodActive($method);
    }

    /**
     * {@inheritdoc}
     */
    public function getBuildNotationCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getBuildNotationCode');
        return $pluginInfo ? $this->___callPlugins('getBuildNotationCode', func_get_args(), $pluginInfo) : parent::getBuildNotationCode();
    }

    /**
     * {@inheritdoc}
     */
    public function formatPrice($price)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'formatPrice');
        return $pluginInfo ? $this->___callPlugins('formatPrice', func_get_args(), $pluginInfo) : parent::formatPrice($price);
    }
}
