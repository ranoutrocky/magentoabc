<?php
namespace Venture7\BusinessAccount\Controller\Index\Save;

/**
 * Interceptor class for @see \Venture7\BusinessAccount\Controller\Index\Save
 */
class Interceptor extends \Venture7\BusinessAccount\Controller\Index\Save implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Amasty\CompanyAccount\Model\CompanyContext $companyContext, \Psr\Log\LoggerInterface $logger, \Magento\Customer\Model\Session $customerSession, \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository, \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator, \Magento\Customer\Model\Customer $customer, \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory, \Venture7\BusinessAccount\Model\BPRefFactory $bprefFactory, \Amasty\CompanyAccount\Api\Data\CompanyInterfaceFactory $companyFactory, \Amasty\CompanyAccount\Api\CompanyRepositoryInterface $companyRepository, \Magento\Customer\Api\AddressRepositoryInterface $addressRepository, \Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory, \Amasty\CompanyAccount\Model\ConfigProvider $configProvider, \Amasty\CompanyAccount\Api\RoleRepositoryInterface $roleRepository, \Amasty\CompanyAccount\Api\Data\RoleInterfaceFactory $roleFactory, \Amasty\CompanyAccount\Model\Repository\PermissionRepository $permissionRepository)
    {
        $this->___init();
        parent::__construct($context, $companyContext, $logger, $customerSession, $customerRepository, $formKeyValidator, $customer, $customerFactory, $bprefFactory, $companyFactory, $companyRepository, $addressRepository, $userCollectionFactory, $configProvider, $roleRepository, $roleFactory, $permissionRepository);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        return $pluginInfo ? $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo) : parent::getActionFlag();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        return $pluginInfo ? $this->___callPlugins('getRequest', func_get_args(), $pluginInfo) : parent::getRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        return $pluginInfo ? $this->___callPlugins('getResponse', func_get_args(), $pluginInfo) : parent::getResponse();
    }
}
