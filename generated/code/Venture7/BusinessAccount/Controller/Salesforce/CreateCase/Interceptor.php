<?php
namespace Venture7\BusinessAccount\Controller\Salesforce\CreateCase;

/**
 * Interceptor class for @see \Venture7\BusinessAccount\Controller\Salesforce\CreateCase
 */
class Interceptor extends \Venture7\BusinessAccount\Controller\Salesforce\CreateCase implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Customer\Model\Session $session, \Magento\Customer\Model\Customer $customer, \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory, \Magento\Customer\Api\AddressRepositoryInterface $addressRepository, \Magento\Framework\View\Result\PageFactory $pageFactory, \Magento\Framework\HTTP\Client\Curl $curl)
    {
        $this->___init();
        parent::__construct($context, $session, $customer, $customerFactory, $addressRepository, $pageFactory, $curl);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        return $pluginInfo ? $this->___callPlugins('execute', func_get_args(), $pluginInfo) : parent::execute();
    }

    /**
     * {@inheritdoc}
     */
    public function getRegion($address)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRegion');
        return $pluginInfo ? $this->___callPlugins('getRegion', func_get_args(), $pluginInfo) : parent::getRegion($address);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        return $pluginInfo ? $this->___callPlugins('dispatch', func_get_args(), $pluginInfo) : parent::dispatch($request);
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        return $pluginInfo ? $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo) : parent::getActionFlag();
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        return $pluginInfo ? $this->___callPlugins('getRequest', func_get_args(), $pluginInfo) : parent::getRequest();
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        return $pluginInfo ? $this->___callPlugins('getResponse', func_get_args(), $pluginInfo) : parent::getResponse();
    }
}
