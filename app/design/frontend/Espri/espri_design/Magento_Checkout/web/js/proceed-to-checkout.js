/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'Magento_Customer/js/model/authentication-popup',
    'Magento_Customer/js/customer-data'
], function ($, authenticationPopup, customerData) {
    'use strict';

    return function (config, element) {
        $(element).click(function (event) {
          var cart = customerData.get('cart'),
          customer = customerData.get('customer');
          
          event.preventDefault();

            if (!customer().firstname && cart().isGuestCheckoutAllowed === false) {
                $(".customer-login-link").click();
                return false;
            }
            location.href = 'https://devorder.esprigas.com//onestepcheckout/';
            //$(element).attr('disabled', true);
            // $.ajax({
            //     type: 'post',
            //     url: 'https://devorder.esprigas.com//custommodule/index/checkqty',
            //     data: 'check',
            //     success: function (rs) {
            //       if(rs=="active"){
            //         location.href = 'https://devorder.esprigas.com//once/';
            //       }else{
            //         location.reload();
            //       }
            //     }
            //   });
            //
        });

    };
});
