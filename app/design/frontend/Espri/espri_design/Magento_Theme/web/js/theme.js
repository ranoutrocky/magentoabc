/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], function ($, keyboardHandler) {
    'use strict';


    $(window).scroll(function () {
        var header = $('.navbar'),
            scroll = $(window).scrollTop();

        if (scroll >= 400) header.addClass('navbar-fixed');
        else header.removeClass('navbar-fixed');
    });

    function openNav() {
        $('#sidebar').addClass('active');
        $('.menu-overlay').fadeIn();
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    };

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.menu-overlay').fadeOut();
    });

    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

    });

    // Show Login Pop up window
    $('#loginMenu').click(function () {
        $('#content').show();
    });
	$('#signupMenu').click(function () {
        $('#signupcontent').show();
    });
	
	$('#loginModal').on('show.bs.modal', function (e) {
	$('#signupModal').modal('hide')
  });
  $('#signupModal').on('show.bs.modal', function (e) {
	$('#loginModal').modal('hide')
  });
        

    keyboardHandler.apply();
});
