<?php

/**
 * @category  Dotsquares
 * @package   Dotsquares_CustomAttribute
 * @author    Dotsquares Team <info@dotsquares.com>
 * @copyright 2020 Dotsquares (https://www.dotsquares.com/)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Dotsquares_CustomAttribute',
    __DIR__
);
