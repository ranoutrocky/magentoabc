<?php

/**
 * @category  Dotsquares
 * @package   Dotsquares_CustomAttribute
 * @author    Dotsquares Team <info@dotsquares.com>
 * @copyright 2020 Dotsquares (https://www.dotsquares.com/)
 */

namespace Dotsquares\CustomAttribute\Block\Adminhtml\Attribute\Edit\Options;

/**
 * Labels block
 */
class Labels extends \Magento\Eav\Block\Adminhtml\Attribute\Edit\Options\Labels
{
    /**
     * @var string
     */
    protected $_template = 'Dotsquares_CustomAttribute::customer/attribute/labels.phtml';

    /**
     * Retrieve attribute object from registry
     *
     * @return \Magento\Eav\Model\Entity\Attribute\AbstractAttribute
     * @codeCoverageIgnore
     */
    private function getAttributeObject()
    {
        return $this->_registry->registry('entity_attribute');
    }
}
