<?php

/**
 * @category  Dotsquares
 * @package   Dotsquares_CustomAttribute
 * @author    Dotsquares Team <info@dotsquares.com>
 * @copyright 2020 Dotsquares (https://www.dotsquares.com/)
 */

namespace Dotsquares\CustomAttribute\Block\Adminhtml\Attribute\Edit\Options;

/**
 * Options block
 */
class Options extends \Magento\Eav\Block\Adminhtml\Attribute\Edit\Options\Options
{
    /**
     * @var string
     */
    protected $_template = 'Dotsquares_CustomAttribute::customer/attribute/options.phtml';
}
