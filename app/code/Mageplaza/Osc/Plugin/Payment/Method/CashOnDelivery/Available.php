<?php

namespace Mageplaza\Osc\Plugin\Payment\Method\CashOnDelivery;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Backend\Model\Auth\Session as BackendSession;
use Magento\OfflinePayments\Model\Cashondelivery;

class Available
{

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var BackendSession
     */
    protected $backendSession;

     /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * @param CustomerSession $customerSession
     * @param BackendSession $backendSession
     */
    public function __construct(
        CustomerSession $customerSession,
        BackendSession $backendSession,
        \Magento\Customer\Model\Customer $customer
    ) {
        $this->customerSession = $customerSession;
        $this->backendSession = $backendSession;
        $this->customer = $customer;
    }

    /**
     *
     * @param Cashondelivery $subject
     * @param $result
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function afterIsAvailable(Cashondelivery $subject, $result)
    {
        // Do not remove payment method for admin
        if ($this->backendSession->isLoggedIn()) {
            return $result;
        }

        $customerId = $this->customerSession->getCustomerId();
        $customer = $this->customer->load($customerId);

        if($customer->getNavAch()) {
            return true;
        }

        return false;
    }
}