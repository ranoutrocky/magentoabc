<?php
/**
 * Copyright © 2015 EspriModule . All rights reserved.
 */
namespace EspriModule\Custommodule\Block\Index;

class Allproduct extends \Magento\Framework\View\Element\Template
{	protected $_productCollectionFactory;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        
        array $data = []
		)
	{	$this->_productCollectionFactory = $productCollectionFactory;    
        parent::__construct($context, $data);
	}

    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->setPageSize(3); // fetching only 3 products
        return $collection;
    }
	
}
