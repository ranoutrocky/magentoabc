<?php

/**
 * Copyright © 2015 EspriModule . All rights reserved.
 */

namespace EspriModule\Custommodule\Block\Index;

use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\View\Element\Template;

class Index extends Template
{
	protected $customerSession;
	protected $orderCollectionFactory;
	protected $priceHelper;
	protected $customAttribute;

	/**
	 * Construct
	 *
	 * @param \Magento\Framework\View\Element\Template\Context $context
	 * @param \Magento\Customer\Model\Session $customerSession
	 * @param array $data
	 */
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
		\Magento\Catalog\Model\ProductFactory $_productloader,
		\Magento\Catalog\Block\Product\ListProduct $listBlock,
		\Magento\Framework\Pricing\Helper\Data $priceHelper,
		\EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace\Collection $rentalbalace,
		\EspriModule\Custommodule\Helper\CustomAttribute $customAttribute,
		DateTimeFactory $dateTimeFactory,

		array $data = []
	) {
		parent::__construct($context, $data);
		$this->rentalbalance = $rentalbalace;
		$this->_productloader = $_productloader;
		$this->orderCollectionFactory = $orderCollectionFactory;
		$this->priceHelper = $priceHelper;
		$this->customerSession = $customerSession;
		$this->dateTimeFactory = $dateTimeFactory;
		$this->listBlock = $listBlock;
		$this->customAttribute = $customAttribute;
	}

	public function getRentalbalance($customerId)
	{
		$post = $this->rentalbalance
			->addFieldToFilter('customerid', $customerId);
		return $post->getData();
	}
	public function getRentalbalancetotel($customerId)
	{
		$post = $this->rentalbalance
			->addFieldToFilter('customerid', $customerId);
		$totel = [];
		foreach ($post->getData() as $value) {
			$totel[] = $value['qty'];
		}
		return array_sum($totel);
	}
	public function getRentalbalancedate($customerId)
	{
		$post = $this->rentalbalance
			->addFieldToFilter('customerid', $customerId);
		$date_array = [];
		foreach ($post->getData() as $value) {
			$date_array[] = $value['asofdate'];
		}
		return end($date_array);
	}
	public function getCurentcustomer()
	{
		return $this->customerSession->getCustomer()->getId();
	}

	public function getCurentcustomertimezone()
	{
		return $this->customerSession->getCustomer()->getTimeZone();
	}


	public function getFormattedPrice($price)
	{
		return $this->priceHelper->currency($price, true, false);
	}
	 public function deliveryDay()
	{
		return $this->customerSession->getCustomer()->getDeliveryDaysId();
	}
	public function getLastestOrderDate($id)
	{
		$customerId = $id;
		$collection = $this->getOrder($customerId);
		if ($collection->getData()) {

			$i = 0;
			foreach ($collection->getData() as $key => $value) {
				if ($i == 0) {
					$date = $value['created_at'];

					break;
				}

				$i++;
			}
			$dateModel = $this->dateTimeFactory->create();
			return date("m/d/Y", strtotime($dateModel->gmtDate($date)));
		} else {
			return "No order placed";
		}
	}

	public function getLastestOrderId($id)
	{
		$customerId = $id;
		$collection = $this->getOrder($customerId);
		if ($collection->getData()) {

			$i = 0;
			foreach ($collection->getData() as $key => $value) {
				if ($i == 0) {
					$orderid = $value['entity_id'];

					break;
				}

				$i++;
			}
			
			return $orderid;
		} else {
			return "No order placed";
		}
	}
	
	public function getnextOrderDate($id)
	{
		$customerId = $id;
		$collection = $this->getOrder($customerId);
		if ($collection->getData()) {

			$i = 0;
			foreach ($collection->getData() as $key => $value) {
				if ($i == 0) {
					$date = $value['created_at'];

					break;
				}

				$i++;
			}
			$dateModel = $this->dateTimeFactory->create();
			
			// calculate next expected order date
			//$date = strtotime("+80 day", strtotime($dateModel->gmtDate($date)));
			//$date = $this->calculateNextOrderDate($date);
			if($this->getAttributeValue('next_order_date')) {
				$date = strtotime($this->getAttributeValue('next_order_date'));
				return date('m/d/Y', $date);
			}
			else {
				return "Due Now";
			}

		} else {
			return "Due Now";
		}
	}

	public function calculateNextOrderDate($orderDate)
	{
		$dateModel = $this->dateTimeFactory->create();

		if($this->getAttributeValue('next_order_date')) {
			$date = strtotime($this->getAttributeValue('next_order_date'));
		}
		else {
			if($this->getAttributeValue('order_frequency')) {
				$days = $this->getAttributeValue('order_frequency');
				$date = strtotime("+$days day", strtotime($dateModel->gmtDate($orderDate)));
			}
			else {
				$date = strtotime("+80 day", strtotime($dateModel->gmtDate($orderDate)));
			}
		}

		return $date;
	}

	public function getremainDays($id)
	{
		$customerId = $id;
		$collection = $this->getOrder($customerId);
		if ($collection->getData()) {

			$i = 0;
			foreach ($collection->getData() as $key => $value) {
				if ($i == 0) {
					$date = $value['created_at'];

					break;
				}

				$i++;
			}
			$dateModel = $this->dateTimeFactory->create();
			//$date = strtotime("+80 day", strtotime($dateModel->gmtDate($date)));
			//$date = $this->calculateNextOrderDate($date);

			if($this->getAttributeValue('next_order_date')) {
				$date = strtotime($this->getAttributeValue('next_order_date'));
				
				$date1 = date_create(date('m/d/Y'));  //current date or any date
				$date2 = date_create(date('m/d/Y', $date));   //Future date
				$diff = date_diff($date1, $date2);
				
				$days = $diff->format("%R%a days");

				$remainingDays = "$days Left";
			}
		} else {
			return "No order placed";
		}
	}


	public function getOrder($id)
	{
		$customerId = $id;
		$customerOrder = $this->orderCollectionFactory->create()
			->addFieldToFilter('customer_id', $customerId)->setOrder(
				'created_at',
				'desc'
			);
		$customerOrder->getData();
		return $customerOrder;
	}
	
	public function getOrderdue($id)
	{
		$customerId = $id;
		$customerOrder = $this->orderCollectionFactory->create()
			->addFieldToFilter('customer_id', $customerId)->addAttributeToFilter('status', ['eq' => 'processing']);

		$grandtotel = [];
		$orderCollection = $customerOrder;
		if ($orderCollection && count($orderCollection) > 0) {
			foreach ($orderCollection as $orders) {

				$grandtotel[] = $orders->getGrandTotal();
			}
		}
		return $this->getFormattedPrice(array_sum($grandtotel));
	}
	public function getOrderlists($id)
	{
		$customerId = $id;
		$collection = $this->getOrder($customerId);
		//$date = $collection[0]->getCreatedAt();
		$i = 0;
		if ($collection->getData()) {
			foreach ($collection as $key => $value) {

				foreach ($value->getAllVisibleItems() as $item) {
					$products[] = $item->getProductId();
				}

				$i++;
			}
			$products = array_count_values($products);
			return $products;
		}
	}
	public function gettoptwo($id)
	{
		$orderlist = $this->getOrderlists($id);

		if (isset($orderlist)) {
			$pID = array();
			if (count($orderlist) > 1) {
				$firstproduct = array_search(max($orderlist), $orderlist);
				$pID[] = $firstproduct;
				foreach ($orderlist as $key => $value) {
					if ($key == $firstproduct) {
						unset($orderlist[$firstproduct]);
					}
				}
			}


			$pID[] = array_search(max($orderlist), $orderlist);
			$productdata = array();
			foreach ($pID as $value) {
				$productdata[] = $this->getLoadProduct($value);
			}
			return $productdata;
		}
	}
	public function getLoadProduct($id)
	{
		$productdata = array();

		$product =  $this->_productloader->create()->load($id);
		$productdata['name'] = $product->getName();
		$productdata['image'] = $product->getData('image');
		$productdata['price'] = $this->getFormattedPrice($product->getPrice());
		$productdata['url'] = $this->listBlock->getAddToCartUrl($product);
		return $productdata;
	}
	public function getproductbysku($productsku)
	{

		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$product = $objectManager->get('Magento\Catalog\Api\ProductRepositoryInterface')->get($productsku);
		return $product->getName();
	}
	public function getRecentProduct($id)
	{

		$collection = $this->getOrder($id);
		if ($collection->getData()) {
			$orderdata = array();
			$i = 0;
			foreach ($collection as $key => $value) {
				$orderdata[$i]['id'] = $value->getEntityId();
				$orderdata[$i]['orderid'] = $value->getIncrementId();
				$date = $value->getCreatedAt();
				$dateModel = $this->dateTimeFactory->create();
				$orderdata[$i]['createat'] =  date("m/d/y", strtotime($dateModel->gmtDate($date)));
				$orderdata[$i]['grandtotel'] = $this->getFormattedPrice($value->getGrandTotal());
				$orderdata[$i]['status'] = $value->getStatus();
				if ($i == '2') {
					break;
				}

				$i++;
			}



			return $orderdata;
		}
	}
	public function getOutstanding($id)
	{

		$collection = $this->getOrder($id);
		if ($collection->getData()) {
			$orderdata = array();
			$i = 0;
			foreach ($collection as $key => $value) {
				$orderdata[$i]['id'] = $value->getEntityId();
				$orderdata[$i]['orderid'] = $value->getIncrementId();
				$date = $value->getCreatedAt();
				$dateModel = $this->dateTimeFactory->create();
				$orderdata[$i]['createat'] =  date("m/d/y", strtotime($dateModel->gmtDate($date)));
				$orderdata[$i]['grandtotel'] = $this->getFormattedPrice($value->getGrandTotal());
				$orderdata[$i]['status'] = $value->getStatus();
				if ($i == '2') {
					break;
				}

				$i++;
			}
			return $orderdata;
		}
	}
	public function getChartData($id)
	{

		$customerId = $id;

		$today = date("Y-m-d h:i:s");
		$month = date("M-Y");

		$timestamp    = strtotime($month);
		$first_second = date('Y-m-01 00:00:00', $timestamp);

		// echo $last_second  = date('m-t-Y 12:59:59', $timestamp);
		$startDate = date("Y-m-d h:i:s", strtotime($today)); // start date
		// echo  "date".$endDate = strtotime("Y-m-d h:i:s", strtotime($first_second)); // end date
		// exit;

		$orders = $this->orderCollectionFactory->create()->addFieldToFilter('customer_id', $customerId)
			->addAttributeToFilter('created_at', array('from' => $first_second, 'to' => $startDate));
		$totelpending = array();
		$totelcomplete = array();

		foreach ($orders as $key => $orderlist) {
			//echo $orderlist->getGrandTotal();
			if ($orderlist->getStatus() != "complete") {
				$totelpending[] = $orderlist->getGrandTotal();
			} else {
				$totelcomplete[] = $orderlist->getGrandTotal();
			}
		}
		$totelpending = array_sum($totelpending);
		$totelcomplete = array_sum($totelcomplete);

		$first_second1 = date('Y-m-01 00:00:00', strtotime('-1 month'));
		$month2 = date("M-Y", strtotime('-1 month'));
		// echo $last_second  = date('m-t-Y 12:59:59', $timestamp);
		$startDate1 = date('Y-m-t 12:59:59', strtotime('-1 month')); // start date
		// echo  "date".$endDate = strtotime("Y-m-d h:i:s", strtotime($first_second)); // end date


		$orders1 = $this->orderCollectionFactory->create()->addFieldToFilter('customer_id', $customerId)
			->addAttributeToFilter('created_at', array('from' => $first_second1, 'to' => $startDate1));
		$totelpending1 = array();
		$totelcomplete1 = array();

		foreach ($orders1 as $key => $orderlist1) {
			//echo $orderlist->getGrandTotal();
			if ($orderlist1->getStatus() != "complete") {
				$totelpending1[] = $orderlist1->getGrandTotal();
			} else {
				$totelcomplete1[] = $orderlist1->getGrandTotal();
			}
		}
		$totelpending1 = array_sum($totelpending1);
		$totelcomplete1 = array_sum($totelcomplete1);
		// $totelpending1 = '560';
		// $totelcomplete1 = '320';

		$first_second2 = date('Y-m-01 00:00:00', strtotime('-2 month'));
		$month3 = date("M-Y", strtotime('-2 month'));
		// echo $last_second  = date('m-t-Y 12:59:59', $timestamp);
		$startDate2 = date('Y-m-t 12:59:59', strtotime('-2 month')); // start date
		// echo  "date".$endDate = strtotime("Y-m-d h:i:s", strtotime($first_second)); // end date


		$orders2 = $this->orderCollectionFactory->create()->addFieldToFilter('customer_id', $customerId)
			->addAttributeToFilter('created_at', array('from' => $first_second2, 'to' => $startDate2));
		$totelpending2 = array();
		$totelcomplete2 = array();

		foreach ($orders2 as $key => $orderlist2) {
			//echo $orderlist->getGrandTotal();
			if ($orderlist2->getStatus() != "complete") {
				$totelpending2[] = $orderlist2->getGrandTotal();
			} else {
				$totelcomplete2[] = $orderlist2->getGrandTotal();
			}
		}
		$totelpending2 = array_sum($totelpending2);
		$totelcomplete2 = array_sum($totelcomplete2);
		// $totelpending2 = '680';
		// $totelcomplete2 = '650';

		$totleplaced = $totelpending2 . "," . $totelpending1 . "," . $totelpending;
		$totelget = $totelcomplete2 . "," . $totelcomplete1 . "," . $totelcomplete;
		$month = '"' . $month3 . '","' . $month2 . '","' . $month . '"';

		return array($totleplaced, $totelget, $month);
	}

	public function getAttributeValue($attributeCode)
	{
		$customerId = $this->customerSession->getCustomer()->getId();
		return $this->customAttribute->getCustomer($customerId)->getData($attributeCode);
	}

	public function getCustomAttribute($attributeCode)
	{
		return $this->customAttribute->getUserDefinedAttribures($attributeCode);
	}
}
