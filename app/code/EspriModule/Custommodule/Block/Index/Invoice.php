<?php
/**
 * Copyright © 2015 EspriModule . All rights reserved.
 */
namespace EspriModule\Custommodule\Block\Index;

class Invoice extends \Magento\Framework\View\Element\Template
{		protected $orderCollectionFactory;
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
		)
	{	$this->orderCollectionFactory = $orderCollectionFactory;
		parent::__construct($context);
	}

	public function getOrders($id)
	{
		$customerId = $id;
		$customerOrder = $this->orderCollectionFactory->create()
			->addFieldToFilter('customer_id', $customerId)->setOrder(
				'created_at',
				'desc'
			);
		$customerOrder->getData();
		
		return $customerOrder;
	}
	
}
