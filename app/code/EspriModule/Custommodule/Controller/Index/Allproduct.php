<?php
/**
 *
 * Copyright © 2015 EspriModulecommerce. All rights reserved.
 */
namespace EspriModule\Custommodule\Controller\Index;

//class Invoice extends \Magento\Framework\App\Action\Action
class Allproduct extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        // echo "string";
        // exit;
        return $this->_pageFactory->create();
    }
}
