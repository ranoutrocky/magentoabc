<?php
/**
 *
 * Copyright © 2015 EspriModulecommerce. All rights reserved.
 */
namespace EspriModule\Custommodule\Controller\Index;
use Magento\Checkout\Model\Session;
//class Invoice extends \Magento\Framework\App\Action\Action
class Reorder extends \Magento\Framework\App\Action\Action
{

    
    protected $_pageFactory;
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        Session $session,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->request = $request;
        $this->resultRedirectFactory = $resultFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
        $this->messageManager = $messageManager;
        $this->_pageFactory = $pageFactory;
        $this->_session = $session;
        return parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParams('orderid');
        
         $orderId = $data['orderid'];
        
        $order = $this->orderRepository->get($orderId);
    $resultRedirect = $this->resultRedirectFactory->create();
    $cart = $this->_objectManager->get('Magento\Checkout\Model\Cart');
    
    
    foreach ($order->getAllVisibleItems() as $item) {
        try {
            $cart->addOrderItem($item);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_objectManager->get('Magento\Checkout\Model\Session')->getUseNotice(true)) {
                $this->messageManager->addNotice($e->getMessage());
            } else {
                $this->messageManager->addError($e->getMessage());
            }
            return $resultRedirect->setPath('*/*/history');
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
            return $resultRedirect->setPath('checkout/cart');
        }
    }

    $cart->save();
    $cart->getQuote()->setTotalsCollectedFlag(false)->collectTotals();
    $cart->getQuote()->save();
    return $resultRedirect->setPath('checkout/cart');
    exit;
    }
}
