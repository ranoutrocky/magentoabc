<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: mohammad
 * Date: 05/01/21
 * Time: 3:09 PM
 * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
 */
namespace EspriModule\InvoiceIntegration\Model;

use EspriModule\InvoiceIntegration\Helper\Data;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\File\Csv;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\CartItemInterface;
use Magento\Quote\Model\Quote\Address\RateFactory;
use Magento\Quote\Model\Quote\Item\ToOrderItem;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Service\InvoiceService;
use Psr\Log\LoggerInterface;

class Invoice
{
    /**
     * @var Csv
     */
    protected $csv;
    /**
     * @var OrderInterface
     */
    protected $order;
    /**
     * @var OrderItemInterface
     */
    protected $orderItem;
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;
    /**
     * @var CartItemRepositoryInterface
     */
    protected $orderItemRepository;
    /**
     * @var CartInterface
     */
    protected $cart;
    /**
     * @var CartItemInterface
     */
    protected $cartItem;
    /**
     * @var CartRepositoryInterface
     */
    protected $cartRepository;
    /**
     * @var
     */
    protected $cartItemRepository;
    /**
     * @var Data
     */
    protected $helper;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var ToOrderItem
     */
    protected $toOrderItem;
    /**
     * @var InvoiceService
     */
    protected $invoiceService;
    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;
    /**
     * @var RateFactory
     */
    protected $shippingAddressRate;

    const SHIPPING_METHOD_CODE = 'flatrate_flatrate';

    /**
     * Invoice constructor.
     * @param Csv $csv
     * @param OrderInterface $order
     * @param OrderItemInterface $orderItem
     * @param OrderRepositoryInterface $orderRepository
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param CartInterface $cart
     * @param CartItemInterface $cartItem
     * @param CartRepositoryInterface $cartRepository
     * @param CartItemRepositoryInterface $cartItemRepository
     * @param Data $helper
     * @param LoggerInterface $logger
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ToOrderItem $toOrderItem
     * @param InvoiceService $invoiceService
     * @param TransactionFactory $transactionFactory
     * @param ManagerInterface $messageManager
     * @param InvoiceSender $invoiceSender
     * @param RateFactory $shippingAddressRate
     */
    public function __construct(
        Csv $csv,
        OrderInterface $order,
        OrderItemInterface $orderItem,
        OrderRepositoryInterface $orderRepository,
        OrderItemRepositoryInterface $orderItemRepository,
        CartInterface $cart,
        CartItemInterface $cartItem,
        CartRepositoryInterface $cartRepository,
        CartItemRepositoryInterface  $cartItemRepository,
        Data $helper,
        LoggerInterface $logger,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ToOrderItem $toOrderItem,
        InvoiceService $invoiceService,
        TransactionFactory $transactionFactory,
        ManagerInterface $messageManager,
        InvoiceSender $invoiceSender,
        RateFactory $shippingAddressRate
    ) {
        $this->csv = $csv;
        $this->order = $order;
        $this->orderItem = $orderItem;
        $this->orderRepository = $orderRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->cart = $cart;
        $this->cartItem = $cartItem;
        $this->cartRepository = $cartRepository;
        $this->cartItemRepository = $cartItemRepository;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->toOrderItem = $toOrderItem;
        $this->invoiceService = $invoiceService;
        $this->invoiceSender = $invoiceSender;
        $this->messageManager = $messageManager;
        $this->transactionFactory = $transactionFactory;
        $this->shippingAddressRate = $shippingAddressRate;
    }

    /**
     * @param $fileName
     * @param $sourceDir
     * @return bool
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function processInvoice($fileName, $sourceDir)
    {
        if ($sourceDir) {
            if ($fileName) {
                try {
                    $data = $this->csv->getData($sourceDir . $fileName);
                } catch (\Exception $e) {
                    $this->logger->debug(__("Unable to read Invoice CSV file from source %1.", $sourceDir . $fileName));
                }
                array_shift($data);
                if (count($data)) {
                    foreach ($data as $lineData) {
                        //var_dump($lineData);
                        if (is_array($lineData) && count($lineData) == 20) {
                            return $this->processLine($lineData);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $lineData
     * @return bool
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function processLine($lineData): bool
    {
        $increment_id = str_replace('#', '', $lineData[8]);
        $order = $this->getOrderByIncrementId($increment_id);
        if ($order) {
            try {
                // Updating Quote data
                $quote = $this->cartRepository->get($order->getQuoteId());
                if ($quote->getItemsCount() > 0) {
                    // Updating Quote Items
                    foreach ($quote->getItemsCollection() as $quoteItem) {
                        $origOrderItem = $order->getItemByQuoteItemId($quoteItem->getId());
                        $orderItemId = $origOrderItem->getItemId();
                        $quoteItem->setDescription($lineData[12]);
                        $quoteItem->setQty($lineData[13]);
                        $quoteItem->setCustomerNumber($lineData[5]);
                        $quoteItem->setDeliveryDate($lineData[3]);
                        $quoteItem->setDueDate($lineData[4]);
                    }
                    // Updating Quote Payment PO number
                    $payment = $quote->getPayment();
                    $payment->setPoNumber($lineData[6]);
                    $quote->setPayment($payment);
                    // Updating Quote Shipment rate
                    $address = $quote->getShippingAddress();
                    $rates = $address->removeAllShippingRates();
                    $rate = $this->shippingAddressRate->create();
                    $rate->setCode(self::SHIPPING_METHOD_CODE)
                        ->setPrice(55.00);
                    $address->addShippingRate($rate);
                    $address->setShippingAmount(floatval($lineData[17]))
                        ->setBaseShippingAmount(floatval($lineData[17]));
                    $quote->addAddress($address);

                    $quote->collectTotals();
                    $this->cartRepository->save($quote);
                }
                foreach ($quote->getItemsCollection() as $quoteItem) {
                    $orderItem = $this->toOrderItem->convert($quoteItem);
                    $origOrderItemNew = $order->getItemByQuoteItemId($quoteItem->getId());

                    if ($origOrderItemNew) {
                        $customFields = [
                            'delivery_date' => $lineData[3],
                            'due_date' => $lineData[4],
                            'customer_number' => $lineData[5]
                        ];
                        $origOrderItemNew->addData(
                            array_merge($orderItem->getData(), $customFields)
                        );
                    } else {
                        if ($quoteItem->getParentItem()) {
                            $orderItem->setParentItem(
                                $order->getItemByQuoteItemId($orderItem->getParentItem()->getId())
                            );
                        }
                        $order->addItem($orderItem);
                    }
                    $payment = $order->getPayment();
                    $payment->setPoNumber($lineData[6]);
                    // Updating Quote Shipment rate
                    $payment->setBaseShippingAmount(floatval($lineData[17]));
                    $payment->setBaseShippingCaptured(floatval($lineData[17]));
                    $payment->setShippingAmount(floatval($lineData[17]));
                    $payment->setShippingCaptured(floatval($lineData[17]));
                    $order->setPayment($payment);
                    $order->setShippingAmount(floatval($lineData[17]))
                        ->setBaseShippingAmount(floatval($lineData[17]))
                        ->setSubtotal($quote->getSubtotal())
                        ->setBaseSubtotal($quote->getBaseSubtotal())
                        ->setGrandTotal($quote->getGrandTotal())
                        ->setBaseGrandTotal($quote->getBaseGrandTotal());
                    $this->cartRepository->save($quote);
                    $this->orderRepository->save($order);
                }
                $this->generateInvoice($order->getEntityId());
            } catch (NoSuchEntityException $e) {
                $this->logger->debug(__("No quote available having id %1", $order->getQuoteId()));
                return false;
            } catch (\Exception $e) {
                $this->logger->debug(__("Unable to update cart (id %1).", $order->getQuoteId()));
                return false;
            }
        }
        return true;
    }

    /**
     * @param $increment_id
     * @return OrderInterface
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    protected function getOrderByIncrementId($increment_id) : OrderInterface
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(
                'increment_id',
                $increment_id,
                'eq'
            )->create();
        $orders = $this->orderRepository->getList($searchCriteria);
        foreach ($orders->getItems() as $order) {
            return $order;
        }
    }

    /**
     * @param $quote
     * @param $order
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    protected function updateQuote($quote, $order)
    {
        var_dump($quote->getId());
    }

    /**
     * @param $orderId
     * @return \Magento\Sales\Api\Data\InvoiceInterface|\Magento\Sales\Model\Order\Invoice
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    protected function generateInvoice($orderId)
    {
        try {
            $order = $this->orderRepository->get($orderId);
            if (!$order->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
            }
            if (!$order->canInvoice()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('The order does not allow an invoice to be created.')
                );
            }

            $invoice = $this->invoiceService->prepareInvoice($order);
            if (!$invoice) {
                throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t save the invoice right now.'));
            }
            if (!$invoice->getTotalQty()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('You can\'t create an invoice without products.')
                );
            }
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
            $invoice->register();
            $invoice->getOrder()->setCustomerNoteNotify(false);
            $invoice->getOrder()->setIsInProcess(true);
            $order->addStatusHistoryComment('Automatically INVOICED', false);
            $transactionSave = $this->transactionFactory->create()->addObject($invoice)->addObject($invoice->getOrder());
            $transactionSave->save();

            // send invoice emails, If you want to stop mail disable below try/catch code
            try {
                $this->invoiceSender->send($invoice);
            } catch (\Exception $e) {
                $this->messageManager->addError(__('We can\'t send the invoice email right now.'));
            }
            return $invoice;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
