<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: mohammad
 * Date: 04/01/21
 * Time: 3:09 PM
 * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
 */
namespace EspriModule\InvoiceIntegration\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\DirectoryList;
use Magento\Framework\Json\Helper\Data as JsonData;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var DirectoryList
     */
    private $directoryList;
    /**
     * @var JsonData
     */
    private $jsonData;

    const NAV_INTEGRATION_ENABLED                       = 'integration/general/enable';
    const NAV_INVOICE_CSV_PATH                          = 'integration/invoice/source';
    const NAV_INVOICE_CSV_ARCHIVE_PATH                  = 'integration/invoice/archive';
    const NAV_INVOICE_CSV_HAVING_ERR_PATH               = 'integration/invoice/csv_error';
    const NAV_INVOICE_CSV_FILE_NAME                     = 'integration/invoice/file_name';
    const NAV_INVOICE_CSV_PATH_NEW                          = 'integration/invoice/invoicefile';
    const NAV_CUSTOMER_CSV_PATH                          = 'integration/invoice/customerfile';
    const NAV_RENTAL_CSV_PATH                          = 'integration/invoice/rentalfile';
    const NAV_RENTAL_INVOICE_CSV_PATH                          = 'integration/invoice/rentalinvoicefile';
    const NAV_SUBS_CSV_PATH                          = 'integration/invoice/subscriptionfile';



    /**
     * Data constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param DirectoryList $directoryList
     * @param JsonData $jsonData
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        DirectoryList $directoryList,
        JsonData $jsonData
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->directoryList = $directoryList;
        $this->jsonData = $jsonData;
        parent::__construct($context);
    }


    public function getInvoiceName()
    {
        if ($this->scopeConfig->getValue(self::NAV_INVOICE_CSV_PATH_NEW, ScopeInterface::SCOPE_STORE)) {
            return $this->scopeConfig->getValue(self::NAV_INVOICE_CSV_PATH_NEW, ScopeInterface::SCOPE_STORE);
        } else {
            throw new LocalizedException('NAV Integration module is not enabled.');
        }
    }
    public function getCustomerName()
    {
        if ($this->scopeConfig->getValue(self::NAV_CUSTOMER_CSV_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->scopeConfig->getValue(self::NAV_CUSTOMER_CSV_PATH, ScopeInterface::SCOPE_STORE);
        } else {
            throw new LocalizedException('NAV Integration module is not enabled.');
        }
    }
    public function getRenatalName()
    {
        if ($this->scopeConfig->getValue(self::NAV_RENTAL_CSV_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->scopeConfig->getValue(self::NAV_RENTAL_CSV_PATH, ScopeInterface::SCOPE_STORE);
        } else {
            throw new LocalizedException('NAV Integration module is not enabled.');
        }
    }
    public function getRenatalinvoiceName()
    {
        if ($this->scopeConfig->getValue(self::NAV_RENTAL_INVOICE_CSV_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->scopeConfig->getValue(self::NAV_RENTAL_INVOICE_CSV_PATH, ScopeInterface::SCOPE_STORE);
        } else {
            throw new LocalizedException('NAV Integration module is not enabled.');
        }
    }
    public function getSubscriptionName()
    {
        if ($this->scopeConfig->getValue(self::NAV_SUBS_CSV_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->scopeConfig->getValue(self::NAV_SUBS_CSV_PATH, ScopeInterface::SCOPE_STORE);
        } else {
            throw new LocalizedException('NAV Integration module is not enabled.');
        }
    }






    /**
     * @return bool
     * @throws LocalizedException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function isModuleEnabled(): bool
    {
        if ($this->scopeConfig->getValue(self::NAV_INTEGRATION_ENABLED, ScopeInterface::SCOPE_STORE)) {
            return true;
        } else {
            throw new LocalizedException('NAV Integration module is not enabled.');
        }
    }






    /**
     * @param null $dirName
     * @return string
     * @throws FileSystemException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getPath($dirName = null): string
    {
        if (!$dirName) {
            return $this->directoryList->getRoot();
        } else {
            return $this->directoryList->getPath($dirName);
        }
    }

    /**
     * @param $dir
     * @return string
     * @throws FileSystemException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    protected function _extractBaseDir($dir): string
    {
        $dir = str_replace('{date}', date('Ymd'), $dir);

        $options = [
            'var' => $this->getPath('var'),
            'tmp' => $this->getPath('tmp'),
            'media' => $this->getPath('media')
        ];

        foreach ($options as $option => $path) {
            $placeHolder    = sprintf('{%s}', $option);
            if (strpos($dir, $placeHolder) !== false) {
                $dir = str_replace($placeHolder, $path, $dir);
            }
        }
        $dir = str_replace('/', DIRECTORY_SEPARATOR, $dir);
        return $dir;
    }

    /**
     * Get export directory
     *
     * @param string $config
     * @param string $file
     * @param string $default
     * @return  string
     * @throws LocalizedException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getDirectory(string $config, string $file = '', $default = ''): string
    {
        $config = $this->_extractBaseDir($config);

        if (empty($config) && !empty($default)) {
            if ($file) {
                return $default . DIRECTORY_SEPARATOR . $file;
            } else {
                return $default . DIRECTORY_SEPARATOR;
            }
        }

        if (is_dir($config)) {
            if ($file) {
                return $config . DIRECTORY_SEPARATOR . $file;
            } else {
                return $config . DIRECTORY_SEPARATOR;
            }
        }

        // Make sure that config dir exists
        if (!is_dir($config)) {
            if (!mkdir($config, 0777, true)) {
                throw new LocalizedException(
                    sprintf(
                        'Could not create directory [%s]',
                        $config
                    )
                );
            }
        }
        if ($file) {
            return $config . DIRECTORY_SEPARATOR . $file;
        } else {
            return $config . DIRECTORY_SEPARATOR;
        }
    }

    /**
     * @return string|null
     * @throws FileSystemException
     * @throws LocalizedException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getInvoiceCsvDirPath(): ?string
    {
        if ($rawPath = $this->scopeConfig->getValue(self::NAV_INVOICE_CSV_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->getDirectory($rawPath);
        } else {
            throw new LocalizedException('NAV invoice CSV dir path isn\'t available.');
        }
    }

    /**
     * @return string|null
     * @throws LocalizedException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getInvoiceArchiveDirPath(): ?string
    {
        if ($rawPath = $this->scopeConfig->getValue(self::NAV_INVOICE_CSV_ARCHIVE_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->getDirectory($rawPath);
        } else {
            throw new LocalizedException('NAV invoice CSV dir path isn\'t available.');
        }
    }

    /**
     * @return string|null
     * @throws LocalizedException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getInvoiceErrorDirPath(): ?string
    {
        if ($rawPath = $this->scopeConfig->getValue(self::NAV_INVOICE_CSV_HAVING_ERR_PATH, ScopeInterface::SCOPE_STORE)) {
            return $this->getDirectory($rawPath);
        } else {
            throw new LocalizedException('NAV invoice CSV dir path isn\'t available.');
        }
    }

    /**
     * @return mixed|void
     * @throws LocalizedException
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getInvoiceFileName(): ?string
    {
        if ($fileName = $this->scopeConfig->getValue(self::NAV_INVOICE_CSV_FILE_NAME, ScopeInterface::SCOPE_STORE)) {
            return $fileName;
        } else {
            throw new LocalizedException('NAV invoice CSV file name isn\'t available.');
        }
    }

    /**
     * @param $dir
     * @return array|false
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function getAllFiles($dir): array
    {
        //scanning files in a given directory in unsorted order
        $files = scandir($dir, SCANDIR_SORT_NONE);
        return array_slice($files, 2);
    }
}
