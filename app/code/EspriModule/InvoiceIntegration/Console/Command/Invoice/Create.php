<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: mohammad
 * Date: 29/12/20
 * Time: 3:09 PM
 * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
 */
namespace EspriModule\InvoiceIntegration\Console\Command\Invoice;

use EspriModule\InvoiceIntegration\Helper\Data;
use EspriModule\InvoiceIntegration\Model\Invoice;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem\Driver\File;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Create extends Command
{
    /**
     * @var Data
     */
    private $helper;
    /**
     * @var Invoice
     */
    private $invoiceModel;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var File
     */
    protected $fileSystem;

    /**
     * Create constructor.
     * @param Data $helper
     * @param Invoice $invoiceModel
     * @param LoggerInterface $logger
     * @param string|null $name
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    public function __construct(
        Data $helper,
        Invoice $invoiceModel,
        LoggerInterface $logger,
        File $fileSystem,
        string $name = null
    ) {
        $this->helper = $helper;
        $this->invoiceModel = $invoiceModel;
        $this->logger = $logger;
        $this->fileSystem = $fileSystem;
        parent::__construct($name);
    }

    /**
     * perent method extended
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    protected function configure()
    {
        $this->setDescription('This is my first console command.');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @author  Mohammad Shadab Alam <shadab.alam@williamscommerce.com>
     */
    protected function execute(InputInterface $input, OutputInterface $output) : void
    {
        $output->writeln("<info>***Integration Processing of Invoice coming from NAV Started***</info>");
        try {
            $sourceDir = $this->helper->getInvoiceCsvDirPath();
            $archiveDir = $this->helper->getInvoiceArchiveDirPath();
            $errorDir = $this->helper->getInvoiceErrorDirPath();
            $fileName = $this->helper->getInvoiceFileName();
            $output->writeln("<comment>" . $sourceDir . "</comment>");
            $output->writeln("<comment>" . $fileName . "</comment>");
            $files = $this->helper->getAllFiles($sourceDir);
            if (count($files) > 0) {
                foreach ($files as $file) {
                    if ($this->invoiceModel->processInvoice($file, $sourceDir)) {
                        $source = $sourceDir . $file;
                        $destination = $archiveDir . $file;
                        $this->fileSystem->copy($source, $destination);
                        $this->fileSystem->deleteFile($source);
                    } else {
                        $source = $sourceDir . $file;
                        $destination = $errorDir . $file;
                        $this->fileSystem->copy($source, $destination);
                        $this->fileSystem->deleteFile($source);
                    }
                }
            }
        } catch (LocalizedException $e) {
            $this->logger->debug($e->getMessage());
        }
    }
}
