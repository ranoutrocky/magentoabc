<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Model;

use EspriModule\Customernotification\Api\Data\OrderstateInterface;
use EspriModule\Customernotification\Api\Data\OrderstateInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Orderstate extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'esprimodule_customernotification_orderstate';
    protected $dataObjectHelper;

    protected $orderstateDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param OrderstateInterfaceFactory $orderstateDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \EspriModule\Customernotification\Model\ResourceModel\Orderstate $resource
     * @param \EspriModule\Customernotification\Model\ResourceModel\Orderstate\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        OrderstateInterfaceFactory $orderstateDataFactory,
        DataObjectHelper $dataObjectHelper,
        \EspriModule\Customernotification\Model\ResourceModel\Orderstate $resource,
        \EspriModule\Customernotification\Model\ResourceModel\Orderstate\Collection $resourceCollection,
        array $data = []
    ) {
        $this->orderstateDataFactory = $orderstateDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve orderstate model with orderstate data
     * @return OrderstateInterface
     */
    public function getDataModel()
    {
        $orderstateData = $this->getData();
        
        $orderstateDataObject = $this->orderstateDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $orderstateDataObject,
            $orderstateData,
            OrderstateInterface::class
        );
        
        return $orderstateDataObject;
    }
}

