<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Model\Data;

use EspriModule\Customernotification\Api\Data\OrderstateInterface;

class Orderstate extends \Magento\Framework\Api\AbstractExtensibleObject implements OrderstateInterface
{

    /**
     * Get orderstate_id
     * @return string|null
     */
    public function getOrderstateId()
    {
        return $this->_get(self::ORDERSTATE_ID);
    }

    /**
     * Set orderstate_id
     * @param string $orderstateId
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setOrderstateId($orderstateId)
    {
        return $this->setData(self::ORDERSTATE_ID, $orderstateId);
    }

    /**
     * Get customerid
     * @return string|null
     */
    public function getCustomerid()
    {
        return $this->_get(self::CUSTOMERID);
    }

    /**
     * Set customerid
     * @param string $customerid
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setCustomerid($customerid)
    {
        return $this->setData(self::CUSTOMERID, $customerid);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \EspriModule\Customernotification\Api\Data\OrderstateExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \EspriModule\Customernotification\Api\Data\OrderstateExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \EspriModule\Customernotification\Api\Data\OrderstateExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get orderid
     * @return string|null
     */
    public function getOrderid()
    {
        return $this->_get(self::ORDERID);
    }

    /**
     * Set orderid
     * @param string $orderid
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setOrderid($orderid)
    {
        return $this->setData(self::ORDERID, $orderid);
    }

    /**
     * Get status
     * @return string|null
     */
    public function getStatus()
    {
        return $this->_get(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get seenstatus
     * @return string|null
     */
    public function getSeenstatus()
    {
        return $this->_get(self::SEENSTATUS);
    }

    /**
     * Set seenstatus
     * @param string $seenstatus
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setSeenstatus($seenstatus)
    {
        return $this->setData(self::SEENSTATUS, $seenstatus);
    }

    /**
     * Get lastupdate
     * @return string|null
     */
    public function getLastupdate()
    {
        return $this->_get(self::LASTUPDATE);
    }

    /**
     * Set lastupdate
     * @param string $lastupdate
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setLastupdate($lastupdate)
    {
        return $this->setData(self::LASTUPDATE, $lastupdate);
    }
}

