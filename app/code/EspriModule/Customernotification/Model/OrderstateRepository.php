<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Model;

use EspriModule\Customernotification\Api\Data\OrderstateInterfaceFactory;
use EspriModule\Customernotification\Api\Data\OrderstateSearchResultsInterfaceFactory;
use EspriModule\Customernotification\Api\OrderstateRepositoryInterface;
use EspriModule\Customernotification\Model\ResourceModel\Orderstate as ResourceOrderstate;
use EspriModule\Customernotification\Model\ResourceModel\Orderstate\CollectionFactory as OrderstateCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class OrderstateRepository implements OrderstateRepositoryInterface
{

    protected $orderstateFactory;

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $dataOrderstateFactory;

    private $storeManager;

    protected $orderstateCollectionFactory;


    /**
     * @param ResourceOrderstate $resource
     * @param OrderstateFactory $orderstateFactory
     * @param OrderstateInterfaceFactory $dataOrderstateFactory
     * @param OrderstateCollectionFactory $orderstateCollectionFactory
     * @param OrderstateSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceOrderstate $resource,
        OrderstateFactory $orderstateFactory,
        OrderstateInterfaceFactory $dataOrderstateFactory,
        OrderstateCollectionFactory $orderstateCollectionFactory,
        OrderstateSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->orderstateFactory = $orderstateFactory;
        $this->orderstateCollectionFactory = $orderstateCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataOrderstateFactory = $dataOrderstateFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \EspriModule\Customernotification\Api\Data\OrderstateInterface $orderstate
    ) {
        /* if (empty($orderstate->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $orderstate->setStoreId($storeId);
        } */
        
        $orderstateData = $this->extensibleDataObjectConverter->toNestedArray(
            $orderstate,
            [],
            \EspriModule\Customernotification\Api\Data\OrderstateInterface::class
        );
        
        $orderstateModel = $this->orderstateFactory->create()->setData($orderstateData);
        
        try {
            $this->resource->save($orderstateModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the orderstate: %1',
                $exception->getMessage()
            ));
        }
        return $orderstateModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($orderstateId)
    {
        $orderstate = $this->orderstateFactory->create();
        $this->resource->load($orderstate, $orderstateId);
        if (!$orderstate->getId()) {
            throw new NoSuchEntityException(__('Orderstate with id "%1" does not exist.', $orderstateId));
        }
        return $orderstate->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->orderstateCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \EspriModule\Customernotification\Api\Data\OrderstateInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \EspriModule\Customernotification\Api\Data\OrderstateInterface $orderstate
    ) {
        try {
            $orderstateModel = $this->orderstateFactory->create();
            $this->resource->load($orderstateModel, $orderstate->getOrderstateId());
            $this->resource->delete($orderstateModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Orderstate: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($orderstateId)
    {
        return $this->delete($this->get($orderstateId));
    }
}

