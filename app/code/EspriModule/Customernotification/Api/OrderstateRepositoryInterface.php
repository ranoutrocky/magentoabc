<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface OrderstateRepositoryInterface
{

    /**
     * Save Orderstate
     * @param \EspriModule\Customernotification\Api\Data\OrderstateInterface $orderstate
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \EspriModule\Customernotification\Api\Data\OrderstateInterface $orderstate
    );

    /**
     * Retrieve Orderstate
     * @param string $orderstateId
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($orderstateId);

    /**
     * Retrieve Orderstate matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \EspriModule\Customernotification\Api\Data\OrderstateSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Orderstate
     * @param \EspriModule\Customernotification\Api\Data\OrderstateInterface $orderstate
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \EspriModule\Customernotification\Api\Data\OrderstateInterface $orderstate
    );

    /**
     * Delete Orderstate by ID
     * @param string $orderstateId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($orderstateId);
}

