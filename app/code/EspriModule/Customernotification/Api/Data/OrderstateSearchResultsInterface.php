<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Api\Data;

interface OrderstateSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Orderstate list.
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface[]
     */
    public function getItems();

    /**
     * Set customerid list.
     * @param \EspriModule\Customernotification\Api\Data\OrderstateInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

