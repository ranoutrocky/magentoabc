<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Api\Data;

interface OrderstateInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const STATUS = 'status';
    const ORDERID = 'orderid';
    const CUSTOMERID = 'customerid';
    const SEENSTATUS = 'seenstatus';
    const ORDERSTATE_ID = 'orderstate_id';
    const LASTUPDATE = 'lastupdate';

    /**
     * Get orderstate_id
     * @return string|null
     */
    public function getOrderstateId();

    /**
     * Set orderstate_id
     * @param string $orderstateId
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setOrderstateId($orderstateId);

    /**
     * Get customerid
     * @return string|null
     */
    public function getCustomerid();

    /**
     * Set customerid
     * @param string $customerid
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setCustomerid($customerid);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \EspriModule\Customernotification\Api\Data\OrderstateExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \EspriModule\Customernotification\Api\Data\OrderstateExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \EspriModule\Customernotification\Api\Data\OrderstateExtensionInterface $extensionAttributes
    );

    /**
     * Get orderid
     * @return string|null
     */
    public function getOrderid();

    /**
     * Set orderid
     * @param string $orderid
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setOrderid($orderid);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setStatus($status);

    /**
     * Get seenstatus
     * @return string|null
     */
    public function getSeenstatus();

    /**
     * Set seenstatus
     * @param string $seenstatus
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setSeenstatus($seenstatus);

    /**
     * Get lastupdate
     * @return string|null
     */
    public function getLastupdate();

    /**
     * Set lastupdate
     * @param string $lastupdate
     * @return \EspriModule\Customernotification\Api\Data\OrderstateInterface
     */
    public function setLastupdate($lastupdate);
}

