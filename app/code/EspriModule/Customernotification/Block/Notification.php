<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customernotification\Block;

class Notification extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\View\Element\Template\Context $context,
        \EspriModule\Customernotification\Model\ResourceModel\Orderstate\Collection $orderstate,
        array $data = []
    ) {
        $this->customerSession = $customerSession;
        $this->notifications = $orderstate;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function newnotification($customerId)
    {
        //$customerId = $this->getCurentcustomer();
        $post = $this->notifications
			->addFieldToFilter('customerid', $customerId)
            ->addFieldToFilter('seenstatus', '0')->setOrder(
                'orderstate_id',
                'desc'
            )->setPageSize('10');
		return $post->getData();
    }

    /**
     * @return string
     */
    public function notification($customerId)
    {
        //$customerId = $this->getCurentcustomer();
        $post = $this->notifications
			->addFieldToFilter('customerid', $customerId);
		return $post->getData();
    }


    /**
     * @return string
     */

    public function getCurentcustomer()
	{
		$this->customerSession->getCustomer()->getId();
	}
   
}

