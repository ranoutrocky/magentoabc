<?php

/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace EspriModule\Customernotification\Observer\Sales;

class OrderSaveAfter implements \Magento\Framework\Event\ObserverInterface
{

    protected $orderRepository;
    public function __construct(
        \Magento\Framework\Registry $registry,
        \EspriModule\Customernotification\Model\Orderstate $notification,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
    ) {
        $this->registry = $registry;
        $this->ordernotification = $notification;
        $this->orderRepository = $orderRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $registeryObserverCheck = $this->registry->registry('registeryObserverCheck');
    if(!$registeryObserverCheck) {
        $order = $observer->getEvent()->getOrder();
        $customerId = $order->getCustomerId();
        $orderincriment = $order->getIncrementId();
        $OrderStatus = $order->getStatus();
        $seenstatus = '0';
        

       $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
       $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
       $connection = $resource->getConnection();
       $tableName = $resource->getTableName('esprimodule_customernotification_orderstate'); //gives table name with prefix

       //Select Data from table
       $sql1 = "Insert Into " . $tableName . " (customerid, orderid, status, seenstatus) Values ($customerId,'$orderincriment','$OrderStatus',$seenstatus)";
       $connection->query($sql1);

       $this->registry->register('registeryObserverCheck', true);
        return $this;
    }
        
    }
}
