<?php
/**
 * Copyright © Rentalbalace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Rentalbalace\Api\Data;

interface RentalbalaceInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const QTY = 'qty';
    const NAVCUSTOMERID = 'navcustomerid';
    const ASOFDATE = 'asofdate';
    const ITEMNUMBER = 'itemnumber';
    const CUSTOMERID = 'customerid';
    const RENTALBALACE_ID = 'rentalbalace_id';

    /**
     * Get rentalbalace_id
     * @return string|null
     */
    public function getRentalbalaceId();

    /**
     * Set rentalbalace_id
     * @param string $rentalbalaceId
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setRentalbalaceId($rentalbalaceId);

    /**
     * Get customerid
     * @return string|null
     */
    public function getCustomerid();

    /**
     * Set customerid
     * @param string $customerid
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setCustomerid($customerid);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \EspriModule\Rentalbalace\Api\Data\RentalbalaceExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \EspriModule\Rentalbalace\Api\Data\RentalbalaceExtensionInterface $extensionAttributes
    );

    /**
     * Get navcustomerid
     * @return string|null
     */
    public function getNavcustomerid();

    /**
     * Set navcustomerid
     * @param string $navcustomerid
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setNavcustomerid($navcustomerid);

    /**
     * Get itemnumber
     * @return string|null
     */
    public function getItemnumber();

    /**
     * Set itemnumber
     * @param string $itemnumber
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setItemnumber($itemnumber);

    /**
     * Get qty
     * @return string|null
     */
    public function getQty();

    /**
     * Set qty
     * @param string $qty
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setQty($qty);

    /**
     * Get asofdate
     * @return string|null
     */
    public function getAsofdate();

    /**
     * Set asofdate
     * @param string $asofdate
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setAsofdate($asofdate);
}

