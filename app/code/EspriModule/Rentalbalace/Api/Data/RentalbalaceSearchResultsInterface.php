<?php
/**
 * Copyright © Rentalbalace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Rentalbalace\Api\Data;

interface RentalbalaceSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Rentalbalace list.
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface[]
     */
    public function getItems();

    /**
     * Set customerid list.
     * @param \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

