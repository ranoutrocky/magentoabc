<?php
/**
 * Copyright © Rentalbalace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Rentalbalace\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RentalbalaceRepositoryInterface
{

    /**
     * Save Rentalbalace
     * @param \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface $rentalbalace
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface $rentalbalace
    );

    /**
     * Retrieve Rentalbalace
     * @param string $rentalbalaceId
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($rentalbalaceId);

    /**
     * Retrieve Rentalbalace matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Rentalbalace
     * @param \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface $rentalbalace
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface $rentalbalace
    );

    /**
     * Delete Rentalbalace by ID
     * @param string $rentalbalaceId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($rentalbalaceId);
}

