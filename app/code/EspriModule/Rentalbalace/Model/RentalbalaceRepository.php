<?php
/**
 * Copyright © Rentalbalace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Rentalbalace\Model;

use EspriModule\Rentalbalace\Api\Data\RentalbalaceInterfaceFactory;
use EspriModule\Rentalbalace\Api\Data\RentalbalaceSearchResultsInterfaceFactory;
use EspriModule\Rentalbalace\Api\RentalbalaceRepositoryInterface;
use EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace as ResourceRentalbalace;
use EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace\CollectionFactory as RentalbalaceCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class RentalbalaceRepository implements RentalbalaceRepositoryInterface
{

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $rentalbalaceFactory;

    protected $extensionAttributesJoinProcessor;

    protected $dataRentalbalaceFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $rentalbalaceCollectionFactory;

    protected $resource;

    private $storeManager;


    /**
     * @param ResourceRentalbalace $resource
     * @param RentalbalaceFactory $rentalbalaceFactory
     * @param RentalbalaceInterfaceFactory $dataRentalbalaceFactory
     * @param RentalbalaceCollectionFactory $rentalbalaceCollectionFactory
     * @param RentalbalaceSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceRentalbalace $resource,
        RentalbalaceFactory $rentalbalaceFactory,
        RentalbalaceInterfaceFactory $dataRentalbalaceFactory,
        RentalbalaceCollectionFactory $rentalbalaceCollectionFactory,
        RentalbalaceSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->rentalbalaceFactory = $rentalbalaceFactory;
        $this->rentalbalaceCollectionFactory = $rentalbalaceCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRentalbalaceFactory = $dataRentalbalaceFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface $rentalbalace
    ) {
        /* if (empty($rentalbalace->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $rentalbalace->setStoreId($storeId);
        } */
        
        $rentalbalaceData = $this->extensibleDataObjectConverter->toNestedArray(
            $rentalbalace,
            [],
            \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface::class
        );
        
        $rentalbalaceModel = $this->rentalbalaceFactory->create()->setData($rentalbalaceData);
        
        try {
            $this->resource->save($rentalbalaceModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the rentalbalace: %1',
                $exception->getMessage()
            ));
        }
        return $rentalbalaceModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($rentalbalaceId)
    {
        $rentalbalace = $this->rentalbalaceFactory->create();
        $this->resource->load($rentalbalace, $rentalbalaceId);
        if (!$rentalbalace->getId()) {
            throw new NoSuchEntityException(__('Rentalbalace with id "%1" does not exist.', $rentalbalaceId));
        }
        return $rentalbalace->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->rentalbalaceCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface $rentalbalace
    ) {
        try {
            $rentalbalaceModel = $this->rentalbalaceFactory->create();
            $this->resource->load($rentalbalaceModel, $rentalbalace->getRentalbalaceId());
            $this->resource->delete($rentalbalaceModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Rentalbalace: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($rentalbalaceId)
    {
        return $this->delete($this->get($rentalbalaceId));
    }
}

