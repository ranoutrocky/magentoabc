<?php
/**
 * Copyright © Rentalbalace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Rentalbalace\Model;

use EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface;
use EspriModule\Rentalbalace\Api\Data\RentalbalaceInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Rentalbalace extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $rentalbalaceDataFactory;

    protected $_eventPrefix = 'esprimodule_rentalbalace_rentalbalace';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param RentalbalaceInterfaceFactory $rentalbalaceDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace $resource
     * @param \EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        RentalbalaceInterfaceFactory $rentalbalaceDataFactory,
        DataObjectHelper $dataObjectHelper,
        \EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace $resource,
        \EspriModule\Rentalbalace\Model\ResourceModel\Rentalbalace\Collection $resourceCollection,
        array $data = []
    ) {
        $this->rentalbalaceDataFactory = $rentalbalaceDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve rentalbalace model with rentalbalace data
     * @return RentalbalaceInterface
     */
    public function getDataModel()
    {
        $rentalbalaceData = $this->getData();
        
        $rentalbalaceDataObject = $this->rentalbalaceDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $rentalbalaceDataObject,
            $rentalbalaceData,
            RentalbalaceInterface::class
        );
        
        return $rentalbalaceDataObject;
    }
}

