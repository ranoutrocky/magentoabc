<?php
/**
 * Copyright © Rentalbalace All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Rentalbalace\Model\Data;

use EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface;

class Rentalbalace extends \Magento\Framework\Api\AbstractExtensibleObject implements RentalbalaceInterface
{

    /**
     * Get rentalbalace_id
     * @return string|null
     */
    public function getRentalbalaceId()
    {
        return $this->_get(self::RENTALBALACE_ID);
    }

    /**
     * Set rentalbalace_id
     * @param string $rentalbalaceId
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setRentalbalaceId($rentalbalaceId)
    {
        return $this->setData(self::RENTALBALACE_ID, $rentalbalaceId);
    }

    /**
     * Get customerid
     * @return string|null
     */
    public function getCustomerid()
    {
        return $this->_get(self::CUSTOMERID);
    }

    /**
     * Set customerid
     * @param string $customerid
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setCustomerid($customerid)
    {
        return $this->setData(self::CUSTOMERID, $customerid);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \EspriModule\Rentalbalace\Api\Data\RentalbalaceExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \EspriModule\Rentalbalace\Api\Data\RentalbalaceExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get navcustomerid
     * @return string|null
     */
    public function getNavcustomerid()
    {
        return $this->_get(self::NAVCUSTOMERID);
    }

    /**
     * Set navcustomerid
     * @param string $navcustomerid
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setNavcustomerid($navcustomerid)
    {
        return $this->setData(self::NAVCUSTOMERID, $navcustomerid);
    }

    /**
     * Get itemnumber
     * @return string|null
     */
    public function getItemnumber()
    {
        return $this->_get(self::ITEMNUMBER);
    }

    /**
     * Set itemnumber
     * @param string $itemnumber
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setItemnumber($itemnumber)
    {
        return $this->setData(self::ITEMNUMBER, $itemnumber);
    }

    /**
     * Get qty
     * @return string|null
     */
    public function getQty()
    {
        return $this->_get(self::QTY);
    }

    /**
     * Set qty
     * @param string $qty
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setQty($qty)
    {
        return $this->setData(self::QTY, $qty);
    }

    /**
     * Get asofdate
     * @return string|null
     */
    public function getAsofdate()
    {
        return $this->_get(self::ASOFDATE);
    }

    /**
     * Set asofdate
     * @param string $asofdate
     * @return \EspriModule\Rentalbalace\Api\Data\RentalbalaceInterface
     */
    public function setAsofdate($asofdate)
    {
        return $this->setData(self::ASOFDATE, $asofdate);
    }
}

