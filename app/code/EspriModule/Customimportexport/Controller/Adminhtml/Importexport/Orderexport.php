<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Controller\Adminhtml\Importexport;

class Orderexport extends \Magento\Backend\App\Action
{

   protected $fileFactory;
    protected $csvProcessor;
    protected $directoryList;
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    ) {
        $this->_customerFactory = $customerFactory;
        $this->fileFactory = $fileFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->resultPageFactory = $resultPageFactory;
        $this->_customerSession= $customerSession;
        parent::__construct($context ,$customerSession);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        
        $fileName = 'Magento_to_NAV_Orders_'.date('mdYh').'.csv';
        $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR)
            . "/" . $fileName;

        
        $personalData = $this->getPresonalData();
        if (count($personalData) < 2) {
            echo "<pre>";
            print_r($personalData);
            echo "You Do not have any new record";
            exit;
        }else{
            $this->csvProcessor
            ->setDelimiter(',')
            ->setEnclosure('"')
            ->saveData(
                $filePath,
                $personalData
            );

        return $this->fileFactory->create(
            $fileName,
            [
                'type' => "filename",
                'value' => $fileName,
                'rm' => true,
            ],
            \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
            'application/octet-stream'
        );
        }

       

    }

    protected function getPresonalData()
    {
        // $status = 'processing';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
 
$orderCollectionFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory')->create();
 $orderCollection = $orderCollectionFactory
                ->addAttributeToSelect('*');
        // ->addAttributeToFilter('status', ['eq'=> $status]);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
       $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
       $connection = $resource->getConnection();
       $tableName = $resource->getTableName('esprimodule_customimportexport_order'); //gives table name with prefix

       //Select Data from table
       $sql = "Select * FROM " . $tableName." ORDER BY `esprimodule_customimportexport_order`.`order_export_id` DESC LIMIT 1";
       $result1 = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
    //    echo "<pre>";
    //    print_r($result1);
       //exit;
// Get order collection
        $result = [];
         $result[] = [
            'Order Date',
            'PO Number',
           'Customer No',
           'Magento CustID',
           'Magento Order Number',
           'Magento Order Item Number',
           'LineNumber',
           'Number',
           'Description',
           'Quantity',
           'Rush Delivery',
       ];
       if ($orderCollection && count($orderCollection) > 0) {
        $firstid ="";
        $lastorderid ="";
    foreach ($orderCollection AS $orders) {
        if (count($result1) > 0) {
            // echo count($result);
             if ($result1['0']['to_order_id'] > $orders->getId() ) {
                 continue;
             }
             if ($result1['0']['to_order_id'] == $orders->getId() ) {
                 
                 continue;
             }
            // echo "==".count($result);
         }

        if ($firstid) {
                
        }else{
            $firstid = $orders->getId();
        }
        $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($orders->getId());
        
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
        //Loop through each item and fetch data
        $itemid=[];
        $itemname=[];
        $itemqty=[];
        $i=1;
        if($order->getShippingMethod()=="flatrate_flatrate"){
            $rush = "Yes";
        }else{
            $rush = "No";
        }
foreach ($order->getAllItems() as $item)
{
//     $itemid[]= $item->getId();
//    $itemname[]= $item->getSku();
//    $itemqty[]= $item->getQtyOrdered();
   $date = date_create($order->getCreatedAt());
        $result[] = [
            date_format($date,"m/d/Y"),
            '',
            $customerObj->getData('ms_id'),
            $customerObj->getId(),
            $order->getIncrementId(),
            $item->getId(),
            $i,
            $item->getSku(),
            $item->getSku(),
            $item->getQtyOrdered(),
            $rush,
            
        ];
   
     
$i++;}
// echo "<pre>";
// print_r($itemid);

        $lastorderid = $orders->getId();
    }
  
      //Insert Data into table
      if($firstid!=""){
        $sql1 = "Insert Into " . $tableName . " (from_order_id, to_order_id) Values ($firstid,$lastorderid)";
      $connection->query($sql1);
      }
      
       
}

        return $result;
    }
}

