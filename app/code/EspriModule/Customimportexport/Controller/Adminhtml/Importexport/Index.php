<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);


namespace EspriModule\Customimportexport\Controller\Adminhtml\Importexport;


use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Index extends \Magento\Backend\App\Action
{
    protected $fileFactory;
    protected $csvProcessor;
    protected $directoryList;
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Flagbit\FlysystemS3\Helper\Config $s3helper
    ) {
        
        $this->_s3helper = $s3helper;
        $this->_customerFactory = $customerFactory;
        $this->fileFactory = $fileFactory;
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
        $this->resultPageFactory = $resultPageFactory;
        $this->_customerSession= $customerSession;
        parent::__construct($context ,$customerSession);
        $this->client = new \Aws\S3\S3Client([
            'region'  => 'us-east-1',
            'version' => 'latest',
            'credentials' => [
                'key'    => "AKIAZP34VWTZPDZOZVWM",
                'secret' => "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",
            ]
        ]);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
       
echo "--key".$s3key = $this->_s3helper->getS3Key();

echo "--secret--".$s3secretkey = $this->_s3helper->getS3Secret();
echo "--regin--".$s3region = $this->_s3helper->getS3Region();
echo "--bucket--".$s3bucket = $this->_s3helper->getS3Bucket();






$bucket = $s3bucket;
/*
$s3 = new S3Client([
    'version' => 'latest',
    'region'  => 'us-east-1'
]);
*/



$this->client->registerStreamWrapper();

// $keyname1='temp.txt';
// $url1 = 's3://' . $bucket . '/' . $keyname1;
 
// echo file_get_contents($url1);
        
        $fileName = 'Magento_to_NAV_Customers_'.date('mdYhNN').'.csv';
        // $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR)
        //     . "/" . $fileName;
        $url1 = 's3://' . $bucket . '/' . $fileName;

        
        $personalData = $this->getPresonalData();
        if (empty($personalData)) {
            echo "<pre>";
            print_r($personalData);
            echo "You Do not have any new record";
            exit;
        }else{
            $this->csvProcessor
            ->setDelimiter(',')
            ->setEnclosure('"')
            ->saveData(
                $url1,
                $personalData
            );
            file_put_contents($url1,'$personalData');
            // $this->client->putObject(array(
            //     'Bucket' => $bucket,
            //     'Key'    => $fileName,
            //     'Body'   => $personalData
            // ));
            
        // return $this->fileFactory->create(
        //     $fileName,
        //     [
        //         'type' => "filename",
        //         'value' => $fileName,
        //         'rm' => true,
        //     ],
        //     \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
        //     'application/octet-stream'
        // );
        }
        
       

    }

    protected function getPresonalData()
    {
        $result = [];
        $collection = $this->_customerFactory->create()->getCollection()->addAttributeToSelect("*")->load();
        $result[] = [
            'Magento Cust ID',
            'Business Name',
            'firstname',
            'lastname',
           'email',
           'status'
       ];
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
       $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
       $connection = $resource->getConnection();
       $tableName = $resource->getTableName('esprimodule_customimportexport_customer'); //gives table name with prefix

       //Select Data from table
       $sql = "Select * FROM " . $tableName." ORDER BY `esprimodule_customimportexport_customer`.`customer_export_id` DESC LIMIT 1 ";
       $result = $connection->fetchAll($sql); // gives associated array, table fields as key in array.
    //    echo "<pre>";
    //    print_r($result);

    $result1 =[];
       $firstid ="";
       $lastcustomer ="";
       if ($collection && count($collection) > 1) {
    foreach ($collection AS $customer) {
       
        // if (count($result) > 0) {
        //    // echo count($result);
        //     if ($result['0']['to_customer_id'] > $customer->getId() ) {
        //         continue;
        //     }
        //     if ($result['0']['to_customer_id'] == $customer->getId() ) {
                
        //         continue;
        //     }
        //    // echo "==".count($result);
        // }
        
        
        if($customer->getData('customer_status')=='6'){
            $status ="";
            if($customer->getData('customer_status')=='6'){
                $status='New';

            }
            if($customer->getData('customer_status')=='12'){
                $status='Blocked_for_invoice';
                
            }
            if($customer->getData('customer_status')=='13'){
                $status='Blocked_for_purchage';
                
            }
            if ($firstid) {
                
            }else{
                $firstid = $customer->getId();
            }
        $result1[] = [
            $customer->getId(),
            $customer->getBusinessName(),
            $customer->getFirstname(),
            $customer->getLastname(),
           $customer->getEmail(),
           $status,
        ];
        $lastcustomer = $customer->getId();
    }
        
    }
}
//echo $firstid."=".$lastcustomer;
           //Insert Data into table
           if ($firstid!="") {
            $sql1 = "Insert Into " . $tableName . " (from_customer_id, to_customer_id) Values ($firstid,$lastcustomer)";
            $connection->query($sql1);
           }
       
        return $result1;
    }
}

