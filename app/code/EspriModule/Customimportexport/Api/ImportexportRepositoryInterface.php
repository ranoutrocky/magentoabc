<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ImportexportRepositoryInterface
{

    /**
     * Save importexport
     * @param \EspriModule\Customimportexport\Api\Data\ImportexportInterface $importexport
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \EspriModule\Customimportexport\Api\Data\ImportexportInterface $importexport
    );

    /**
     * Retrieve importexport
     * @param string $importexportId
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($importexportId);

    /**
     * Retrieve importexport matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete importexport
     * @param \EspriModule\Customimportexport\Api\Data\ImportexportInterface $importexport
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \EspriModule\Customimportexport\Api\Data\ImportexportInterface $importexport
    );

    /**
     * Delete importexport by ID
     * @param string $importexportId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($importexportId);
}

