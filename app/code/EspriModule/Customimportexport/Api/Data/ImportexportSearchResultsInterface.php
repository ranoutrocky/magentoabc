<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Api\Data;

interface ImportexportSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get importexport list.
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface[]
     */
    public function getItems();

    /**
     * Set event list.
     * @param \EspriModule\Customimportexport\Api\Data\ImportexportInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

