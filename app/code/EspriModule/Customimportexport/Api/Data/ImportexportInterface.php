<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Api\Data;

interface ImportexportInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const IMPORTEXPORT_ID = 'importexport_id';
    const DATE = 'date';
    const EVENT = 'event';

    /**
     * Get importexport_id
     * @return string|null
     */
    public function getImportexportId();

    /**
     * Set importexport_id
     * @param string $importexportId
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     */
    public function setImportexportId($importexportId);

    /**
     * Get event
     * @return string|null
     */
    public function getEvent();

    /**
     * Set event
     * @param string $event
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     */
    public function setEvent($event);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \EspriModule\Customimportexport\Api\Data\ImportexportExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \EspriModule\Customimportexport\Api\Data\ImportexportExtensionInterface $extensionAttributes
    );

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     */
    public function setDate($date);
}

