<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Model;

use EspriModule\Customimportexport\Api\Data\ImportexportInterface;
use EspriModule\Customimportexport\Api\Data\ImportexportInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Importexport extends \Magento\Framework\Model\AbstractModel
{

    protected $importexportDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'esprimodule_customimportexport_importexport';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ImportexportInterfaceFactory $importexportDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \EspriModule\Customimportexport\Model\ResourceModel\Importexport $resource
     * @param \EspriModule\Customimportexport\Model\ResourceModel\Importexport\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ImportexportInterfaceFactory $importexportDataFactory,
        DataObjectHelper $dataObjectHelper,
        \EspriModule\Customimportexport\Model\ResourceModel\Importexport $resource,
        \EspriModule\Customimportexport\Model\ResourceModel\Importexport\Collection $resourceCollection,
        array $data = []
    ) {
        $this->importexportDataFactory = $importexportDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve importexport model with importexport data
     * @return ImportexportInterface
     */
    public function getDataModel()
    {
        $importexportData = $this->getData();
        
        $importexportDataObject = $this->importexportDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $importexportDataObject,
            $importexportData,
            ImportexportInterface::class
        );
        
        return $importexportDataObject;
    }
}

