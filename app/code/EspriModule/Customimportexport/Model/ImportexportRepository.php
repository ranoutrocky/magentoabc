<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Model;

use EspriModule\Customimportexport\Api\Data\ImportexportInterfaceFactory;
use EspriModule\Customimportexport\Api\Data\ImportexportSearchResultsInterfaceFactory;
use EspriModule\Customimportexport\Api\ImportexportRepositoryInterface;
use EspriModule\Customimportexport\Model\ResourceModel\Importexport as ResourceImportexport;
use EspriModule\Customimportexport\Model\ResourceModel\Importexport\CollectionFactory as ImportexportCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class ImportexportRepository implements ImportexportRepositoryInterface
{

    protected $importexportCollectionFactory;

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $extensionAttributesJoinProcessor;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $dataImportexportFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $importexportFactory;

    private $storeManager;


    /**
     * @param ResourceImportexport $resource
     * @param ImportexportFactory $importexportFactory
     * @param ImportexportInterfaceFactory $dataImportexportFactory
     * @param ImportexportCollectionFactory $importexportCollectionFactory
     * @param ImportexportSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceImportexport $resource,
        ImportexportFactory $importexportFactory,
        ImportexportInterfaceFactory $dataImportexportFactory,
        ImportexportCollectionFactory $importexportCollectionFactory,
        ImportexportSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->importexportFactory = $importexportFactory;
        $this->importexportCollectionFactory = $importexportCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataImportexportFactory = $dataImportexportFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \EspriModule\Customimportexport\Api\Data\ImportexportInterface $importexport
    ) {
        /* if (empty($importexport->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $importexport->setStoreId($storeId);
        } */
        
        $importexportData = $this->extensibleDataObjectConverter->toNestedArray(
            $importexport,
            [],
            \EspriModule\Customimportexport\Api\Data\ImportexportInterface::class
        );
        
        $importexportModel = $this->importexportFactory->create()->setData($importexportData);
        
        try {
            $this->resource->save($importexportModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the importexport: %1',
                $exception->getMessage()
            ));
        }
        return $importexportModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($importexportId)
    {
        $importexport = $this->importexportFactory->create();
        $this->resource->load($importexport, $importexportId);
        if (!$importexport->getId()) {
            throw new NoSuchEntityException(__('importexport with id "%1" does not exist.', $importexportId));
        }
        return $importexport->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->importexportCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \EspriModule\Customimportexport\Api\Data\ImportexportInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \EspriModule\Customimportexport\Api\Data\ImportexportInterface $importexport
    ) {
        try {
            $importexportModel = $this->importexportFactory->create();
            $this->resource->load($importexportModel, $importexport->getImportexportId());
            $this->resource->delete($importexportModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the importexport: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($importexportId)
    {
        return $this->delete($this->get($importexportId));
    }
}

