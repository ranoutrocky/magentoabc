<?php
/**
 * Copyright © EspriModule All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace EspriModule\Customimportexport\Model\Data;

use EspriModule\Customimportexport\Api\Data\ImportexportInterface;

class Importexport extends \Magento\Framework\Api\AbstractExtensibleObject implements ImportexportInterface
{

    /**
     * Get importexport_id
     * @return string|null
     */
    public function getImportexportId()
    {
        return $this->_get(self::IMPORTEXPORT_ID);
    }

    /**
     * Set importexport_id
     * @param string $importexportId
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     */
    public function setImportexportId($importexportId)
    {
        return $this->setData(self::IMPORTEXPORT_ID, $importexportId);
    }

    /**
     * Get event
     * @return string|null
     */
    public function getEvent()
    {
        return $this->_get(self::EVENT);
    }

    /**
     * Set event
     * @param string $event
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     */
    public function setEvent($event)
    {
        return $this->setData(self::EVENT, $event);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \EspriModule\Customimportexport\Api\Data\ImportexportExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \EspriModule\Customimportexport\Api\Data\ImportexportExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get date
     * @return string|null
     */
    public function getDate()
    {
        return $this->_get(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \EspriModule\Customimportexport\Api\Data\ImportexportInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }
}

