<?php
/**
 * @category IbnabMegaMenuExtended
 * @package  WilliamsCommerce\IbnabMegaMenuExtended
 * @author   WilliamsCommerce <gaurav.verma@williamscommerce.com>
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'EspriModule_ProductName', __DIR__);
