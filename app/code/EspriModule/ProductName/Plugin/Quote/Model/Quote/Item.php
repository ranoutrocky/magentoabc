<?php
namespace EspriModule\ProductName\Plugin\Quote\Model\Quote;

class Item {

    public function afterSetProduct(
        \Magento\Quote\Model\Quote\Item $subject,
        $result
    ){
        $productName = html_entity_decode($subject->getName());
        $subject->setName($productName);
    }

}
