<?php

namespace EspriModule\ProductName\Plugin\Customer;

class LoginPost
{
    protected $formKey;
    protected $cart;
    protected $product;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Catalog\Model\Product $product,
        \Magento\Customer\Model\Session $session,
        array $data = []
    ) {
        $this->formKey = $formKey;
        $this->cart = $cart;
        $this->_customerSession = $session;
        $this->product = $product;
        $this->_request = $context->getRequest();
        $this->_response = $context->getResponse();
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->resultFactory = $context->getResultFactory();
    }

    public function aroundExecute(\Magento\Customer\Controller\Account\LoginPost $subject, $proceed)
    {
        $login =  $this->_request->getPost('login');
        $custom_redirect = false;

        $returnValue = $proceed();

        if ($this->_customerSession->isLoggedIn()) {

            $customer = $this->_customerSession;
           $customermsId = $customer->getCustomer()->getData('ms_id');
            if ($customermsId) {
                // echo "===";
            }else{
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('businessaccount/index/index');
                return $resultRedirect;
            }

// exit;

            if ($this->_request->getPost('quickadd') == "addbulk") {
                $productId = 14;
                $params = array(
                    'form_key' => $this->formKey->getFormKey(),
                    'product' => $productId, //product Id
                    'qty'   => 1 //quantity of product                
                );
                //Load the product based on productID   
                $_product = $this->product->load($productId);
                $this->cart->addProduct($_product, $params);
                $this->cart->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout');
                return $resultRedirect;
            } elseif ($this->_request->getPost('quickadd') == "addsingle") {
                $productId = 10;
                $params = array(
                    'form_key' => $this->formKey->getFormKey(),
                    'product' => $productId, //product Id
                    'qty'   => 1 //quantity of product                
                );
                //Load the product based on productID   
                $_product = $this->product->load($productId);
                $this->cart->addProduct($_product, $params);
                $this->cart->save();
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('checkout');
                return $resultRedirect;
            } else {
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('businessaccount/index/index/');
                return $resultRedirect;
            }
        } else {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('');
            return $resultRedirect;
        }
    }
}
