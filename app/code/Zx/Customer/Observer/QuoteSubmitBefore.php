<?php
namespace Zx\Customer\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\PaymentException;
use Magento\Customer\Model\Session as CustomerSession;

class QuoteSubmitBefore implements ObserverInterface
{
    /**
     * @var CustomerSession
     */
    protected $session;
	
	/**
     * @var customer
     */
    protected $customer;
	
	/**
     * @var customer
     */
    protected $amastyResourceCustomer;

    /**
     * QuoteSubmitBefore constructor.
     * @param CustomerSession $session
     */
    public function __construct(
        CustomerSession $session,
		\Magento\Customer\Model\Customer $customer,
		\Amasty\CompanyAccount\Model\ResourceModel\Customer $amastyResourceCustomer
    ) {
        $this->session = $session;
		$this->customer = $customer;
		$this->amastyResourceCustomer = $amastyResourceCustomer;
    }

    public function execute(Observer $observer)
    {
		if($this->session->isLoggedIn()) {
			
			// Company Super User Id
			$customerId = $this->session->getId();
			$superUserId = $this->amastyResourceCustomer->getSuperUserIdBySubAccountId($customerId);
			$customer = $this->customer->load($superUserId);
			
			//$msId = $this->session->getCustomer()->getMsId();
			$msId = $customer->getMsId();
			
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ms-log.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Log added');
            //$logger->info('customer Id '.$this->session->getCustomer()->getId());
			$logger->info('customer Id '.$customer->getId());
            $logger->info('Ms Id '.$msId);
            if (trim($msId) == '') {
                throw new PaymentException(__('Sorry,You can not place order as your account is under review.'));
            }
        }
    }
}
