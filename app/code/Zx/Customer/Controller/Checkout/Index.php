<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Zx\Customer\Controller\Checkout;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Checkout\Controller\Index\Index as CoreCheckoutIndex;

class Index extends CoreCheckoutIndex
{
    /**
     * @var customer
     */
    protected $customer;
	
	/**
     * @var customer
     */
    protected $amastyResourceCustomer;
	
	/**
     * QuoteSubmitBefore constructor.
     * @param CustomerSession $session
     */
    public function __construct(
		\Magento\Customer\Model\Customer $customer,
		\Amasty\CompanyAccount\Model\ResourceModel\Customer $amastyResourceCustomer
    ) {
		$this->customer = $customer;
		$this->amastyResourceCustomer = $amastyResourceCustomer;
    }
	
	/**
     * Checkout page
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $session = $this->_objectManager->get(\Magento\Customer\Model\Session::class);
        if($session->isLoggedIn()) {
			
			// Company Super User Id
			$customerId = $session->getId();
			$superUserId = $this->amastyResourceCustomer->getSuperUserIdBySubAccountId($customerId);
			$customer = $this->customer->load($superUserId);
			
            //$msId = $session->getCustomer()->getMsId();
			$msId = $customer->getMsId();
			
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/ms-log.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info('Log added before');
            //$logger->info('customer Id '.$session->getCustomer()->getId());
			$logger->info('customer Id '.$customer->getId());
            $logger->info('Ms Id '.$msId);
            if ($msId == '') {
                $this->messageManager->addErrorMessage(__('Sorry,You can not place order as your account is under review.'));

                return $this->resultRedirectFactory->create()->setPath('checkout/cart');
            }
        }

        return parent::execute();
    }
}
