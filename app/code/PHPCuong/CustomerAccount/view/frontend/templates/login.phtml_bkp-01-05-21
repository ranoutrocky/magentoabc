<?php
/**
 * GiaPhuGroup Co., Ltd.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GiaPhuGroup.com license that is
 * available through the world-wide-web at this URL:
 * https://www.giaphugroup.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    PHPCuong
 * @package     PHPCuong_CustomerAccount
 * @copyright   Copyright (c) 2018-2019 GiaPhuGroup Co., Ltd. All rights reserved. (http://www.giaphugroup.com/)
 * @license     https://www.giaphugroup.com/LICENSE.txt
 */
 /** @var \PHPCuong\CustomerAccount\Block\Form\Login $block */

 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$customerSession = $objectManager->create('Magento\Customer\Model\Session');
$storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
$currentStore = $storeManager->getStore();
?>
<?php if (!$block->customerIsAlreadyLoggedIn()): ?>
    <style>
        .customer-popup-login {
            display: none;
        }
        .or-another-selection {
            display: inline-block;
            padding-right: 5px;
        }
        @media(max-width: 767px) {
            .or-another-selection {
                display: block;
                text-align: center;
                margin-bottom: 5px;
            }
        }
    </style>
    <div id="customer-popup-login" class="customer-popup-login login-box">
    <div class="login-left">
		<img src="<?= $block->getViewFileUrl('images/sign-up-1.svg'); ?>" class="topShape">
		<img src="<?= $block->getViewFileUrl('images/sign-up-2.svg'); ?>" class="bottomShape">
		<div class="">
			<h2 class="text-center">Returning Customer</h2>
			<p class="text-center">Welcome to the EspriGas Online Ordering Portal!</p>
			<img src="<?= $block->getViewFileUrl('images/CokeConsolidated Image.png'); ?>" alt="" class="img-fluid">
		</div>
	</div>
	<div class ="login-right">
        <button class="action-close close" data-role="closeBtn " data-dismiss="modal" type="button"> <span>Close</span></button>
        <div class="block block-customer-login">
            <div class="block-content" aria-labelledby="block-customer-popup-login-heading">
                <h2 class="login-title">Sign in</h2>
                <form class="form form-login"
                      action="<?php /* @escapeNotVerified */ echo $block->getPostActionUrl() ?>"
                      method="post"
                      id="customer-popup-login-form"
                      data-mage-init='{"validation":{}}'>
                    
                    <?php echo $block->getBlockHtml('formkey'); ?>
                    <input type="hidden" name="redirect_url" value="<?php echo $currentStore->getBaseUrl().'custommodule/index/'; ?>" />
                    <fieldset class="fieldset login" data-hasrequired="<?php /* @escapeNotVerified */ echo __('* Required Fields') ?>">
                        <div class="field note"><?php /* @escapeNotVerified */ echo __('If you have an account, sign in with your email address.') ?></div>
                        <div class="messages"></div>
                        <div class="form-group field email required">
                            <input name="username" value="" <?php if ($block->isAutocompleteDisabled()) :?> autocomplete="off"<?php endif; ?> id="email-login" type="email" class="input-text form-control" placeholder="Email address" title="<?php /* @escapeNotVerified */ echo __('Email') ?>" data-validate="{required:true, 'validate-email':true}">
                        </div>
                        <div class="form-group field password required">
                            <input name="password" type="password" <?php if ($block->isAutocompleteDisabled()) :?> autocomplete="off"<?php endif; ?> class="input-text form-control" id="pass-login" placeholder="Password" title="<?php /* @escapeNotVerified */ echo __('Password') ?>" data-validate="{required:true}" >
                        </div>
                        <div class="form-group clearfix">
                            <div class="checkbox float-left">
                                <label><input type="checkbox"> Remember me</label>
                            </div>
                            <div style="text-align: right;" class="checkbox-right customer-password-link"><a href="<?php /* @escapeNotVerified */ echo $block->getCustomerforgotUrl() ?>" >Forgot Password</a></div>
                        </div>
                        <div class="form-group"></div>
                        <div class="text-center actions-toolbar">
                            <div class=""><button type="submit" class="btn btn-secondary btn-block btn-common-orange" name="send" id="send2-login"><span><?php /* @escapeNotVerified */ echo __('Sign In') ?></span></button></div>
                            <div class="clearfix"></div>
                            <?php if ($block->getRegistration()->isAllowed()): ?>
                                <!-- <div class="or-another-selection"><?php echo __('or'); ?></div> -->
                                <div class="forget-pass">New to EspriGas?  <a class="action remind signupa customer-register-link" href="<?php /* @escapeNotVerified */ echo $block->getCustomerRegistrationUrl() ?>" id="customer-popup-registration"><span><?php /* @escapeNotVerified */ echo __('Create an Account') ?></span></a></div>
                            <?php endif; ?>
                        </div>
                    </fieldset>
                </form>
            </div>
			</div>
        </div>
        <script type="text/x-magento-init">
            {
                "#customer-popup-login": {
                    "PHPCuong_CustomerAccount/js/action/customer-authentication-popup": {
                        "popupTitle": "<?php /* @escapeNotVerified */ echo __('Sign In') ?>",
                        "innerWidth": "730"
                    }
                }
            }
        </script>
    </div>
<?php endif; ?>
