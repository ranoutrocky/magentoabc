<?php
namespace MarkShust\OrderGrid\Model\ResourceModel\Order\Grid;

use Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult;

/**
 * Class Collection
 * @package MarkShust\OrderGrid\Model\ResourceModel\Order\Grid
 */
class Collection extends SearchResult
{
    /**
     * Initialize the select statement.
     *
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        // Add the sales_order_item model to this collection
        $this->join(
            [$this->getTable('sales_order_item')],
            "main_table.entity_id = {$this->getTable('sales_order_item')}.order_id",
            []
        );

        // Group by the order id, which is initially what this grid is id'd by
        $this->getSelect()->group('main_table.entity_id');

        return $this;
    }

    /**
     * Add field to filter.
     *
     * @param string|array $field
     * @param string|int|array|null $condition
     * @return SearchResult
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field === 'products' && !$this->getFlag('product_filter_added')) {
            // Add the sales/order_item model to this collection
            $this->getSelect()->join(
                [$this->getTable('sales_order_item')],
                "main_table.entity_id = {$this->getTable('sales_order_item')}.order_id",
                []
            );

            // Group by the order id, which is initially what this grid is id'd by
            $this->getSelect()->group('main_table.entity_id');

            // On the products field, let's add the sku and name as filterable fields
            $this->addFieldToFilter([
                "{$this->getTable('sales_order_item')}.sku",
                "{$this->getTable('sales_order_item')}.name",
            ], [
                $condition,
                $condition,
            ]);

            $this->setFlag('product_filter_added', 1);
        }

        return parent::addFieldToFilter($field, $condition);
    }

    /**
     * Perform operations after collection load.
     *
     * @return SearchResult
     */
    protected function _afterLoad()
    {
        $items = $this->getColumnValues('entity_id');

        if (count($items)) {
            $connection = $this->getConnection();

            // Build out item sql to add products to the order data
            $select = $connection->select()
                ->from([
                    'sales_order_item' => $this->getTable('sales_order_item'),
                ], [
                    'order_id',
                    'product_id'  => new \Zend_Db_Expr('GROUP_CONCAT(`sales_order_item`.item_id SEPARATOR "|")'),
                    'product_skus'  => new \Zend_Db_Expr('GROUP_CONCAT(`sales_order_item`.sku SEPARATOR "|")'),
                    'product_names' => new \Zend_Db_Expr('GROUP_CONCAT(`sales_order_item`.name SEPARATOR "|")'),
                    'product_qtys'  => new \Zend_Db_Expr('GROUP_CONCAT(`sales_order_item`.qty_ordered SEPARATOR "|")'),
                ])
                ->where('order_id IN (?)', $items)
                ->where('parent_item_id IS NULL') // Eliminate configurable products, otherwise two products show
                ->group('order_id');

            $itemCollection = $connection->fetchAll($select);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
            
            // Loop through this sql an add items to related orders
            $i=0;
            foreach ($itemCollection as $item) {
                $row = $this->getItemById($item['order_id']);
                $productSkus = explode('|', $item['product_skus']);
                $productQtys = explode('|', $item['product_qtys']);
                $productNames = explode('|', $item['product_names']);
                $html = '';
                $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($item['order_id']);
                
                $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                foreach ($productSkus as $index => $sku) {
                    $html .= sprintf('%d x [%s] %s ', $productQtys[$index], $sku, $productNames[$index]);
                }
                if ($order->getShippingMethod()=="flatrate_flatrate") {
                    $rush="Yes";
                }else{
                    $rush ="No";
                }
                $count = count($productNames);
            $date=date_create($order->getCreatedAt());
                $createddate= date_format($date,"m/d/Y");
                $row->setOrderDate($createddate);
                $row->setMagentoCustId($order->getCustomerId());
                $row->setMagentoOrderNumber($order->getIncrementId());
                $row->setMagentoOrderItemNumber($item['product_id']);
                $row->setLineNumber(number_format($count));
                $row->setDescription($html);
                
                $row->setQty($item['product_qtys']);
                $row->setCustomerNav($customerObj->getData('ms_id'));
                $row->setRushDelivery($rush);

            $i++;}
            
        }

        return parent::_afterLoad();
    }

    /**
     * Get the record count.
     *
     * @return int
     */
    public function getSize()
    {
        if ($this->_totalRecords === null) {
            $sql = $this->getSelectCountSql();
            $this->_totalRecords = count($this->getConnection()->fetchAll($sql, $this->_bindParams));
        }

        return (int) $this->_totalRecords;
    }
}
