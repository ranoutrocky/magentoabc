<?php
namespace Venture7\AccountReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Venture7\AccountReport\Model\CustomAttribute;

class PricingPlan extends Column
{
    /**
     * @var \Venture7\AccountReport\Model\CustomAttribute
     */
    private $customAttribute;

    /**
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CustomAttribute $customAttribute,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->customAttribute = $customAttribute;
    }
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item)
            {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customer =    $objectManager->create('Magento\Customer\Model\Customer')->load($item['entity_id']);
                
                $pricingPlanLabel = $customer->getPricingPlan();

                $attributeCode = 'pricing_plan';
                $options = $this->customAttribute->getAttributeOptions($attributeCode);
                foreach ($options as $option) {
                    if($customer->getPricingPlan() == $option['value']) {
                        $pricingPlanLabel = $option['label'];
                    }
                }

                $item[$this->getData('name')] = $pricingPlanLabel;
            }
        }
        return $dataSource;
    }
}
?>