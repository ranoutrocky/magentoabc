<?php

namespace Venture7\AccountReport\Ui\Component\Listing\Column\CustomerGroup;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Group\Collection
     */
    private $customerGroup;

    public function __construct(
       \Magento\Customer\Model\ResourceModel\Group\Collection $customerGroup
    ) {
        $this->customerGroup = $customerGroup;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $options = $this->customerGroup->toOptionArray();
            array_splice($options,0,1);
            $this->options = $options;
        }

        return $this->options;
    }
}
