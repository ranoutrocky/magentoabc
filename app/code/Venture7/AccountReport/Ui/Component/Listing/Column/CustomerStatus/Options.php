<?php

namespace Venture7\AccountReport\Ui\Component\Listing\Column\CustomerStatus;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Venture7\AccountReport\Model\CustomAttribute
     */
    private $customAttribute;

    public function __construct(
        \Venture7\AccountReport\Model\CustomAttribute $customAttribute
    ) {
        $this->customAttribute = $customAttribute;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
             $attributeCode = 'customer_status';
            $options = $this->customAttribute->getAttributeOptions($attributeCode);
            $this->options = $options;
        }
        
        return $this->options;
    }
}
