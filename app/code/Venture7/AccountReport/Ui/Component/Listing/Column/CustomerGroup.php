<?php
namespace Venture7\AccountReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Customer\Model\ResourceModel\Group\Collection;

class CustomerGroup extends Column
{
    /**
     * @var \Venture7\AccountReport\Model\CustomAttribute
     */
    private $customerGroup;

    /**
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Collection $customerGroup,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->customerGroup = $customerGroup;
    }
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item)
            {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customer =    $objectManager->create('Magento\Customer\Model\Customer')->load($item['entity_id']);
                
                $customerGroup = $customer->getGroupId();
                $groups = $this->customerGroup->toOptionArray();
                array_splice($groups,0,1);
                
                foreach ($groups as $group) {
                    if($customer->getGroupId() == $group['value']) {
                        $customerGroup = $group['label'];
                    }
                }
                
                $item[$this->getData('name')] = $customerGroup;
            }
        }
        return $dataSource;
    }
}
?>