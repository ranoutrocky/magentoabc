<?php
namespace Venture7\AccountReport\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class ProductActions
 *
 * @api
 * @since 100.0.2
 */
class Actions extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $storeId = $this->context->getFilterParam('store_id');

            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')] = [
                        'login' => [
                            'href' => $this->urlBuilder->getUrl(
                                'mploginascustomer/login/index',
                                [ 'id' => $item['entity_id'], 'store' => $storeId]
                            ),
                            'target' => '_blank',
                            'label' => __('Login As'),
                            'hidden' => false
                        ],
                        'resetpassword' => [
                            'href' => $this->urlBuilder->getUrl(
                                'venture7_account/reports/resetPassword',
                                [ 'customer_id' => $item['entity_id'], 'store' => $storeId]
                            ),
                            'label' => __('Reset Password'),
                            'hidden' => false
                        ]
                    ];
            }
        }

        return $dataSource;
    }
}
