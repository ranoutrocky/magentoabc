<?php

namespace Venture7\AccountReport\Model;

class CustomAttribute
{
	protected $customAttribute;

	public function __construct(
        \Venture7\AccountReport\Helper\CustomAttribute $customAttribute
    ) {
        $this->customAttribute = $customAttribute;
    }

    public function getCustomAttribute($attributeCode)
	{
		return $this->customAttribute->getUserDefinedAttribures($attributeCode);
	}

	public function getFrontEndLabel($attributeCode)
	{
		$attribute = $this->getCustomAttribute($attributeCode);
		return $attribute->getStoreLabel($this->customAttribute->getStoreId());
	}

	public function getFieldValue($attributeCode)
	{
		$attribute = $this->getCustomAttribute($attributeCode);
		return $this->customAttribute->getCustomer($this->getCustomerId())->getData($attribute->getAttributeCode());
	}

	public function getAttributeOptions($attributeCode)
	{
		$attribute = $this->getCustomAttribute($attributeCode);
		return $this->customAttribute->getAttributeOptions($attribute->getAttributeCode());
	}
}