<?php
namespace Venture7\BusinessAccount\Block;

use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Customer\Helper\Address;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;

class Index extends \Magento\Directory\Block\Data
{

    protected $_address = null;

	protected $_customAttribute;

	protected $_customerSession;

	protected $_addressRepository;

	protected $_objectManager;

	protected $addressDataFactory;

	protected $currentCustomer;

    protected $_storeManager;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerFactory
     */
    protected $customerFactory;

    protected $country;

    protected $regionFactory;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Directory\Helper\Data $directoryHelper,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
		\Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regionCollectionFactory,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
		\Venture7\BusinessAccount\Helper\CustomAttribute $customAttribute,
		\Magento\Customer\Model\Session $customerSession,
		\Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
		\Magento\Framework\ObjectManagerInterface $objectmanager,
		\Magento\Customer\Api\Data\AddressInterfaceFactory $addressDataFactory,
		\Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
		\Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory,
        \Magento\Directory\Model\Country $country,
        \Magento\Directory\Model\RegionFactory $regionFactory,
		array $data = [],
		AddressMetadataInterface $addressMetadata = null,
        Address $addressHelper = null
	) {
		$this->_customAttribute = $customAttribute;
		$this->_customerSession = $customerSession;
		$this->_addressRepository = $addressRepository;
		$this->_objectManager = $objectmanager;
		$this->addressDataFactory = $addressDataFactory;
		$this->currentCustomer = $currentCustomer;
		$this->addressMetadata = $addressMetadata ?: ObjectManager::getInstance()->get(AddressMetadataInterface::class);
        $data['addressHelper'] = $addressHelper ?: ObjectManager::getInstance()->get(Address::class);
        $data['directoryHelper'] = $directoryHelper;
		$this->dataObjectHelper = $dataObjectHelper;
        $this->_storeManager = $storeManager;
        $this->customer = $customer;
        $this->customerFactory = $customerFactory;
        $this->country = $country;
        $this->regionFactory = $regionFactory;
		parent::__construct(
			$context,
			$directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $data
		);
	}

    /**
     * Prepare the layout of the address edit block.
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        $this->initAddressObject();

        $title = 'My Business Info';
        $this->pageConfig->getTitle()->set($title);

        if ($postedData = $this->_customerSession->getAddressFormData(true)) {
            $postedData['region'] = [
                'region_id' => isset($postedData['region_id']) ? $postedData['region_id'] : null,
                'region' => $postedData['region'],
            ];
            $this->dataObjectHelper->populateWithArray(
                $this->_address,
                $postedData,
                \Magento\Customer\Api\Data\AddressInterface::class
            );
        }

        $this->precheckRequiredAttributes();
        return $this;
    }

    /**
     * Initialize address object.
     *
     * @return void
     */
    private function initAddressObject()
    {
        $customer = $this->_customAttribute->getCustomer($this->getCustomerId());

		$billingAddress = $customer->getDefaultBillingAddress();

        if($billingAddress) {
            $addressId = $billingAddress->getId();
        } else {
            $addressId = null;
        }

        // Init address object
        if ($addressId) {
            try {
                $this->_address = $this->_addressRepository->getById($addressId);

                if ($this->_address->getCustomerId() != $this->_customerSession->getCustomerId()) {
                    $this->_address = null;
                }
            } catch (NoSuchEntityException $e) {
                $this->_address = null;
            }
        }

        if ($this->_address === null || !$this->_address->getId()) {
            $this->_address = $this->addressDataFactory->create();
            $customer = $this->getCustomer();
            $this->_address->setPrefix($customer->getPrefix());
            $this->_address->setFirstname($customer->getFirstname());
            $this->_address->setMiddlename($customer->getMiddlename());
            $this->_address->setLastname($customer->getLastname());
            $this->_address->setSuffix($customer->getSuffix());
        }
    }

    /**
     * Precheck attributes that may be required in attribute configuration.
     *
     * @return void
     */
    private function precheckRequiredAttributes()
    {
        $precheckAttributes = $this->getData('check_attributes_on_render');
        $requiredAttributesPrechecked = [];
        if (!empty($precheckAttributes) && is_array($precheckAttributes)) {
            foreach ($precheckAttributes as $attributeCode) {
                $attributeMetadata = $this->addressMetadata->getAttributeMetadata($attributeCode);
                if ($attributeMetadata && $attributeMetadata->isRequired()) {
                    $requiredAttributesPrechecked[$attributeCode] = $attributeCode;
                }
            }
        }
        $this->setData('required_attributes_prechecked', $requiredAttributesPrechecked);
    }

	public function getAddress()
	{
		return $this->_address;
	}

	 public function getCustomer()
    {
        return $this->currentCustomer->getCustomer();
    }

    /**
     * Return the specified numbered street line.
     *
     * @param int $lineNumber
     * @return string
     */
    public function getStreetLine($lineNumber)
    {
        $street = $this->_address->getStreet();
        return isset($street[$lineNumber - 1]) ? $street[$lineNumber - 1] : '';
    }

    /**
     * Return the country Id.
     *
     * @return int|null|string
     */
    public function getCountryId()
    {
        if ($countryId = $this->getAddress()->getCountryId()) {
            return $countryId;
        }
        return parent::getCountryId();
    }

    /**
     * Return the name of the region for the address being edited.
     *
     * @return string region name
     */
    public function getRegion()
    {
        $region = $this->getAddress()->getRegion();
        return $region === null ? '' : $region->getRegion();
    }

    /**
     * Return the id of the region being edited.
     *
     * @return int region id
     */
    public function getRegionId()
    {
        $region = $this->getAddress()->getRegion();
        return $region === null ? 0 : $region->getRegionId();
    }

    /**
     * Get config value.
     *
     * @param string $path
     * @return string|null
     */
    public function getConfig($path)
    {
        return $this->_scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    public function getCustomAttribute($attributeCode)
	{
		return $this->_customAttribute->getUserDefinedAttribures($attributeCode);
	}

	public function getFrontEndLabel($attributeCode)
	{
		$attribute = $this->getCustomAttribute($attributeCode);
		return $attribute->getStoreLabel($this->_customAttribute->getStoreId());
	}

	public function getFieldValue($attributeCode)
	{
		$attribute = $this->getCustomAttribute($attributeCode);
		$fieldValue = $this->_customAttribute->getCustomer($this->getCustomerId())->getData($attribute->getAttributeCode());
		return $fieldValue;
	}

	public function getAttributeOptions($attributeCode)
	{
		$attribute = $this->getCustomAttribute($attributeCode);
		return $this->_customAttribute->getAttributeOptions($attribute->getAttributeCode());
	}

	public function getCustomerId()
	{
		$customerSession = $this->_objectManager->create("Magento\Customer\Model\Session");
		return $customerSession->getCustomerId();
	}

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    public function getCustomerData()
    {
        $customerId = $this->_customerSession->getCustomerId();
        $customer = $this->customer->load($customerId);

        return $customer;
    }

    public function getRegionsOfCountry($countryCode)
    {
        $regionCollection = $this->country->loadByCode($countryCode)->getRegions();
        $regions = $regionCollection->loadData()->toOptionArray(false);
        return $regions;
    }

    public function getRegionById($regionId)
    {
        return $this->regionFactory->create()->load($regionId);
    }

}
