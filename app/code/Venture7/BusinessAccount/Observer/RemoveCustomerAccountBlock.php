<?php

namespace Venture7\BusinessAccount\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RemoveCustomerAccountBlock implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    private $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;

    /**
     * @var \Magento\Customer\Model\ResourceModel\CustomerFactory
     */
	protected $customerFactory;

    /**
     * @var \Amasty\CompanyAccount\Model\CompanyContext
     */
    private $companyContext;

     /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
   		\Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Session $session,
        \Magento\Customer\Model\Customer $customer,
		\Magento\Customer\Model\ResourceModel\CustomerFactory $customerFactory,
        \Amasty\CompanyAccount\Model\CompanyContext $companyContext,
        \Amasty\CompanyAccount\Model\ResourceModel\Customer\CollectionFactory $collectionFactory
	) {
    	$this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->session = $session;
        $this->customer = $customer;
		$this->customerFactory = $customerFactory;
        $this->companyContext = $companyContext;
        $this->collectionFactory = $collectionFactory;
	}

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $event = $observer->getEvent();
        $layout = $observer->getLayout();

        $currentUrl = $this->url->getCurrentUrl();

        $customerId = $this->session->getCustomerId();
        $customer = $this->customer->load($customerId);

		$customer->getCustomerStatus();

		$customerNewStatus = 6;
        $customerApprovedStatus = 11;

        $company = $this->companyContext->getCurrentCompany();
        $user = $this->collectionFactory->create()->getCompanyCustomers((int)$company->getCompanyId())->addFieldToFilter('customer_id', ['eq' => $customerId])->getFirstItem();

        $roleName = $user->getRoleName();

         // <Delimiters
        $layout->unsetElement('customer-account-navigation-delimiter-0');
        $layout->unsetElement('customer-account-navigation-delimiter-1');
        $layout->unsetElement('customer-account-navigation-delimiter-2');

        if($customer->getCustomerStatus() == $customerNewStatus && $roleName != 'User'){
        	$layout->unsetElement('customer_dashboard');
        	$layout->unsetElement('customer_invoice');

			$layout->unsetElement('customer-account-navigation-customer-balance-link');
			$layout->unsetElement('customer-account-navigation-downloadable-products-link');
			$layout->unsetElement('customer-account-navigation-newsletter-subscriptions-link');
			$layout->unsetElement('customer-account-navigation-billing-agreements-link');
			$layout->unsetElement('customer-account-navigation-product-reviews-link');
			$layout->unsetElement('customer-account-navigation-my-credit-cards-link');
			$layout->unsetElement('customer-account-navigation-account-link');
			$layout->unsetElement('customer-account-navigation-account-edit-link');
			$layout->unsetElement('customer-account-navigation-address-link');
			$layout->unsetElement('customer-account-navigation-orders-link');
			$layout->unsetElement('customer-account-navigation-wish-list-link');
			$layout->unsetElement('customer-account-navigation-gift-card-link');
			$layout->unsetElement('customer-account-navigation-checkout-sku-link');
			$layout->unsetElement('customer-account-navigation-giftregistry-link');
            $layout->unsetElement('customer-account-navigation-reward-link');
            $layout->unsetElement('stripe-payments-subscriptions');
            $layout->unsetElement('amcompany-customer-account-tab-company-roles');
        }

        if($roleName == 'User') {
            $layout->unsetElement('stripe-payments-customer-cards');
            $layout->unsetElement('customer-account-navigation-businessaccount');
            $layout->unsetElement('customer-account-navigation-orders-link');
        }
        else {
            $layout->unsetElement('amcompany-customer-account-tab-company-orders');
        }

        // Show users menu if customer approved
        if($customer->getCustomerStatus() != $customerApprovedStatus) {
            $layout->unsetElement('amcompany-customer-account-tab-company-users');

            if(strpos($currentUrl, 'user')) {
                $this->noRoute();
            }
        }

        // Credit card not accessable to User Role
        if($roleName == 'User') {
            if(strpos($currentUrl, 'stripe') || strpos($currentUrl, 'businessaccount')) {
                $this->noRoute();
            }
        }

        if(!strpos($currentUrl, 'businessaccount') && !strpos($currentUrl, 'stripe/customer/cards') && $customer->getCustomerStatus() == $customerNewStatus && $roleName != 'User') {
        	$redirectUrl = $this->url->getUrl('businessaccount/index/index');
            $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
            exit;
        }
    }

    protected function noRoute()
    {
        $redirectUrl = $this->url->getUrl('noroute');
        $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
        exit;
    }
}