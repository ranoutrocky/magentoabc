<?php

namespace Venture7\BusinessAccount\Controller\BPRef;

class Search extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	protected $_jsonResultFactory;

	protected $_bprefFactory;
	
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		\Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
		\Venture7\BusinessAccount\Model\BPRefFactory $bprefFactory
	) {
		$this->_pageFactory = $pageFactory;
		$this->_jsonResultFactory = $jsonResultFactory;
		$this->_bprefFactory = $bprefFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		if ($this->getRequest()->isPost()) {

			$postData = $this->getRequest()->getPostValue();

			$cokeId = $postData['search_coke_id'];
			$postcode = $postData['search_postcode'];
			$magento_customer_id = $postData['magento_customer_id'];

			$refData = $this->_bprefFactory->create();
			$collection = $refData->getCollection()
								  ->addFieldToFilter('coke_id', ['eq' => $cokeId])
								  ->addFieldToFilter('postcode', ['eq' => $postcode])
								  ->addFieldToFilter('magento_customer_id', ['neq' => $magento_customer_id])
								  ->getFirstItem();

			$row = $collection->getData();
			
			if($row) {
				$countryCode = 'US';
				$regionId = $this->getRegionIdByCode($row['state'], $countryCode);
				$row['region_id'] = $regionId;
			}

			$result = $this->_jsonResultFactory->create();
	    	$result->setData($row);

	    	return $result;
   		}
	}

	protected function getRegionIdByCode($stateCode, $countryCode)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\Region')->loadByCode($stateCode, $countryCode);
        $regionData = $region->getData();

        return $regionData['region_id'];
    }

}

