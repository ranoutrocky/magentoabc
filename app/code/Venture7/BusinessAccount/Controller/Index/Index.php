<?php

namespace Venture7\BusinessAccount\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
	
	/**
      * Page Factory
      *
      * @var  \Magento\Framework\View\Result\PageFactory
      */
	protected $_pageFactory;

	/**
      * Customer Session
      *
      * @var \Magento\Customer\Model\Session
      */
    protected $_customer;

    protected $_messageManager;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Customer\Model\Session $customer,
		\Magento\Framework\View\Result\PageFactory $pageFactory
	)
	{
		$this->_pageFactory = $pageFactory;
		$this->_customer = $customer;
		return parent::__construct($context);
	}

	public function execute()
	{
		$resultRedirect = $this->resultRedirectFactory->create();
		
		if(!$this->_customer->isLoggedIn()) {
			return $resultRedirect->setPath('customer/account/login');
		}

		return $this->_pageFactory->create();
	}
}
