<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Venture7\BusinessAccount\Controller\Index;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\InputException;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerFactory;

use Amasty\CompanyAccount\Api\Data\CompanyInterface;
use Amasty\CompanyAccount\Controller\AbstractAction;
use Amasty\CompanyAccount\Model\Source\Company\Status as CompanyStatus;
use Magento\Framework\Exception\LocalizedException;

use Amasty\CompanyAccount\Api\Data\PermissionInterface;
use Amasty\CompanyAccount\Api\Data\RoleInterface;
use Amasty\CompanyAccount\Api\Data\RoleInterfaceFactory;
use Amasty\CompanyAccount\Api\RoleRepositoryInterface;
use Amasty\CompanyAccount\Model\Repository\PermissionRepository;


class Save extends \Magento\Framework\App\Action\Action
{
    const SESSION_NAME = 'Amasty_CompanyAccount_Data';
    const ROLE_PERMISSIONS = 'Amasty_CompanyAccount::orders_add,Amasty_CompanyAccount::orders,Amasty_CompanyAccount::index,Amasty_CompanyAccount::orders_all_view,Amasty_CompanyAccount::orders_view,Amasty_CompanyAccount::use_credit';

    protected $formKeyValidator;
	
	protected $session;
	
	protected $customerRepository;
	
	protected $customer;

	protected $customerFactory;

	protected $_bprefFactory;

	/**
     * @var  CompanyInterface
     */
    private $currentCompany;

	 /**
     * @var \Amasty\CompanyAccount\Api\Data\CompanyInterfaceFactory
     */
    private $companyFactory;

    /**
     * @var \Amasty\CompanyAccount\Api\CompanyRepositoryInterface
     */
    private $companyRepository;

    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    protected $addressRepository;

      /**
     * @var \Amasty\CompanyAccount\Api\Data\CustomerInterfaceFactory
     */
    private $customerInterfaceFactory;

    /**
     * @var \Amasty\CompanyAccount\Model\ConfigProvider
     */
    private $configProvider;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Amasty\CompanyAccount\Model\CompanyContext
     */
    private $companyContext;

    /**
     * @var RoleInterfaceFactory
     */
    private $roleFactory;

    /**
     * @var PermissionRepository
     */
    private $permissionRepository;

    /**
     * @var \Amasty\CompanyAccount\Api\RoleRepositoryInterface
     */
    private $roleRepository;

	public function __construct(
        Context $context,
        \Amasty\CompanyAccount\Model\CompanyContext $companyContext,
        \Psr\Log\LoggerInterface $logger,
        Session $customerSession,
		CustomerRepositoryInterface $customerRepository,
		Validator $formKeyValidator,
		Customer $customer,
		CustomerFactory $customerFactory,
		\Venture7\BusinessAccount\Model\BPRefFactory $bprefFactory,
		\Amasty\CompanyAccount\Api\Data\CompanyInterfaceFactory $companyFactory,
		\Amasty\CompanyAccount\Api\CompanyRepositoryInterface $companyRepository,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
		\Magento\User\Model\ResourceModel\User\CollectionFactory $userCollectionFactory,
		\Amasty\CompanyAccount\Model\ConfigProvider $configProvider,
		RoleRepositoryInterface $roleRepository,
		RoleInterfaceFactory $roleFactory,
		PermissionRepository $permissionRepository
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
		$this->customerRepository = $customerRepository;
		$this->formKeyValidator = $formKeyValidator;
		$this->customer = $customer;
		$this->customerFactory = $customerFactory;
        $this->addressRepository = $addressRepository;
		$this->_bprefFactory = $bprefFactory;

		$this->logger = $logger;
		$this->companyContext = $companyContext;
		$this->companyFactory = $companyFactory;
		$this->companyRepository = $companyRepository;
		$this->userCollectionFactory = $userCollectionFactory;
		$this->configProvider = $configProvider;
		$this->roleFactory = $roleFactory;
		$this->permissionRepository = $permissionRepository;
		$this->roleRepository = $roleRepository;
    }

    public function execute()
    {
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());

        if ($validFormKey && $this->getRequest()->isPost()) {

			$customerId = $this->session->getCustomerId();
			$customer = $this->customer->load($customerId);
			$postData = $this->getRequest()->getPostValue();
			$customerData = $this->checkCustomerCokeId($postData['coke_id']);

            $billingAddress = $customer->getDefaultBillingAddress();

            if($billingAddress) {
                $addressId = $billingAddress->getId();
            } else {
                $addressId = null;
            }

			$this->saveCustomerGroupId($customerId, $postData['group_id']);

			if($customer->getCokeId() == $postData['coke_id'])
			{
                //update customer
				$this->saveCustomAttribute($customerId);
                if($addressId) {
                    $this->updateCustomerAddress($addressId);
                } else {
                    $this->saveCustomerAddress($customerId);
                }
			} 
			elseif(empty($customerData)) {
				//new customer
				$this->saveCustomAttribute($customerId);
				$this->saveCustomerAddress($customerId);
			}
			else {
				//already used coke id
				$this->messageManager->addErrorMessage(__('This is not valid Coke ID. Please select another'));
				$resultRedirect = $this->resultRedirectFactory->create();
            	return $resultRedirect->setPath('businessaccount/index/index', array('closemodal' => 'Y'));
			}

			if($postData['organization_name'] && $postData['street'] && $postData['city'] && $postData['region_id'] && $postData['postcode'] && $postData['telephone']) {

				$companyData = array(
					'company_name' => $postData['organization_name'],
					'company_email' => $customer->getEmail(),
					'street' => $postData['street'],
					'city' => $postData['city'],
					'country_id' => $postData['country_id'],
					'region_id' => $postData['region_id'],
					'region' => $postData['region'],
					'postcode' => $postData['postcode'],
					'telephone' => $postData['telephone']
				);

				if($this->getCompanyValue(CompanyInterface::COMPANY_ID)) {
					$companyData['company_id'] = $this->getCompanyValue(CompanyInterface::COMPANY_ID);
				}

				$this->saveCompany($companyData);
			}

			$this->session->setCustomerFormData($this->getRequest()->getPostValue());

			if($postData['submit'] != 'Save for Later') {
				$resultRedirect = $this->resultRedirectFactory->create();
        		return $resultRedirect->setPath('businessaccount/salesforce/createcase');
        	}
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('businessaccount/index/index');
    }

    private function saveCustomAttribute($customerId)
    {
        
		$customer = $this->customer->load($customerId);
		
		$customerData = $customer->getDataModel();
		$customerResource = $this->customerFactory->create();
		
		$postData = $this->getRequest()->getPostValue();

		unset($postData['submit']);
		unset($postData['group_id']);
        unset($postData['firstname']);
        unset($postData['lastname']);
		unset($postData['country_id']);
		unset($postData['region_id']);
		unset($postData['region']);
		unset($postData['street']);
		unset($postData['city']);
		unset($postData['postcode']);
		unset($postData['telephone']);
		unset($postData['form_key']);

		foreach($postData as $attr => $value) {
			
			if($attr == 'other_gases'){
				$customerData->setCustomAttribute($attr, json_encode($value));
				$customer->updateData($customerData);
				$customerResource->saveAttribute($customer, $attr);
			}
			else {
				$customerData->setCustomAttribute($attr, $value);
				$customer->updateData($customerData);
				$customerResource->saveAttribute($customer, $attr);
			}
		}
    }

    private function saveCustomerAddress($customerId)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	    $addresss = $objectManager->get('\Magento\Customer\Model\AddressFactory');

		$postData = $this->getRequest()->getPostValue();

        $firstName = ($postData['firstname'])?$postData['firstname']:$postData['organization_name'];
        $lastName = ($postData['lastname'])?$postData['lastname']:$postData['organization_name'];

		$address = $addresss->create();

		$address->setCustomerId($customerId)
				->setFirstname($firstName)
                ->setLastname($lastName)
				->setCountryId($postData['country_id'])
				->setPostcode($postData['postcode'])
				->setCity($postData['city'])
				->setRegionId($postData['region_id'])
				->setTelephone($postData['telephone'])
				->setStreet($postData['street'])
				->setIsDefaultBilling(true)
				->setIsDefaultShipping(true)
				->setSaveInAddressBook(false);
		try{

			$address->save();
			
			if($postData['cust_ext_id']) {
				$this->updateBPRef($customerId);
			}

            $organizationName = $postData['organization_name'];
			$this->messageManager->addSuccessMessage(__("Thank you, $organizationName! Your information has been submitted."));

		} catch (InputException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addErrorMessage($error->getMessage());
            }
        } catch (\Exception $e) {
        	$resultRedirect = $this->resultRedirectFactory->create();
            $this->messageManager->addExceptionMessage($e, __('We can\'t save the address.'));
            return $resultRedirect->setPath('businessaccount/index/index');
        }
	}

    private function updateCustomerAddress($addressId)
    {
        $address = $this->addressRepository->getById($addressId);

        $postData = $this->getRequest()->getPostValue();

        $firstName = ($postData['firstname'])?$postData['firstname']:$postData['organization_name'];
        $lastName = ($postData['lastname'])?$postData['lastname']:$postData['organization_name'];

        $address->setFirstname($firstName);
        $address->setLastname($lastName);
        $address->setCountryId($postData['country_id']);
        $address->setPostcode($postData['postcode']);
        $address->setCity($postData['city']);
        $address->setRegionId($postData['region_id']);
        $address->setTelephone($postData['telephone']);
        $address->setStreet($postData['street']);

        try{

            $this->addressRepository->save($address);
            
             $organizationName = $postData['organization_name'];
            $this->messageManager->addSuccessMessage(__("Thank you, $organizationName! Your information has been submitted."));

        } catch (InputException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addErrorMessage($error->getMessage());
            }
        } catch (\Exception $e) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $this->messageManager->addExceptionMessage($e, __('We can\'t save the address.'));
            return $resultRedirect->setPath('businessaccount/index/index');
        }
    }

	private function saveCustomerGroupId($customerId, $groupId)
	{
		try {
            $postData = $this->getRequest()->getPostValue();
            $customer = $this->customerRepository->getById($customerId);

            $firstName = ($postData['firstname'])?$postData['firstname']:$postData['organization_name'];
            $lastName = ($postData['lastname'])?$postData['lastname']:$postData['organization_name'];

            $customer->setGroupId($groupId);
            $customer->setFirstname($firstName);
            $customer->setLastname($lastName);

           $this->customerRepository->save($customer);

       } catch (Exception $e){
           $this->messageManager->addErrorMessage(__('Something went wrong! Please try again.'));
       }
	}

	private function updateBPRef($customerId)
	{
		$postData = $this->getRequest()->getPostValue();

		$refData = $this->_bprefFactory->create();

		$collection = $refData->getCollection()->addFieldToFilter('coke_id', ['eq' => $postData['coke_id']]);
        $collection->getFirstItem()->setData('magento_customer_id',$customerId)->save();
	}

	private function checkCustomerCokeId($cokeId) {
		$collection = $this->customer->getCollection()
		   ->addAttributeToSelect("*")
		   ->addAttributeToFilter("coke_id", array("eq" => $cokeId))
		   ->getFirstItem();

		return $collection->getData();
	}

	/* Create company account*/
	protected function saveCompany(array $data)
    {
        $this->session->setData(self::SESSION_NAME, $data);
        try {
        	$this->validateEmail($data);
            $company = $this->companyFactory->create();
            $company->setData($data);
            $this->prepareCompanyData($company);
            $result = $this->companyRepository->save($company);
            $this->session->setData(self::SESSION_NAME, []);
            if( ! $this->getCompanyValue(CompanyInterface::COMPANY_ID)) {
				if($result->getCompanyId()) {
					$this->saveRole($result);
				}
			}
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred on the server. Your changes have not been saved.')
            );
            $this->logger->critical($e);
        }
    }

    /**
     * @param array $data
     *
     * @throws LocalizedException
     */
    protected function validateEmail(array $data)
    {
        if (isset($data[CompanyInterface::COMPANY_EMAIL])
            && $this->isEmailExist($data[CompanyInterface::COMPANY_EMAIL])
            && !isset($data[CompanyInterface::COMPANY_ID])
        ) {
            throw new LocalizedException(
                __('There is already a company account associated with this email address.'
                    . ' Please enter a different email address.')
            );
        }
    }

    /**
     * @param CompanyInterface $company
     */
    private function prepareCompanyData(CompanyInterface $company)
    {
        $customerId = $this->session->getCustomerId();
        $postData = $this->getRequest()->getPostValue();
        $groupId = $postData['group_id'];

        if (!$company->getCompanyId()) {
            $company->setSuperUserId($this->companyContext->getCurrentCustomerId());
            $company->setCustomerGroupId($this->companyContext->getCurrentCustomerGroupId());
            $this->setStatus($company);
            $userCollection = $this->userCollectionFactory->create();
            $userCollection->getSelect()->limit(1);
            $company->setSalesRepresentativeId($userCollection->getFirstItem()->getId());
        }

        $this->setStreet($company);
    }

    /**
     * @param CompanyInterface $company
     */
    private function setStreet(CompanyInterface $company)
    {
        $street = $company->getStreet();
        if (is_array($street) && count($street)) {
            $company->setStreet(trim(implode("\n", $street)));
        }
    }

    /**
     * @param CompanyInterface $company
     */
    private function setStatus(CompanyInterface $company)
    {
        if ($this->configProvider->isAutoApprove()) {
            $company->setStatus(CompanyStatus::STATUS_ACTIVE);
        } else {
            $company->setStatus(CompanyStatus::STATUS_PENDING);
        }
    }

    /**
     * @param string $email
     * @return bool
     */
    private function isEmailExist($email)
    {
        $company = $this->companyRepository->getByField(CompanyInterface::COMPANY_EMAIL, $email);

        return (bool)$company->getCompanyId();
    }

    /**
     * @return CompanyInterface
     */
    protected function getCurrentCompany()
    {
        if (!$this->currentCompany) {
            $this->currentCompany = $this->companyContext->getCurrentCompany();
            /*if ($this->currentCompany && !$this->currentCompany->getCompanyId()) {
                $this->currentCompany->addData($this->session->getData(SaveCompany::SESSION_NAME) ?: []);
            }*/
        }

        return $this->currentCompany;
    }

    /**
     * @param string $value
     * @return string
     */
    protected function getCompanyValue(string $value)
    {
        $company = $this->getCurrentCompany();
        return $company && $company->getData($value) ? $company->getData($value) : '';
    }

    // Save Role
    protected function saveRole($company)
    {
    	$data = array(
    		'role_name' => 'User',
    		'role_permissions' => self::ROLE_PERMISSIONS
    	);

    	try {
            $this->createRole($data, $company);
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('An error occurred on the server. Your changes have not been saved.')
            );
            $this->logger->critical($e);
        }
    }

    /**
     * @param array $data
     */
    private function createRole(array $data, $company)
    {
        if (isset($data[RoleInterface::ROLE_NAME])) {
            $role = $this->roleFactory->create();
            //$company = $this->companyContext->getCurrentCompany();
            $role->setCompanyId($company->getCompanyId());
            $role->setRoleName($data[RoleInterface::ROLE_NAME]);
            $role->setRoleId($data[RoleInterface::ROLE_ID] ?? null);
            $this->roleRepository->save($role);
            $this->savePermissions($role);
        }
    }

    /**
     * @param RoleInterface $role
     */
    private function savePermissions(RoleInterface $role)
    {
        $permissions = self::ROLE_PERMISSIONS;
        $permissions = $permissions ? explode(',', $permissions) : [];

        $data = [];
        foreach ($permissions as $permission) {
            $data[] = [
                PermissionInterface::ROLE_ID => $role->getRoleId(),
                PermissionInterface::RESOURCE_ID => $permission
            ];
        }
        $this->permissionRepository->multipleSave($data);
    }

}
