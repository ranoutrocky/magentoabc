<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Venture7\BusinessAccount\Controller\Index;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\InputException;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerFactory;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $formKeyValidator;
	
	protected $session;
	
	protected $customerRepository;
	
	protected $customer;

	protected $customerFactory;

	protected $_bprefFactory;

	public function __construct(
        Context $context,
        Session $customerSession,
		CustomerRepositoryInterface $customerRepository,
		Validator $formKeyValidator,
		Customer $customer,
		CustomerFactory $customerFactory,
		\Venture7\BusinessAccount\Model\BPRefFactory $bprefFactory
    ) {
        parent::__construct($context);
        $this->session = $customerSession;
		$this->customerRepository = $customerRepository;
		$this->formKeyValidator = $formKeyValidator;
		$this->customer = $customer;
		$this->customerFactory = $customerFactory;
		$this->_bprefFactory = $bprefFactory;
    }

    public function execute()
    {
        $validFormKey = $this->formKeyValidator->validate($this->getRequest());

        if ($validFormKey && $this->getRequest()->isPost()) {
			
			$customerId = $this->session->getCustomerId();

			$customer = $this->customer->load($customerId);

			$postData = $this->getRequest()->getPostValue();
			$customerData = $this->checkCustomerCokeId($postData['coke_id']);

			if($customer->getCokeId() == $postData['coke_id'])
			{
				//update customer
				$this->saveCustomAttribute($customerId);
				$this->saveCustomerAddress($customerId);
			} 
			elseif(empty($customerData)) {
				//new customer
				$this->saveCustomAttribute($customerId);
				$this->saveCustomerAddress($customerId);
			}
			else {
				//already used coke id
				$this->messageManager->addErrorMessage(__('This is not valid Coke ID. Please select another'));
				$resultRedirect = $this->resultRedirectFactory->create();
            	return $resultRedirect->setPath('businessaccount/index/index', array('closemodal' => 'Y'));
			}

			$this->session->setCustomerFormData($this->getRequest()->getPostValue());
        }

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('businessaccount/index/index');
      
        return $resultRedirect;
    }

    private function saveCustomAttribute($customerId)
    {
        
		$customer = $this->customer->load($customerId);
		
		$customerData = $customer->getDataModel();
		$customerResource = $this->customerFactory->create();
		
		$postData = $this->getRequest()->getPostValue();

		unset($postData['country_id']);
		unset($postData['region_id']);
		unset($postData['region']);
		unset($postData['street']);
		unset($postData['city']);
		unset($postData['postcode']);
		unset($postData['telephone']);
		unset($postData['form_key']);

		foreach($postData as $attr => $value) {
			
			if($attr == 'other_gases'){
				$customerData->setCustomAttribute($attr, json_encode($value));
				$customer->updateData($customerData);
				$customerResource->saveAttribute($customer, $attr);
			}
			else {
				$customerData->setCustomAttribute($attr, $value);
				$customer->updateData($customerData);
				$customerResource->saveAttribute($customer, $attr);
			}
		}
    }

    public function saveCustomerAddress($customerId)
	{
	    
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	    $addresss = $objectManager->get('\Magento\Customer\Model\AddressFactory');

		$postData = $this->getRequest()->getPostValue();

		$address = $addresss->create();

		$address->setCustomerId($customerId)
				->setFirstname($postData['organization_name'])
                ->setLastname($postData['organization_name'])
				->setCountryId($postData['country_id'])
				->setPostcode($postData['postcode'])
				->setCity($postData['city'])
				->setRegionId($postData['region_id'])
				->setTelephone($postData['telephone'])
				->setStreet($postData['street'])
				->setIsDefaultBilling(true)
				->setIsDefaultShipping(true)
				->setSaveInAddressBook(false);

		try{

			$address->save();
			
			if($postData['cust_ext_id']) {
				$this->updateBPRef($customerId);
			}

			$this->messageManager->addSuccessMessage(__('You saved the my business info.'));

		} catch (InputException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addErrorMessage($error->getMessage());
            }
        } catch (\Exception $e) {
        	$resultRedirect = $this->resultRedirectFactory->create();
            $this->messageManager->addExceptionMessage($e, __('We can\'t save the address.'));
            return $resultRedirect->setPath('businessaccount/index/index');
        }
	}

	public function updateBPRef($customerId)
	{
		$postData = $this->getRequest()->getPostValue();

		$refData = $this->_bprefFactory->create();

		$collection = $refData->getCollection()->addFieldToFilter('coke_id', ['eq' => $postData['coke_id']]);
        $collection->getFirstItem()->setData('magento_customer_id',$customerId)->save();
	}

	public function checkCustomerCokeId($cokeId) {
		$collection = $this->customer->getCollection()
		   ->addAttributeToSelect("*")
		   ->addAttributeToFilter("coke_id", array("eq" => $cokeId))
		   ->getFirstItem();

		return $collection->getData();
	}
}
