<?php

namespace Venture7\BusinessAccount\Model\ResourceModel\BPRef;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'coke_id';
    
    protected $_eventPrefix = 'businessaccount_bpref_collection';

    protected $_eventObject = 'bpref_collection';
    
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init('Venture7\BusinessAccount\Model\BPRef', 'Venture7\BusinessAccount\Model\ResourceModel\BPRef');
    }


   /*
   protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinRight(
            ['region' => $this->getTable("directory_country_region")], 'main_table.state = region.code',
            ['region_id' => 'region.region_id']
        );
    }
    */
}

