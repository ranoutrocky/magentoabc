<?php

namespace Venture7\BusinessAccount\Model;

class BPRef extends \Magento\Framework\Model\AbstractModel
{
	const CACHE_TAG = 'bp_ref';
	
	//Unique identifier for use within caching
	protected $_cacheTag = self::CACHE_TAG;

    protected $_eventPrefix = 'bp_ref';
	
	protected function _construct()
    {
        $this->_init('Venture7\BusinessAccount\Model\ResourceModel\BPRef');
    }
	
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }
}

