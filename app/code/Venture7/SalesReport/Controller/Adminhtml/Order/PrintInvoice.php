<?php

namespace Venture7\SalesReport\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Url;
use Venture7\SalesReport\Helper\Data;

class PrintInvoice extends Action
{
    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var Data
     */
    protected $_loginHelper;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param CustomerFactory $customerFactory
     */
    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        Data $helper
    ) {
        $this->_customerFactory = $customerFactory;
        $this->_loginHelper     = $helper;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function execute()
    {
        $customerId = $this->getRequest()->getParam('customer_id');
        $invoiceId = $this->getRequest()->getParam('invoice_id');

        $customer   = $this->_customerFactory->create()->load($customerId);
        if (!$customer || !$customer->getId()) {
            $this->messageManager->addErrorMessage(__('Customer does not exist.'));

            return $this->_redirect('customer');
        }

        $user  = $this->_auth->getUser();
        
        $store    = $this->_loginHelper->getStore($customer);
        $loginUrl = $this->_objectManager->create(Url::class)
            ->setScope($store)
            ->getUrl('reports/order/printinvoice', ['invoice_id' => $invoiceId, 'customer_id' => $customerId, '_nosid' => true]);

        return $this->getResponse()->setRedirect($loginUrl);
    }
}
