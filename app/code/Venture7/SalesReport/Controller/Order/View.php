<?php

namespace Venture7\SalesReport\Controller\Order;

use Exception;
use Magento\Checkout\Model\Cart;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Forward;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

class View extends Action
{
    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var SessionFactory
     */
    protected $session;

    /**
     * Index constructor.
     *
     * @param Context $context
     * @param SessionFactory $customerSession
     * @param AccountRedirect $accountRedirect
     */
    public function __construct(
        Context $context,
        SessionFactory $customerSession,
        AccountRedirect $accountRedirect
    ) {
        $this->session         = $customerSession;
        $this->accountRedirect = $accountRedirect;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|Forward|Redirect|ResultInterface
     */
    public function execute()
    {
        $customerId   = $this->getRequest()->getParam('customer_id');
        $orderId   = $this->getRequest()->getParam('order_id');
        $session = $this->session->create();

        if ($session->isLoggedIn()) {
            $session->logout();
        }

        try {
            $session->loginById($customerId);
            $session->regenerateId();
        } catch (Exception $e) {
            $this->messageManager->addError(
                __('An unspecified error occurred. Please contact us for assistance.')
            );
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('sales/order/view/', array('order_id' => $orderId, 'support' => 'Y'));
    }
}
