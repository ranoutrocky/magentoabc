<?php

namespace Venture7\SalesReport\Ui\Component\Listing\Column\PricingPlan;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Venture7\SalesReport\Model\CustomAttribute
     */
    private $customAttribute;

    public function __construct(
        \Venture7\SalesReport\Model\CustomAttribute $customAttribute
    ) {
        $this->customAttribute = $customAttribute;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $attributeCode = 'pricing_plan';
            $options = $this->customAttribute->getAttributeOptions($attributeCode);
            $this->options = $options;
        }

        return $this->options;
    }
}
