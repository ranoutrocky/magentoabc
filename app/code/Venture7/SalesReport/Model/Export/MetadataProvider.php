<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Venture7\SalesReport\Model\Export;

use DateTime;
use DateTimeZone;
use Exception;
use Magento\Framework\Api\Search\DocumentInterface;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Ui\Component\Filters;
use Magento\Ui\Component\Filters\Type\Select;
use Magento\Ui\Component\Listing\Columns;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Metadata Provider for grid listing export.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class MetadataProvider
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @var TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var string
     */
    protected $dateFormat;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var \TNW\TriadHQ\Model\Report
     */
    protected $report;

    /**
     * @param Filter $filter
     * @param TimezoneInterface $localeDate
     * @param ResolverInterface $localeResolver
     * @param string $dateFormat
     * @param array $data
     */
    public function __construct(
        Filter $filter,
        TimezoneInterface $localeDate,
        ResolverInterface $localeResolver,
        \Venture7\SalesReport\Model\Report $report,
        $dateFormat = 'M j, Y h:i:s A',
        array $data = []
    ) {
        $this->filter = $filter;
        $this->localeDate = $localeDate;
        $this->locale = $localeResolver->getLocale();
        $this->dateFormat = $dateFormat;
        $this->data = $data;
        $this->report = $report;
    }

    /**
     * Returns Columns component
     *
     * @param UiComponentInterface $component
     *
     * @return UiComponentInterface
     * @throws Exception
     */
    protected function getColumnsComponent(UiComponentInterface $component): UiComponentInterface
    {
        foreach ($component->getChildComponents() as $childComponent) {
            if ($childComponent instanceof Columns) {
                return $childComponent;
            }
        }
        throw new Exception('No columns found'); // @codingStandardsIgnoreLine
    }

    /**
     * Returns columns list
     *
     * @param UiComponentInterface $component
     *
     * @return UiComponentInterface[]
     * @throws Exception
     */
    protected function getColumns(UiComponentInterface $component): array
    {
        if (!isset($this->columns[$component->getName()])) {
            $columns = $this->getColumnsComponent($component);
            foreach ($columns->getChildComponents() as $column) {
                if ($column->getData('config/label') && $column->getData('config/dataType') !== 'actions') {
                    $this->columns[$component->getName()][$column->getName()] = $column;
                }
            }
        }

        return $this->columns[$component->getName()];
    }

    /**
     * Retrieve Headers row array for Export
     *
     * @param UiComponentInterface $component
     *
     * @return string[]
     * @throws Exception
     */
    public function getHeaders(UiComponentInterface $component): array
    {
        $row = [];
        foreach ($this->getColumns($component) as $column) {
            $row[] = $column->getData('config/label');
        }

        return $row;
    }

    /**
     * Returns DB fields list
     *
     * @param UiComponentInterface $component
     *
     * @return string[]
     * @throws Exception
     */
    public function getFields(UiComponentInterface $component): array
    {
        $row = [];
        foreach ($this->getColumns($component) as $column) {
            $row[] = $column->getName();
        }

        return $row;
    }

    /**
     * Returns row data
     *
     * @param DocumentInterface $document
     * @param array $fields
     * @param array $options
     *
     * @return string[]
     */
    public function getRowData(DocumentInterface $document, $fields, $options): array
    {
        $row = [];
        $paymentMethods = $this->report->getPaymentMethodsArray();
        foreach ($fields as $column) {
            if (isset($options[$column])) {
                $key = $document->getCustomAttribute($column)->getValue();
                if (isset($options[$column][$key])) {
                    $row[] = $options[$column][$key];
                } else {
                    $row[] = '';
                }
            } else {
                $value = $document->getCustomAttribute($column)->getValue();
                if (($column == 'payment_method') && array_key_exists($value, $paymentMethods)) {
                    $row[] = $paymentMethods[$value];
                } else if($column == 'total_tax') {
                    $row[] = $document->getCustomAttribute('shipping_tax')->getValue() + $document->getCustomAttribute('item_tax_amount')->getValue();
                } else if($column == 'shipping_city') {
                    if(!$value) {
                        $row[] = $document->getCustomAttribute('billing_city')->getValue();
                    } else {
                        $row[] = $value;
                    }
                } else if($column == 'shipping_region') {
                    if(!$value) {
                        $row[] = $document->getCustomAttribute('billing_region')->getValue();
                    } else {
                        $row[] = $value;
                    }
                } else if($column == 'product_tax_class') {
                    $row[] = $this->getProductTaxClass($document->getCustomAttribute('product_id')->getValue());
                } else if($column == 'tax_percent') {
                    $row[] = $document->getCustomAttribute('tax_percent')->getValue().'%';
                } else {
                    $row[] = $value;
                }
            }
        }

        return $row;
    }

    /**
     * Returns complex option
     *
     * @param array $list
     * @param string $label
     * @param array $output
     *
     * @return void
     */
    protected function getComplexLabel($list, $label, &$output): void
    {
        foreach ($list as $item) {
            if (!is_array($item['value'])) {
                $output[$item['value']] = $label . $item['label'];
            } else {
                $this->getComplexLabel($item['value'], $label . $item['label'], $output);
            }
        }
    }

    /**
     * Prepare array of options.
     *
     * @param array $options
     *
     * @return array
     */
    protected function getOptionsArray(array $options): array
    {
        $preparedOptions = [];
        foreach ($options as $option) {
            if (!is_array($option['value'])) {
                $preparedOptions[$option['value']] = $option['label'];
            } else {
                $this->getComplexLabel(
                    $option['value'],
                    $option['label'],
                    $preparedOptions
                );
            }
        }

        return $preparedOptions;
    }

    /**
     * Returns Filters with options
     *
     * @return array
     * @throws LocalizedException
     */
    public function getOptions(): array
    {
        return array_merge(
            $this->getColumnOptions(),
            $this->getFilterOptions()
        );
    }

    /**
     * Get options from columns.
     *
     * @return array
     * @throws LocalizedException
     * @throws Exception
     */
    protected function getColumnOptions(): array
    {
        $options = [];
        $component = $this->filter->getComponent();
        /** @var Column $columnComponent */
        foreach ($this->getColumns($component) as $columnComponent) {
            if ($columnComponent->hasData('options')) {
                $optionSource = $columnComponent->getData('options');
                $optionsArray = $optionSource instanceof OptionSourceInterface ?
                    $optionSource->toOptionArray() : $optionSource;
                $options[$columnComponent->getName()] = $this->getOptionsArray($optionsArray ?: []);
            }
        }

        return $options;
    }

    /**
     * Get options from column filters.
     *
     * @return array
     * @throws LocalizedException
     */
    protected function getFilterOptions(): array
    {
        $options = [];
        $component = $this->filter->getComponent();
        $childComponents = $component->getChildComponents();
        $listingTop = $childComponents['listing_top'];
        foreach ($listingTop->getChildComponents() as $child) {
            if ($child instanceof Filters) {
                foreach ($child->getChildComponents() as $filter) {
                    if ($filter instanceof Select) {
                        $options[$filter->getName()] = $this->getOptionsArray($filter->getData('config/options'));
                    }
                }
            }
        }

        return $options;
    }

    /**
     * Convert document date(UTC) fields to default scope specified
     *
     * @param DocumentInterface $document
     * @param string $componentName
     *
     * @return void
     * @throws Exception
     */
    public function convertDate($document, $componentName): void
    {
        if (!isset($this->data[$componentName])) {
            return;
        }
        foreach ($this->data[$componentName] as $field) {
            $fieldValue = $document->getData($field);
            if (!$fieldValue) {
                continue;
            }
            $convertedDate = $this->localeDate->date(
                new DateTime($fieldValue, new DateTimeZone('UTC')),
                $this->locale,
                true
            );
            $document->setData($field, $convertedDate->format($this->dateFormat));
        }
    }

    public function getProductTaxClass($producId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($producId);
        $taxClassObj = $objectManager->create('Magento\Tax\Model\TaxClass\Source\Product');
        $taxClasColection = $taxClassObj->getAllOptions();
        foreach($taxClasColection as $taxClass) {
            if(!$product->getTaxClassId()) {
                $item['product_tax_class'] = 'None';
            } else if(is_object($taxClass['value'])) {
                if($product->getTaxClassId() == $taxClass['value']->getText()) {
                    if(is_object($taxClass['label'])) {
                        $item['product_tax_class'] = $taxClass['label']->getText();
                    } else {
                        $item['product_tax_class'] = $taxClass['label']; 
                    }
                }
            } else {
                if($product->getTaxClassId() == $taxClass['value']) {
                    if(is_object($taxClass['label'])) {
                        $item['product_tax_class'] = $taxClass['label']->getText();
                    } else {
                        $item['product_tax_class'] = $taxClass['label']; 
                    }
                }
            }
        }
        return $item['product_tax_class'];
    }
}
