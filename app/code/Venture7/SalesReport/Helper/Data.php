<?php

namespace Venture7\SalesReport\Helper;

use Magento\Customer\Model\Customer;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Math\Random;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    protected $objectManager;

    protected $storeManager;

    /**
     * @var Random
     */
    protected $mathRandom;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param StoreManagerInterface $storeManager
     * @param Random $random
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager,
        Random $random
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager = $storeManager;
        $this->mathRandom = $random;
        parent::__construct($context);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getLoginToken()
    {
        return $this->mathRandom->getUniqueHash();
    }

    /**
     * @param Customer $customer
     *
     * @return StoreInterface|null
     * @throws NoSuchEntityException
     */
    public function getStore($customer)
    {
        if ($storeId = $customer->getStoreId()) {
            return $this->storeManager->getStore($storeId);
        }

        return $this->storeManager->getDefaultStoreView();
    }
}
